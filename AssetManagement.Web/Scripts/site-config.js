﻿$(function () {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "300",
        "timeOut": "10000",
        "extendedTimeOut": "0",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    $(document).on('click', '#hamburger', function () {
        $('.ui.sidebar').sidebar('setting', 'transition', 'overlay')
                        .sidebar('toggle')
                        .sidebar('setting', 'onHidden', function () {
                            transformicons.revert('.tcon-menu--xcross');
                        });
    })

    transformicons.add('.tcon') // add default behavior for all elements with the class .tcon
              .remove('.tcon-menu--xcross') // remove default behavior for the first icon
              .add('.tcon-menu--xcross', {
                  transform: "click",
                  revert: "click"
              }); // add new behavior for the first icon
});