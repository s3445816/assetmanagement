﻿$(document).ready(function () {
    $('.ui.form')
        .form({
            fields: {
                username: {
                    identifier: 'username',
                    rules: [
                      {
                          type: 'empty',
                          prompt: 'Please enter your username'
                      }
                    ]
                },
                password: {
                    identifier: 'password',
                    rules: [
                      {
                          type: 'empty',
                          prompt: 'Please enter a password'
                      }
                    ]
                },
            },
            inline: true,
            onSuccess: function (event, fields) {
                var scope = angular.element($('#loginForm')).scope();

                scope.$apply(function () {
                    scope.login();
                });

                return false;
            }
        })
});