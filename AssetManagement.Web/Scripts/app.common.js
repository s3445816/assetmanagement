﻿function milisecondToMinute(miliseconds) {
    return Math.round(miliseconds / 60000);
}

function dateDiffToMinute(date1, date2) {

    if (typeof date1 == 'string')
        date1 = new Date(date1);
    if (typeof date2 == "string")
        date2 = new Date(date2);

    return milisecondToMinute(Math.abs(date1 - date2));
}

function scrollToElement(element, time) {
    $('html, body').animate({
        scrollTop: $(element).offset().top
    }, time);
}

function convertUTCTimeToLocalTime(utcTime) {
    var momentObj = moment.parseZone(utcTime);
    return momentObj.getCurrentInfo();
}