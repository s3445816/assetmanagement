﻿String.prototype.replaceAll = function (target, replacement) {
    return this.split(target).join(replacement);
};

function openMarkup(content, isHtmlTag) {
    return isHtmlTag ? '<' + content + '>' : '[' + content + ']';
}

function closeMarkup(content, isHtmlTag) {
    return isHtmlTag ? '</' + content + '>' : '[/' + content + ']';
}

function createHyperLinkFromMarkup(markupContent) {
    var start = -1;
    var end = -1;

    do {
        start = markupContent.indexOf('<a>', end + 1);
        if (start != -1) {
            end = markupContent.indexOf('</a>', start);

            var content = markupContent.substring(start + 3, end);

            var innerStart = content.indexOf('{');
            if (innerStart == -1)
                continue;

            var innerEnd = content.indexOf('}');
            var innerContent = content.substring(innerStart + 1, innerEnd);
            content = content.replace('{' + innerContent + '}', '');

            var link = '<a href="#/history?id=' + innerContent + '&name=' + content + '">' + content + '</a>';
            markupContent = markupContent.substring(0, start) + link + markupContent.substring(end + 4);

            end += link.length - 3;
        }
    }
    while (start != -1)

    return markupContent;
}

function parseActivityLogMarkup(markupContent) {

    var result = {};
    result.Content = '';
    result.Items = [];

    var markupList = ['Actor', 'Staff', 'Asset', 'Office', 'Reminder', 'List', 'Item'];

    for (index in markupList) {
        var markup = markupList[index];
        if (markup != 'List' && markup != 'Item') {
            markupContent = markupContent.replaceAll(openMarkup(markup, false), openMarkup('a', true));
            markupContent = markupContent.replaceAll(closeMarkup(markup, false), closeMarkup('a', true));
        }
        else {
            var list = markupContent.indexOf(openMarkup(markup, false));
            if (list != -1) {
                list = markupContent.substring(list);
                markupContent = markupContent.substring(0, markupContent.indexOf(openMarkup(markup, false)));
                var start = -1;
                var end = -1;
                do {
                    start = list.indexOf('[Item]', end + 1);
                    if (start != -1) {
                        end = list.indexOf('[/Item]', start);
                        var item = createHyperLinkFromMarkup(list.substring(start + 6, end));
                        result.Items.push(item);
                    }
                }
                while (start != -1)
            }
        }
    }

    result.Content = createHyperLinkFromMarkup(markupContent);
    return result;
}

function parseActivityLog(activityLog) {

    var log = {};
    var colors = ['red', 'orange', 'yellow', 'olive', 'black'];

    log.TimePassed = dateDiffToMinute(new Date(), activityLog.DateOccurred);

    log.Color = Math.floor(log.TimePassed / 15);
    if (log.Color < 0)
        log.Color = 0;
    if (log.Color >= colors.length)
        log.Color = colors.length - 1;
    log.Color = colors[log.Color];

    var result = parseActivityLogMarkup(activityLog.Content);
    log.ParsedContent = result.Content;
    log.Items = result.Items;

    return log;
}