﻿var lastCodePart = '';

function pad(num, size) {
    var s = "000000000" + num;
    return s.substr(s.length - size);
}

function generateCodes(lastCode, maxLength, amount, defaultName) {

    var codes = [];
    var length = lastCode.length;
    var codePart;
    var textPart;

    while (maxLength > 0) {
        codePart = lastCode.substring(length - maxLength, length);
        if (!isNaN(codePart) && parseInt(codePart) > 0) {
            lastCodePart = parseInt(codePart);
            break;
        }
        maxLength--;
    }

    if (maxLength > 0) {
        textPart = lastCode.substring(0, length - maxLength);
        for (var i = 1; i <= amount; i++) {
            var newCodePart = parseInt(codePart) + i;
            var newCodePartLength = newCodePart.toString().length;
            var newCode = textPart + pad(newCodePart, maxLength > newCodePartLength ? maxLength : newCodePartLength);
            codes.push({ id: i, code: newCode });
        }
    }
    else {
        for (var i = 1; i <= amount; i++) {
            codes.push({ id: i, code: defaultName + i.toString() });
        }
    }

    return codes;
}

function initializeCodes(name, maxLength, amount) {

    var codes = [];

    for (var i = 1; i <= amount; i++) {
        var codePartLength = i.toString().length;
        var newCode = name + pad(i, maxLength > codePartLength ? maxLength : codePartLength);
        codes.push({ id: i, code: newCode });
    }

    return codes;
}