﻿/* Filter to convert array to string */
app.filter('join', function () {
    return function (array, delimiter) {
        if (_.isArray(array)) {
            return array.join(delimiter);
        }
        return array;
    }
});

/* Filter to range */
app.filter('range', function () {
    return function (input, total) {
        total = parseInt(total);
        for (var i = 0; i < total; i++)
            input.push(i);
        return input;
    };
});