﻿'use strict';
app.registerController('searchController', ['$scope', '$routeParams', '$location', '$rootScope', 'baseService', 'oDataService', 'officeService', 'staffService', function ($scope, $routeParams, $location, $rootScope, baseService, oDataService, officeService, staffService) {

    $scope.displayMatches = [].concat($scope.matches);
    $scope.matches = [];
    $scope.itemsByPage = 30;

    $scope.lookup = function (keyword, dropdownId) {
        keyword = keyword == undefined ? $(dropdownId).search('get value') : keyword;

        var criteria = {};
        criteria.entitySet = 'staffs';
        if (keyword) {
            criteria.filter = '$filter=contains(Name,\'' + keyword + '\')';
        }

        oDataService.lookup(
            criteria,
            function (response) {
                $scope.matches = response.value;
            }
        );
    };

    $scope.lookup($routeParams.keyword, '#ddSearchStaff');

    $scope.officesData = [];

    $scope.getOfficesData = function () {
        officeService.getAll()
                     .then(function (response) {
                         $scope.officesData = response.data;
                     }, function (error) {
                         toastr.error(error, "Loading Offices")
                     });
    };

    /*
     * Instant call     
     */
    $scope.getOfficesData();


    /*
     * Change office
     */
    $scope.staffOfficeModel = {
        id: '',
        officeId: ''
    };

    $scope.changeOffice = function (id, officeId) {
        console.log("[changeOffice] function with id: " + id + " and officeId: " + officeId);
        $scope.staffOfficeModel.id = id;
        $scope.staffOfficeModel.officeId = officeId;

        var staff = baseService.findItemById($scope.matches, id);
        if (staff.OfficeId == officeId) {
            return;
        }

        staffService.changeOffice($scope.staffOfficeModel)
                    .then(function (response) {
                        var office = baseService.findItemById($scope.officesData, officeId);
                        staff.OfficeId = officeId;
                        staff.Office.Name = office.Name || office.name;

                        toastr.success("Successfully!", "Changing Office");
                        return true;
                    }, function (error) {
                        toastr.error(error, "Changing Office");
                        return false;
                    });
    }
    
}]);