﻿'use strict';
app.registerController('loginController', ['$rootScope', '$scope', '$location', 'authService', 'localStorageService', 'assetTypeService', function ($rootScope, $scope, $location, authService, localStorageService, assetTypeService) {

    var LOCALSS_ASSETTYPEDATA = 'assetTypeData';

    $scope.loginData = {
        userName: '',
        password: '',
        useRefreshToken: true
    };

    $scope.login = function () {

        // Add loading state
        $('#loginForm').addClass('loading');

        authService.logIn($scope.loginData)
                   .then(function (response) {
                       $('#loginForm').removeClass('loading');
                       if ($rootScope.callbackUrl) {
                           $location.path($rootScope.callbackUrl);
                       } else {
                           $location.path('/home');
                       }

                       assetTypeService.getAssetTypes().then(function (res) {
                           localStorageService.set(LOCALSS_ASSETTYPEDATA, res.data);
                       });
                       

                   }, function (error) {
                       $scope.isLoading = false;
                       $('#loginForm').removeClass('loading');
                       toastr.error(error.error_description,error.error);
                   });

    };
}]);