﻿'use strict';
app.controller('indexController', ['$scope', '$routeParams', '$location', '$rootScope', 'baseService', 'authService', function ($scope, $routeParams, $location, $rootScope, baseService, authService) {
 
    $scope.logOut = function () {
        authService.logOut();
        $location.path('/login');
    }
    
    $scope.authentication = authService.authentication;

    // Remember to pass '/' at the beginning of 'path'
    $scope.isActiveLink = function (path) {
        return $location.path() === path;
    }

    $scope.lookup = function (keyword, dropdownId) {
        keyword = (keyword == undefined || keyword == '')
                        ? $(dropdownId).search('get value') : keyword;
        $location.url('/search/' + keyword);
    };
}]);