﻿'use strict';
app.registerController('assettypesController', ['$scope', 'assetTypeService', '$filter', 'localStorageService', function ($scope, assetTypeService, $filter, localStorageService) {

    var LOCALSS_AUTHORIZATIONDATA = 'authorizationData';
    var LOCALSS_ASSETTYPEDATA = 'assetTypeData';

    $scope.displayAssets = [].concat($scope.assets);
    $scope.assets = [];
    $scope.itemsByPage = 10;

    $scope.assetTypes = [];

    $scope.newAssetName = "";
    $scope.newAssetNote = "";

    $scope.assetTypes = localStorageService.get(LOCALSS_ASSETTYPEDATA);

    $scope.getAssetTypeName = function (type) {
        if (typeof type == "string") {
            type = parseInt(type);
        }

        var typeIndex = _.findLastIndex($scope.assetTypes, { value: type });
        return $scope.assetTypes[typeIndex].display;
    };

    $scope.getAssets = function () {
        assetTypeService.getAssets().then(function (response) {
            $scope.assets = $filter('orderBy')(response, ['ModifiedDate != null ? ModifiedDate : CreatedDate', '-Id'], true);

            for (var index = 0; index < $scope.assets.length; index++) {
                $scope.assets[index].AssetTypeName = $scope.getAssetTypeName($scope.assets[index].Type);
            }
        });
    }();
    //Creating new asset
    $scope.createNewAsset = function ($event, dropdownId) {
        var newAssetType = $('#' + dropdownId).dropdown('get value');
        if ($scope.newAssetName.trim() == "" || newAssetType == -1)
            return;

        $($event.currentTarget).addClass('loading');

        assetTypeService.addNewAsset(
                $scope.newAssetName,
                newAssetType,
                $scope.newAssetNote
            )
            .then(function (response) {
                if (response.status == 200 && response.data != -1) {
                    $scope.assets.splice(0, 0, {
                        Id: response.data,
                        Name: $scope.newAssetName,
                        Type: newAssetType,
                        Note: $scope.newAssetNote,
                        AssetTypeName: $scope.getAssetTypeName(newAssetType)
                    });

                    $scope.newAssetName = "";
                    $scope.newAssetNote = "";
                    $('#' + dropdownId).dropdown('clear');

                    toastr.success("", "Add new asset successfully!");
                }
                else {
                    toastr.error("", "Asset existed!");
                }

                $($event.currentTarget).removeClass('loading');
            });
    };

    $scope.editAsset = function (id, name, type, note) {
        assetTypeService.editAsset(id, name, type, note)
            .then(function (response) {
                if (response.status == 200) {
                    for (var index = 0; index < $scope.assets.length; index++) {
                        if ($scope.assets[index].Id == id) {
                            $scope.assets[index].Name = name;
                            $scope.assets[index].Type = type;
                            $scope.assets[index].Note = note;
                            $scope.assets[index].AssetTypeName = $scope.getAssetTypeName(type);
                            break;
                        }
                    }

                    toastr.success("", "Edit asset successfully!");
                }
                else {
                    toastr.error("", "Edit asset failed!");
                }
            });
    };

    $scope.deleteAsset = function (id) {
        assetTypeService.deleteAsset(id)
            .then(function (response) {
                if (response.status == 200) {
                    for (var index = 0; index < $scope.assets.length; index++) {
                        if ($scope.assets[index].Id == id) {
                            $scope.assets.splice(index, 1);
                            break;
                        }
                    }

                    toastr.success("", "Delete asset successfully!");
                }
                else {
                    toastr.error("", "Delete asset failed!");
                }
            });
    };
}]);