﻿'use strict';
app.registerController('assetsController', ['$scope', '$routeParams', 'assetService', 'assetTypeService', function ($scope, $routeParams, assetService, assetTypeService) {
       
    $scope.assetId = $routeParams.id;
    $scope.isDisplayBatch = false;
    $scope.amount = 5;
    $scope.lastId = "";
    $scope.nextId = "";
    $scope.displayCodes = [].concat($scope.codes);
    $scope.codes = [];
    $scope.itemsByPage = 10;
    $scope.appliedAmount = "";
    $scope.isShowResult = false;

    $scope.getAssets = function () {
        assetTypeService.getAssets().then(function (response) {
            $scope.assets = response;
        });
    }();

    $scope.generateAssetCode = function (dropdownId) {
        var assetId = $('#' + dropdownId).dropdown('get value');
        if (assetId != "" && !isNaN($scope.amount) && $scope.amount > 0) {
            assetService.getLastAssignmentByAssetId(assetId)
                .then(function (response) {
                    var lastCode = response.data;
                    assetService.getAssetTypeName(assetId)
                            .then(function (res) {
                                var typeName = res.data.substring(0, 3).toUpperCase();
                                if (lastCode != "") {
                                    $scope.codes = generateCodes(lastCode, 3, $scope.amount, "ASWVN" + typeName);
                                    $scope.lastId = lastCode;
                                }
                                else {
                                    $scope.codes = initializeCodes("ASWVN" + typeName, 3, $scope.amount);
                                    $scope.lastId = "N/A";
                                }
                                $scope.nextId = $scope.codes[0].code;
                            });
                });

            $scope.isDisplayBatch = true;
            $scope.appliedAmount = $scope.amount;
        }
    };

    $scope.clearAll = function (dropdownId) {
        $scope.amount = 5;
        if ($scope.assetId == undefined || $scope.assetId == null) {
            $('#' + dropdownId).dropdown("clear");
        }
        $scope.codes = [];
        $scope.lastId = "";
        $scope.nextId = "";
        $scope.isDisplayBatch = false;
        $scope.isShowResult = false;
    };

    $scope.showResult = function (dropdownId) {
        $scope.amount = "";
        if ($scope.assetId == undefined || $scope.assetId == null) {
            $('#' + dropdownId).dropdown("clear");
        }
        $scope.isShowResult = true;
    }

    $scope.removeLastItem = function () {
        $scope.codes.splice($scope.codes.length - 1, 1);
        $scope.amount--;
        $scope.appliedAmount = $scope.amount;

        if ($scope.amount == 0) {
            $scope.amount = "";
            $scope.isDisplayBatch = false;
        }
    };

    $scope.createBatch = function (dropdownId) {
        var assetId = $('#' + dropdownId).dropdown('get value');
        assetService.createAssetsByBatch(assetId, $scope.codes)
                .then(function (response) {
                    $scope.showResult(dropdownId);
                    toastr.success("", "Create batch successfully!");
                },
                function (err) {
                    toastr.error("", "Create batch failed!");
                });
    };

    var timer = setInterval(function () {
        var dropdown = $('#dropdown');
        if (dropdown != undefined && dropdown != null) {
            if ($scope.assetId != undefined && $scope.assetId != null) {
                $('#dropdown').dropdown('set selected', $scope.assetId);
            }
            clearInterval(timer);
        }
    }, 300);
    
}]);