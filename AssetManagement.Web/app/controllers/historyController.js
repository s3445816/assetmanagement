﻿'use strict';
app.registerController('historyController', ['$scope', '$routeParams', 'historyService', '$filter', function ($scope, $routeParams, historyService, $filter) {

    $scope.isStaff = true;
    $scope.staffHistory = {};
    $scope.assetHistory = {};

    $scope.displayAssignments = [].concat($scope.assetHistory.items);
    $scope.itemsByPage = 20;

    $scope.getHistory = function (id, name) {
        historyService.getHistory(id, name)
            .then(function (response) {
                var history = response;
                if (history.staffId != undefined && history.staffId != null) {
                    $scope.isStaff = true;
                    $scope.staffHistory = history;
                } else {
                    $scope.isStaff = false;
                    $scope.assetHistory = history;
                }
            },
            function (err) {
                toastr.error("", "Get history failed!");
            });
    }($routeParams.id, $routeParams.name);
}]);