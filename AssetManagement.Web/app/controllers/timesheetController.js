﻿'use strict';
app.registerController('timesheetController', ['$scope', '$filter', 'localStorageService', 'timesheetService', function ($scope, $filter, localStorageService, timesheetService) {

    $scope.data = [];
    $scope.firstCheckIn = {};

    $scope.init = function () {
        timesheetService.getFirstCheckInToday()
                      .then(function (response) {
                          $scope.firstCheckIn = moment(response.data).format("HH:mm:ss");
                      }, function (error) {
                          $scope.firstCheckIn = {};
                          toastr.success("", "Error when gettting first check in today!");
                      });

        timesheetService.getCheckInFromBeginingOfMonth()
                      .then(function (response) {

                          $scope.data = _.groupBy(response.data, function (item) {
                              return item.checktime.substring(0, 10);
                          });

                          var tmp = [];

                          _.each($scope.data, function (val, key) {
                              var firstTime = _.last(val).checktime;

                              var endDate = moment(_.first(val).checktime);
                              var startDate = moment(firstTime);
                              var desiredDate = moment(firstTime);
                              var from;
                              var to;
                              var lackingTime = '';

                              desiredDate.add(9, 'h');

                              if (desiredDate > endDate) {
                                  from = desiredDate;
                                  to = endDate;

                                  var difference = from.diff(to);
                                  var duration = moment.duration(difference);
                                  var hour = Math.floor(duration.asHours());

                                  if (hour < 10) hour = '0' + hour;

                                  lackingTime = hour + moment.utc(difference).format(":mm:ss");

                              } else {
                                  from = endDate;
                                  to = desiredDate;
                              }

                              var startHour = tmp.push({
                                  'date': key,
                                  'start': startDate.format("HH:mm:ss"),
                                  'end': endDate.format("HH:mm:ss"),
                                  'miss': lackingTime
                              });
                          });

                          $scope.data = tmp;

                      }, function (error) {
                          $scope.data = {};
                          toastr.success("", "Error when gettting first check in today!");
                      });

    }

    $scope.init();
}]);