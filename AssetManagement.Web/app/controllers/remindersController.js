﻿'use strict';
app.registerController('remindersController', ['$scope', 'reminderService', '$filter', function ($scope, reminderService, $filter) {

    $scope.displayReminders = [].concat($scope.reminders);
    $scope.reminders = [];
    $scope.itemsByPage = 20;

    var completedReminderIds = [];

    $scope.getAllReminders = function () {
        reminderService.getReminders('$orderby=TimeStamp desc,DueTime desc')
            .then(function (response) {
                $scope.reminders = response;

                for (var index = 0; index < $scope.reminders.length; index++) {
                    console.log($scope.reminders[index].TimeStamp);
                    $scope.reminders[index].TimeStamp = convertUTCTimeToLocalTime($scope.reminders[index].TimeStamp);
                    $scope.reminders[index].DueTime = convertUTCTimeToLocalTime($scope.reminders[index].DueTime);
                }
            });
    };

    $scope.checkComplete = function (id, event) {
        // Mark reminder as complete or undo
        var target = $(event.currentTarget);
        var unCheckClass = 'square outline';
        var checkClass = 'check square orange';

        if (!target.hasClass(unCheckClass)) {
            var tmp = checkClass;
            checkClass = unCheckClass;
            unCheckClass = tmp;
        }

        target.removeClass(unCheckClass);
        target.addClass(checkClass);

        if (!target.hasClass('square outline')) {
            completedReminderIds.push(id);
        } else {
            completedReminderIds.splice(completedReminderIds.indexOf(id), 1);
        }
    };

    $scope.complete = function (event) {
        if (completedReminderIds.length == 0)
            return;

        var target = $(event.currentTarget);
        target.addClass('loading');

        reminderService.complete(completedReminderIds)
            .then(function (response) {
                for (var index = 0; index < $scope.reminders.length; index++) {
                    if (completedReminderIds.indexOf($scope.reminders[index].Id) != -1) {
                        $scope.reminders[index].Completed = true;
                    }
                }

                completedReminderIds = [];

                target.removeClass('loading');
                toastr.success("", "Update reminder successfully!");
            },
            function (err) {
                target.removeClass('loading');
                toastr.error("", "Update reminder failed");
            });
    };

    $scope.createNewReminder = function (timestamp, duetime, content) {
        reminderService.addNewReminder(
                timestamp,
                duetime,
                content
            )
            .then(function (response) {
                console.log(response);
                if (response != -1) {
                    $scope.reminders.splice(0, 0, {
                        Id: response,
                        TimeStamp: timestamp,
                        DueTime: duetime,
                        Content: content,
                        Completed: false
                    });
                }

                toastr.success("", "Add new reminder successfully!");
            },
            function (err) {
                toastr.error("", "Add new reminder failed");
            });
    };

    $scope.updateReminder = function (id, timestamp, duetime, content, completed) {
        reminderService.updateReminder(id, timestamp, duetime, content, completed)
            .then(function (response) {
                for (var index = 0; index < $scope.reminders.length; index++) {
                    if ($scope.reminders[index].Id == id) {
                        $scope.reminders[index].TimeStamp = timestamp;
                        $scope.reminders[index].DueTime = duetime;
                        $scope.reminders[index].Content = content;
                        $scope.reminders[index].Completed = completed;
                        break;
                    }
                }

                console.log($scope.reminders);

                toastr.success("", "Update reminder successfully!");
            },
            function (err) {
                toastr.error("", "Update reminder failed!");
            });
    };

    $scope.deleteReminder = function (id) {
        reminderService.deleteReminder(id)
            .then(function (response) {
                for (var index = 0; index < $scope.reminders.length; index++) {
                    if ($scope.reminders[index].Id == id) {
                        $scope.reminders.splice(index, 1);
                        break;
                    }
                }

                toastr.success("", "Delete reminder successfully!");
            },
            function (err) {
                toastr.error("", "Delete reminder failed!");
            });
    };

    /* Instant call */
    $scope.getAllReminders();

}]);