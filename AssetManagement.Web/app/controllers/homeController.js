﻿'use strict';
app.registerController('homeController', ['$http', '$scope', '$filter', 'localStorageService', 'reminderService', 'activityLogService', 'oDataService', function ($http, $scope, $filter, localStorageService, reminderService, activityLogService, oDataService) {

    var completedReminderIds = [];

    $scope.complete = function (id, e) {

        // Mark reminder as complete or undo
        var target = $(e.currentTarget);
        var unCheckClass = 'square outline';
        var checkClass = 'check square orange';

        if (!target.hasClass(unCheckClass)) {
            var tmp = checkClass;
            checkClass = unCheckClass;
            unCheckClass = tmp;
        }

        target.removeClass(unCheckClass);
        target.addClass(checkClass);

        // Linethrough text or undo
        var content = target.parent().find('.content');
        var linethrough = 'linethrough';

        if (!content.hasClass(linethrough)) {
            content.addClass(linethrough);
            completedReminderIds.push(id);
        } else {
            content.removeClass(linethrough);
            completedReminderIds.splice(completedReminderIds.indexOf(id), 1);
        }
    }

    var groupReminders = function (reminders) {
        if (reminders.length == 0)
            return;

        reminders.sort(function (r1, r2) {
            if (r1.TimeStamp < r2.TimeStamp)
                return -1;
            if (r1.TimeStamp > r2.TimeStamp)
                return 1;
            return 0;
        });

        var group = [];
        var index = 0;
        var lastIndex = 0;
        do {
            while (lastIndex < reminders.length && reminders[lastIndex].TimeStamp.toString() == reminders[index].TimeStamp.toString())
                lastIndex++;
            var listOfReminders = [];
            for (var i = index; i < lastIndex; i++) {
                listOfReminders.push(reminders[i]);
            }
            group.push(
                {
                    "TimeStamp": reminders[index].TimeStamp,
                    "Reminders": listOfReminders
                });
            index = lastIndex;
        }
        while (index < reminders.length);

        return group;
    };

    $scope.getReminders = function () {
        reminderService.getReminders('$filter=Completed eq false')
            .then(function (response) {
                $scope.reminders = response;

                for (var index = 0; index < $scope.reminders.length; index++) {
                    $scope.reminders[index].TimeStamp = convertUTCTimeToLocalTime($scope.reminders[index].TimeStamp);
                    $scope.reminders[index].DueTime = convertUTCTimeToLocalTime($scope.reminders[index].DueTime);
                }

                $scope.reminderCount = $scope.reminders.length;
                $scope.reminderGroup = groupReminders($scope.reminders);
            });
    };

    $scope.updateBinding = function () {
        for (index = $scope.reminderGroup.length - 1; index >= 0; index--) {
            for (var subIndex = $scope.reminderGroup[index].Reminders.length - 1; subIndex >= 0; subIndex--) {
                var reminder = $scope.reminderGroup[index].Reminders[subIndex];
                if (completedReminderIds.indexOf(reminder.Id) != -1) {
                    $scope.reminderGroup[index].Reminders.splice(subIndex, 1);
                    $scope.reminderCount--;
                }
            }
            if ($scope.reminderGroup[index].Reminders.length == 0) {
                $scope.reminderGroup.splice(index, 1);
            }
        }
    };

    $scope.isLoadingReminder = false;

    $scope.refreshReminders = function (panelClassName, e) {
        $scope.isLoadingReminder = true;

        $(panelClassName).dimmer('show');

        reminderService.complete(completedReminderIds)
            .then(function (response) {
                $scope.isLoadingReminder = false;
                $(panelClassName).dimmer('hide');
                $scope.updateBinding();

            }, function (error) {
                $scope.isLoadingReminder = false;
                $(panelClassName).dimmer('hide');
            });
    }

    /*
     * Get staff list
     */
    $scope.displayMatchesStaff = [].concat($scope.matchesStaff);
    $scope.matchesStaff = [];
    $scope.itemsByPage = 10;

    $scope.getStaffList = function (keyword) {

        if (!keyword) {
            $scope.matchesStaff = [];
            return;
        }

        oDataService.lookup(
            {
                entitySet: 'staffs', filter: '$top=10'
            },
            function (response) {
                $scope.matchesStaff = response.value;
            }
        );
    };

    /*
     * Get activity logs
     */
    $scope.numberOfLogs = 5;
    $scope.maxNumberOfLogs = 50;
    $scope.minNumberOfLogs = 5;
    $scope.step = 5;

    $scope.getActivityLogs = function () {
        activityLogService.getActivityLogs('$top=' + $scope.numberOfLogs + '&$orderby=DateOccurred desc')
            .then(function (response) {
                $scope.activityLogs = response;

                // TimePassed, Color, ParsedContent, Items
                $scope.logs = [];

                for (var index = 0; index < $scope.activityLogs.length; index++) {
                    $scope.logs.push(parseActivityLog($scope.activityLogs[index]));
                }

                $scope.logs = $filter('orderBy')($scope.logs, ['-TimePassed'], true);
            });
    }

    $scope.loadMoreActivities = function ($event) {
        var target = $($event.currentTarget);

        if ($scope.numberOfLogs < $scope.maxNumberOfLogs && !target.hasClass('loading')) {

            target.addClass('loading');

            $scope.numberOfLogs += $scope.step;
            $scope.getActivityLogs();
            scrollToElement($event.currentTarget, 1000);

            if ($scope.numberOfLogs > $scope.logs.length) {
                $scope.numberOfLogs -= $scope.step;
            }

            target.removeClass('loading');
        }
    }

    $scope.loadLessActivities = function ($event) {
        var target = $($event.currentTarget);

        if ($scope.numberOfLogs > $scope.minNumberOfLogs && !target.hasClass('loading')) {

            target.addClass('loading');

            $scope.numberOfLogs -= $scope.step;
            $scope.getActivityLogs();
            scrollToElement($event.currentTarget, 1000);

            target.removeClass('loading');
        }
    }

    /*
     * Instant call     
     */
    $scope.getReminders();
    $scope.getStaffList('a');
    $scope.getActivityLogs();

}]);