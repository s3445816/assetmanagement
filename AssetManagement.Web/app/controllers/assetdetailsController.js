﻿'use strict';
app.registerController('assetdetailsController', ['$scope', '$routeParams', 'historyService', function ($scope, $routeParams, historyService) {

    $scope.assetHistory = {};

    $scope.displayAssignments = [].concat($scope.assetHistory.items);
    $scope.itemsByPage = 20;

    $scope.getAssetHistory = function (id) {
        historyService.getAssetHistory(id)
            .then(function (response) {
                $scope.assetHistory = response;
            },
            function (err) {
                toastr.error("", "Get asset details failed!");
            });
    }($routeParams.id);
}]);