﻿'use strict';
app.registerController('staffEditAssignmentsController', ['$scope', '$routeParams', '$rootScope', '$filter', 'staffService', 'authService', 'assetService', function ($scope, $routeParams, $rootScope, $filter, staffService, authService, assetService) {

    $scope.staffId = $routeParams.id;
    $scope.staffName = '';
    $scope.assetTypesData = [];
    $scope.assetTypesDataClone = [];

    $scope.assignmentsDisplayData = [];
    $scope.assignmentsDisplayDataClone = [];
    $scope.assetsId = [];
    $scope.assignmentData = {
        identityCodes: [],
        identityIds: [],
        referenceList: []
    };

    $scope.identityCodesId = "";
    $scope.identityCodesName = "";
    $scope.assetTypeName = "";

    $scope.getAssetTypesData = function () {
        assetService.getAssetTypes()
                    .then(function (response) {
                        $scope.assetTypesData = response.data;

                        // Clone data
                        angular.copy($scope.assetTypesData, $scope.assetTypesDataClone);

                        $rootScope.$broadcast('assetTypesDataLoadedEvent');
                    }, function (error) {
                        toastr.error(error, "Loading Asset Types")
                    });
    };

    $scope.assignmentsData = [];

    $scope.getAssignments = function () {
        staffService.getAssignments($scope.staffId)
                    .then(function (response) {
                        $scope.assignmentsData = response.assignments;
                        $scope.staffName = response.staffName;
                        $rootScope.$broadcast('gotStaffInformationEvent', { message: $scope.staffName });
                        $scope.manipulateAssignmentData();
                    }, function (error) {
                        toastr.error(error, "Loading Assignments")
                    });
    };

    $scope.manipulateAssignmentData = function () {
        _.each($scope.assignmentsData, function (item) {
            var rs = _.find($scope.assignmentsDisplayData, function (i) {
                return i.assetTypeName == item.assetType;
            });

            var assetsName = item.assetName + ' ' + item.identityCode;

            if (rs == undefined) {
                $scope.assignmentsDisplayData.push({
                    assetTypeName: item.assetType,
                    assignmentsId: [item.id],
                    assetsName: [assetsName]
                });
            }
            else {
                //rs.assignmentsId = rs.assignmentsId + ',' + item.id;
                //rs.assetsName = rs.assetsName + ',' + assetsName;
                rs.assignmentsId.push(item.id);
                rs.assetsName.push(assetsName);
            }
        });

        angular.copy($scope.assignmentsDisplayData, $scope.assignmentsDisplayDataClone);
    }

    /*
     * Instant call     
     */
    $scope.getAssetTypesData();
    $scope.getAssignments();

    $scope.addAssignment = function (form) {
        var validated = $(form).form('is valid');
        if (!validated || !$scope.validInput()) {
            // Show message here

            // then return
            return false;
        }

       
        $scope.identityCodesId = $scope.identityCodesId.split(',');
        // Push to list
        $scope.assignmentsDisplayData.push({
            assetTypeName: $scope.assetTypeName,
            assignmentsId: $scope.identityCodesId,
            assetsName: $scope.identityCodesName
        });

        // Remove selected asset type to out of list
        $scope.updateAssetTypesData($scope.assetTypeName, true);

        // Broadcast an event
        $rootScope.$broadcast('assignmentAddedEvent');

        // Reset value
        $scope.resetValue();
    };

    $scope.getAssetTypeDataByName = function (name) {
        var rs = _.find($scope.assetTypesDataClone, function (item) {
            return item.display == name;
        });

        return rs;
    };

    $scope.updateAssetTypesData = function (name, remove) {
        var item = $scope.getAssetTypeDataByName(name);
        if (item == undefined) {
            return;
        }

        if (remove) {
            var indexToRemove = _.findIndex($scope.assetTypesData, { value: item.value });
            $scope.assetTypesData.splice(indexToRemove, 1);
        } else {
            $scope.assetTypesData.push(item);
        }
    };

    $scope.removeAssignment = function (index) {
        var obj = $scope.assignmentsDisplayData[index];

        $scope.assignmentsDisplayData.splice(index, 1);

        $scope.updateAssetTypesData(obj.assetTypeName, false);
    };
    //remove specific item in assignment
    $scope.removeItemInAssignment = function (assignmentIndex, itemIndex) {
        var obj = $scope.assignmentsDisplayData[assignmentIndex];
        if (angular.isString(obj.assetsName))
            obj.assetsName = obj.assetsName.split(',');
        obj.assetsName.splice(itemIndex, 1);
        if (angular.isString(obj.assignmentsId))
            obj.assignmentsId = obj.assignmentsId.split(',');
        obj.assignmentsId.splice(itemIndex, 1);
      
        if(obj.assetsName.length == 0)
        {
            $scope.removeAssignment(assignmentIndex);
        }
    };

    $scope.resetValue = function () {
        $scope.identityCodesId = "";
        $scope.identityCodesName = "";
        $scope.assetTypeName = "";
    }

    $scope.validInput = function () {
        return !($scope.identityCodesId == "" || $scope.identityCodesName == "" || $scope.assetTypeName == "");
    }

    $scope.getAssetsByAssetType = function (id) {
        assetService.getAssetsByAssetType(id)
                    .then(function (response) {

                        for (var i in response.data) {
                            var row = response.data[i];
                            var extenObj = {
                                fullName: row.name + ' ' + row.identityCode
                            }

                            _.extend(row, extenObj);
                        }

                        $scope.assignmentData.referenceList = response.data;
                        $rootScope.$broadcast('assetsDataLoadedEvent');
                    }, function (error) {
                        toastr.error(error, "Loading Assets by Asset Type");
                    });
    };

    $scope.reset = function () {
        $scope.assignmentsDisplayData = $scope.assignmentsDisplayDataClone;
        $scope.assetsId = [];
        $scope.getAssetTypesData();
    };

    $scope.createAssignment = function (container) {
        var dimmer = $(container).children('.ui.dimmer');
        dimmer.addClass('active');

        for (var i in $scope.assignmentsDisplayData) {
            var item = $scope.assignmentsDisplayData[i];
            if (angular.isString(item.assignmentsId))
                item.assignmentsId = item.assignmentsId.split(',');
            var array = item.assignmentsId;
            for (var y in array) {

                var rs = _.find($scope.assetsId, function (id) {
                    return id == array[y];
                });

                if (!rs) {
                    $scope.assetsId.push(array[y]);
                }
            }
        }

        staffService.editAssignment($scope.staffId, $scope.assetsId)
                        .then(function (response) {
                            toastr.success("Successfully!", "Editing Assignment Asset");
                            dimmer.removeClass('active');
                        }, function (error) {
                            toastr.success(error, "Editing Assignment Asset");
                            dimmer.removeClass('active');
                        });
    };
}]);
