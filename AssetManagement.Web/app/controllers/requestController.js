﻿'use strict';
app.registerController('requestController', ['$scope', '$filter', '$location', '$routeParams', 'localStorageService', 'requestService', function ($scope, $filter, $location, $routeParams, localStorageService, requestService) {

    $scope.eventsData = [];
    $scope.succeed = false;

    $scope.addRequest = function (reason, start, end, leaveType) {
        var data = {
            reason: reason,
            start: start,
            end: end,
            leaveType: leaveType
        };

        return requestService.createRequest(data)
                             .then(function (response) {
                                 if (response.status == 200) {
                                     toastr.success("", "Create request successfully!");
                                 } else {
                                     toastr.error(response.data.message, "Error create request!");
                                 }

                                 return response;
                             });

    }

    $scope.init = function () {
        var path = $location.path();
        if (path === '/request') {
            requestService.getRequests()
                          .then(function (response) {
                              $scope.eventsData = response.data;
                          }, function (error) {
                              $scope.eventsData = [];
                              toastr.success("", "Error when gettting requests!");
                          });
        } else if (path.substring(0,16) === '/request/approve') {
            requestService.approve($routeParams.id)
                          .then(function (response) {
                              $scope.succeed = true;
                          }, function (error) {
                              $scope.eventsData = [];
                              toastr.success("", "Error when sending request!");
                          });
        } else if (path.substring(0, 13) === '/request/deny') {
            requestService.deny($routeParams.id)
                          .then(function (response) {
                              $scope.succeed = true;
                          }, function (error) {
                              $scope.eventsData = [];
                              toastr.success("", "Error when sending request!");
                          });
        }

    }

    $scope.init();
}]);