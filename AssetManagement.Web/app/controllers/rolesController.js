﻿'use strict';
app.registerController('rolesController', ['$scope', '$filter', 'localStorageService', 'rolesService', function ($scope, $filter, localStorageService, rolesService) {

    var LOCALSS_AUTHORIZATIONDATA = 'authorizationData';

    $scope.displayRoles = [].concat($scope.roles);
    $scope.roles = [];
    $scope.itemsByPage = 10;

    $scope.name = "";

    $scope.getAllRoles = function () {
        rolesService.getAllRoles()
                    .then(function (response) {
                        $scope.roles = response.data; //$filter('orderBy')(response, ['ModifiedDate != null ? ModifiedDate : CreatedDate', '-Id'], true);
                    });
    };

    /*
     * Instant call
     */
    $scope.getAllRoles();

    //$scope.assetTypes = localStorageService.get(LOCALSS_ASSETTYPEDATA);

    $scope.create = function ($event) {

        $($event.currentTarget).addClass('loading');

        rolesService.create($scope.name)
            .then(function (response) {
                $scope.roles.splice(0, 0, response.data);

                $($event.currentTarget).removeClass('loading');

                toastr.success("Create role successfully!");
            },
            function (err) {
                $($event.currentTarget).removeClass('loading');
                toastr.error("", "Creating role");
            });
    };

    //$scope.editAsset = function (id, name, type) {
    //    assetTypeService.editAsset(id, name, type)
    //        .then(function (response) {
    //            for (var index = 0; index < $scope.assets.length; index++) {
    //                if ($scope.assets[index].Id == id) {
    //                    $scope.assets[index].Name = name;
    //                    $scope.assets[index].Type = type;
    //                    break;
    //                }
    //            }

    //            toastr.success("", "Edit asset successfully!");
    //        },
    //        function (err) {
    //            toastr.error("", "Edit asset failed!");
    //        });
    //};

    $scope.deactiveOrDelete = function (id) {
        rolesService.deactiveOrDelete(id)
            .then(function (response) {
                toastr.success("", "Delete role successfully!");

                var indexToRemove = _.findIndex($scope.roles, { id: id });
                $scope.roles.splice(indexToRemove, 1);

            },
            function (err) {
                toastr.error(err, "Deactive Role");
            });
    };
}]);