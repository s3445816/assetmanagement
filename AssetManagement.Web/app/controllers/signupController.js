﻿'use strict';
app.registerController('signupController', ['$scope', '$location', '$timeout', 'oDataService', 'authService', 'staffService', function ($scope, $location, $timeout, oDataService, authService, staffService) {
    $scope.saveSuccessfully = false;
    $scope.message = '';

    $scope.registration = {
        userName: '',
        password: '',
        confirmPassword: '',
        staffId: ''
    };

    oDataService.lookup({
            entitySet: 'staffs'
        },
        function (response) {
            $scope.staffs = response.value;
        }
    );

    $scope.signUp = function () {
        $scope.registration.staffId = $('#dropdown').dropdown('get value');

        authService.saveRegistration($scope.registration)
                   .then(function (response) {
                       $scope.saveSuccessfully = true,
                       //Update success message in sign-up page
                       toastr.success('User has been registered successfully, you will be redicted to login page in 2 seconds.');
                       //$scope.message = 'User has been registered successfully, you will be redicted to login page in 2 seconds.';
                       startTimer();
                   }, function (response) {
                       var errors = [];
                       for (var key in response.data.modelState) {
                           for (var i = 0; i < response.data.modelState[key].length; i++) {
                               errors.push(response.data.modelState[key][i]+"\n");
                           }
                       }
                       errors.shift();
                       //Update error message in sign-up page
                       toastr.error(errors, 'Failed to register user : ');
                       //$scope.message = 'Failed to register user due to:' + errors.join(' ');
                   });
    };

    var startTimer = function () {
        var timer = $timeout(function () {
            $timeout.cancel(timer);
            $location.path('/login');
        }, 2000);
    };
}]);