﻿'use strict';
app.registerController('staffsController',
    ['$scope', '$rootScope', '$filter', 'staffService', 'officeService', 'authService',
        function ($scope, $rootScope, $filter, staffService, officeService, authService) {

    $scope.officesData = [];

    $scope.getOfficesData = function () {
        officeService.getAll()
                     .then(function (response) {
                         $scope.officesData = response.data;
                     }, function (error) {
                         toastr.error(error, "Loading Offices")
                     });
    };

    $scope.authentication = authService.authentication;

    $scope.getOfficesData();

    /*
     * Register staff
     */
    $scope.staffRegisterData = {
        name: '',
        ext: '',
        note: '',
        isExtDup: false,
        officeId: 0
    };

    $scope.resetRegisterStaff = function () {
        $scope.staffRegisterData = {
            name: '',
            ext: '',
            note: '',
            isExtDup: false,
            officeId: 0
        };
    };

    $scope.staffId = -1;

    $scope.registerStaff = function (form, ddOfficeId, mdlId) {
        $scope.staffRegisterData.isExtDup = false;
        var validated = $(form).form('is valid');
        if (!validated) {
            return false;
        }

        var dimmer = $(form).children('.ui.dimmer');
        dimmer.addClass('active');
                
        $scope.staffRegisterData.officeId = $(ddOfficeId).dropdown('get value');
        staffService.registerStaff($scope.staffRegisterData)
                    .then(function (response) {
                        if (response.status == 200 && response.data != -1)
                        {
                            dimmer.removeClass('active');
                        $scope.staffId = response.data;
                        //Appear comfirm form
                        $(mdlId).modal('setting', 'closable', false).modal('show');
                        }
                        else {
                            /*toastr.error("Exist ext number!", "Error");*/
                            dimmer.removeClass('active');
                            $scope.staffRegisterData.isExtDup = true;
                            return false;
                        }
                    }/*, function (error) {
                        toastr.error(error, "Existed Extnumber Staff");
                        dimmer.removeClass('active');
                    }*/
                    );
        
    };

    /*
     * Resigned a staff
     */
    $scope.resign = function (id, verificationCode) {
        staffService.resign(id, verificationCode)
                    .then(function (response) {
                        toastr.success("Successfully!", "Resignning Staff");
                        return true;
                    }, function (error) {
                        toastr.error(error, "Resignning Staff");
                        return false;
                    });
    }
    
}]);