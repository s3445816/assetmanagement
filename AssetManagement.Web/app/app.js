﻿var app = angular.module('app', ['ngRoute',
                                 'smart-table',
                                 'LocalStorageModule',
                                 'ngResource',
                                 'ngSanitize',
                                 'nvd3',
                                 'angular-loading-bar']);

app.config(function ($httpProvider, $routeProvider, $controllerProvider, $compileProvider, $filterProvider, $provide) {

    /* Register lazy loading for controllers, directives */
    app.registerController = $controllerProvider.register;
    app.registerDirective = $compileProvider.directive;
    app.registerFilter = $filterProvider.register;
    app.registerFactory = $provide.factory;

    $httpProvider.interceptors.push('authInterceptorService');

    /* Page */
    $routeProvider.when('/home', {
        controller: 'homeController',
        templateUrl: '/app/views/home.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/services/reminder-service.js',
                    '/app/services/activitylog-service.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    $routeProvider.when('/roles', {
        controller: 'rolesController',
        templateUrl: '/app/views/roles.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/services/roles-service.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    $routeProvider.when('/login', {
        controller: 'loginController',
        templateUrl: '/app/views/login.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/services/asset-type-service.js',
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    $routeProvider.when('/signup', {
        controller: 'signupController',
        templateUrl: '/app/views/signup.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/services/staff-service.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    /* Search */
    $routeProvider.when('/search/', {
        controller: 'searchController',
        templateUrl: '/app/views/search.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/services/office-service.js',
                    '/app/directives/staff.js',
                    '/app/services/staff-service.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    $routeProvider.when('/search/:keyword', {
        controller: 'searchController',
        templateUrl: '/app/views/search.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/services/office-service.js',
                    '/app/directives/staff.js',
                    '/app/services/staff-service.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    /* Staff */
    $routeProvider.when('/staffs', {
        controller: 'staffsController',
        templateUrl: '/app/views/staffs.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/services/staff-service.js',
                    '/app/directives/staff.js',
                    '/app/services/office-service.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    $routeProvider.when('/staffs/create', {
        controller: 'staffsController',
        templateUrl: '/app/views/create-staff.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/directives/staff.js',
                    '/app/services/staff-service.js',
                    '/app/services/office-service.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    /* Assignment Staff */
    $routeProvider.when('/staffs/assignment/create', {
        controller: 'staffAssignmentsController',
        templateUrl: '/app/views/create-assignment.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/directives/staff.js',
                    '/app/services/staff-service.js',
                    '/app/services/asset-service.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    $routeProvider.when('/staffs/assignment/create/:id', {
        controller: 'staffAssignmentsController',
        templateUrl: '/app/views/create-assignment.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/directives/staff.js',
                    '/app/services/staff-service.js',
                    '/app/services/asset-service.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    $routeProvider.when('/staffs/assignment/edit', {
        controller: 'staffEditAssignmentsController',
        templateUrl: '/app/views/edit-assignment.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/services/asset-service.js',
                    '/app/services/staff-service.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    $routeProvider.when('/staffs/assignment/edit/:id', {
        controller: 'staffEditAssignmentsController',
        templateUrl: '/app/views/edit-assignment.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/services/asset-service.js',
                    '/app/services/staff-service.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    /* Asset */
    $routeProvider.when('/assets', {
        controller: 'assetsController',
        templateUrl: '/app/views/assets.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/services/asset-service.js',
                    '/app/services/asset-type-service.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    $routeProvider.when('/assets/create', {
        controller: 'assetsController',
        templateUrl: '/app/views/create-asset.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/services/asset-service.js',
                    '/app/services/asset-type-service.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    $routeProvider.when('/assets/create/:id', {
        controller: 'assetsController',
        templateUrl: '/app/views/create-asset.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/services/asset-service.js',
                    '/app/services/asset-type-service.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    /* Asset Details */
    $routeProvider.when('/assetdetails', {
        controller: 'assetdetailsController',
        templateUrl: '/app/views/asset-details.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/services/history-service.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    /* Asset Type */
    $routeProvider.when('/assettypes', {
        controller: 'assettypesController',
        templateUrl: '/app/views/create-assettype.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/directives/asset.js',
                    '/app/services/asset-type-service.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    /* Reminder */
    $routeProvider.when('/reminders', {
        controller: 'remindersController',
        templateUrl: '/app/views/create-reminder.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/directives/reminder.js',
                    '/app/services/reminder-service.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    /* History */
    $routeProvider.when('/history', {
        controller: 'historyController',
        templateUrl: '/app/views/history.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/services/history-service.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    /* Request */
    $routeProvider.when('/request', {
        controller: 'requestController',
        templateUrl: '/app/views/request.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/services/request-service.js',
                    '/app/controllers/requestController.js',
                    '/Scripts/app.common.js',
                    '/Scripts/app.request.js',
                    '/app/directives/request.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    $routeProvider.when('/request/approve/:id', {
        controller: 'requestController',
        templateUrl: '/app/views/approve-request.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/services/request-service.js',
                    '/app/controllers/requestController.js',
                    '/Scripts/app.common.js',
                    '/Scripts/app.request.js',
                    '/app/directives/request.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    $routeProvider.when('/request/deny/:id', {
        controller: 'requestController',
        templateUrl: '/app/views/deny-request.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/services/request-service.js',
                    '/app/controllers/requestController.js',
                    '/Scripts/app.common.js',
                    '/Scripts/app.request.js',
                    '/app/directives/request.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    /* TimeSheet */
    $routeProvider.when('/timesheet', {
        controller: 'timesheetController',
        templateUrl: '/app/views/timesheet.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/services/timesheet-service.js',
                    '/app/controllers/timesheetController.js',
                    '/Scripts/app.common.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    /* Error Pages */
    $routeProvider.when('/error/forbidden', {
        controller: 'errorController',
        templateUrl: '/app/views/errors/forbidden.html',
        resolve: {
            deps: function ($q, $rootScope) {
                var deferred = $q.defer();
                var dependencies = [
                    '/app/controllers/errorController.js'
                ];

                // Load the dependencies
                $script(dependencies, function () {
                    // all dependencies have now been loaded by so resolve the promise
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });

                return deferred.promise;
            }
        }
    });

    $routeProvider.otherwise({ redirectTo: '/home' });
});

var serviceBase = 'http://localhost:52052/';
var serviceResourceBase = 'http://localhost:49966/';
app.constant('ngAuthSettings', {
    apiServiceBaseUri: serviceBase,
    apiServiceResourceBaseUri: serviceResourceBase,
    clientId: 'webApp',
    resourceClientId: "fdc3523dcae44ba797e31a0587266b81",
    LOCALSS_AUTHORIZATIONDATA: 'authorizationData'
});

app.run(['authService', '$rootScope', '$location', function (authService, $rootScope, $location) {
    authService.fillAuthData();

    $rootScope.$on('$routeChangeStart', function (e, next) {
        if (!authService.hasLogin() && $location.path() !== '/signup') {
            if ($location.path() !== '/login') {
                $rootScope.callbackUrl = $location.path();
            } else {
                $rootScope.callbackUrl = {};
            }

            $location.path('/login');
        }

        $('.ui.sidebar').sidebar('hide');
    });

}]);