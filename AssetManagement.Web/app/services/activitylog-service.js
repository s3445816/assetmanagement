﻿'use strict';
app.registerFactory('activityLogService', ['$http', '$q', 'oDataService', function ($http, $q, oDataService) {
    var serviceBase = 'http://localhost:49966/';
    var activityLogServiceFactory = {};
    var defaultFilter = '$top=20';

    var _getActivityLogs = function (filter) {
        filter = filter || defaultFilter;

        var deferred = $q.defer();
        oDataService.lookup(
            {
                entitySet: 'activitylogs', filter: filter
            },
            function (response) {
                deferred.resolve(response.value);
            },
            function (error) {
                deferred.reject(error);
            }
        );
        return deferred.promise;
    };

    activityLogServiceFactory.getActivityLogs = _getActivityLogs;

    return activityLogServiceFactory;
}]);