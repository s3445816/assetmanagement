﻿'use strict';
app.registerFactory('assetService', ['$http', '$q', 'localStorageService', function ($http, $q, localStorageService) {
    var serviceBase = 'http://localhost:49966/';
    var assetServiceFactory = {};
    var LOCALSS_AUTHORIZATIONDATA = 'authorizationData';

    var _getAssetTypes = function () {
        return $http.get(serviceBase + 'api/asset/GetAssetTypes')
                        .then(function (response) {
                            return response;
                        });
    };

    var _getAssetsByAssetType = function (id) {
        return $http.get(serviceBase + 'api/asset/GetAssetsByAssetType?id=' + id)
                        .then(function (response) {
                            return response;
                        });
    };

    var _createAssetsByBatch = function (id, codes) {
        var identityCodes = [];
        for (var key in codes) {
            identityCodes.push(codes[key].code);
        }
        return $http.post(serviceBase + 'api/asset/CreateAssetsByBatch', { AssetId: id, IdentityCodes: identityCodes })
                        .then(function (response) {
                            return response;
                        });
    }

    var _getLastAssignmentByAssetId = function (id) {
        return $http.get(serviceBase + 'api/asset/GetLastAssignmentByAssetId?id=' + id)
                        .then(function (response) {
                            return response;
                        });
    };

    var _getAssetTypeName = function (id) {
        return $http.get(serviceBase + 'api/asset/GetAssetTypeName?id=' + id)
                        .then(function (response) {
                            return response;
                        });
    }

    assetServiceFactory.getAssetTypes = _getAssetTypes;
    assetServiceFactory.getAssetsByAssetType = _getAssetsByAssetType;
    assetServiceFactory.createAssetsByBatch = _createAssetsByBatch;
    assetServiceFactory.getLastAssignmentByAssetId = _getLastAssignmentByAssetId;
    assetServiceFactory.getAssetTypeName = _getAssetTypeName;

    return assetServiceFactory;
}]);