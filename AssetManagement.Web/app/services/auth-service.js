﻿'use strict';
app.factory('authService', ['$http', '$q', '$route', 'localStorageService', 'ngAuthSettings', function ($http, $q, $route, localStorageService, ngAuthSettings) {

    var authServiceFactory = {};

    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var _authentication = {
        isAuth: false,
        userName: '',
        useRefreshToken: false
    };

    var _saveRegistration = function (registration) {
        _logOut();

        return $http.post(serviceBase + 'api/account/register', registration)
                        .then(function (response) {
                            return response;
                        });
    };

    var _logIn = function (loginData) {
        var data = "grant_type=password&username=" + loginData.userName
                        + "&password=" + loginData.password
                        + "&client_id=" + ngAuthSettings.clientId
                        + "&resource_client_id=" + ngAuthSettings.resourceClientId;

        var deferred = $q.defer();

        $http.post(serviceBase + 'oauth/token', data, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            ignoreLoadingBar: true
        }).success(function (response) {
            localStorageService.set(ngAuthSettings.LOCALSS_AUTHORIZATIONDATA, {
                token: response.access_token,
                userName: response.username,
                refreshToken: response.refresh_token,
                useRefreshToken: true
            });

            _setAuthData(true, response.username, true);

            deferred.resolve(response);

        }).error(function (err) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;
    };

    var _logOut = function () {
        localStorageService.remove(ngAuthSettings.LOCALSS_AUTHORIZATIONDATA);
        _setAuthData(false, "", false);
    };

    var _fillAuthData = function () {
        var authData = localStorageService.get(ngAuthSettings.LOCALSS_AUTHORIZATIONDATA);
        if(authData)
        {
            _setAuthData(true, authData.userName, true);
        }
        else {
            _setAuthData(false, "", false);
        }
    }

    var _hasLogin = function () {
        var foundAuthData = localStorageService.get(ngAuthSettings.LOCALSS_AUTHORIZATIONDATA);
        return foundAuthData != null;
    }

    var _setAuthData = function (isAuth, userName, useRefreshToken) {
        _authentication.isAuth = isAuth;
        _authentication.userName = userName;
        _authentication.useRefreshToken = useRefreshToken;
    };

    var _refreshToken = function () {
        
        var authData = localStorageService.get(ngAuthSettings.LOCALSS_AUTHORIZATIONDATA);
        if (authData && authData.useRefreshToken) {
            var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken
                        + "&client_id=" + ngAuthSettings.clientId
                        + "&resource_client_id=" + ngAuthSettings.resourceClientId;

            localStorageService.remove(ngAuthSettings.LOCALSS_AUTHORIZATIONDATA);

            var deferred = $q.defer();

            $http.post(serviceBase + 'oauth/token',
                       data,
                       {
                           headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                       })
                .success(function (response) {
                    localStorageService.set(ngAuthSettings.LOCALSS_AUTHORIZATIONDATA, {
                        token: response.access_token,
                        userName: response.userName,
                        refreshToken: response.refresh_token,
                        useRefreshToken: true
                    });

                    _setAuthData(true, response.username, true);

                    deferred.resolve(response);

                    $route.reload();

                }).error(function (err, status) {
                    _logOut();
                    deferred.reject(err);
                });
        }

        return deferred.promise;
    }

    authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.logIn = _logIn;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;
    authServiceFactory.hasLogin = _hasLogin;
    authServiceFactory.refreshToken = _refreshToken;

    return authServiceFactory;
}]);