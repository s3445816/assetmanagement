﻿'use strict';
app.registerFactory('historyService', ['$http', '$q', function ($http, $q) {
    var serviceBase = 'http://localhost:49966/';
    var historyServiceFactory = {};

    var _getHistory = function (id, name) {
        
        var deferred = $q.defer();

        $http.get(serviceBase + 'api/History/GetHistory?id=' + id + '&name=' + name)
            .success(function (response) {
                console.log(response);
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });

        return deferred.promise;
    }

    var _getStaffHistory = function (id) {

        var deferred = $q.defer();

        $http.get(serviceBase + 'api/History/GetStaffHistory?id=' + id)
            .success(function (response) {
                console.log(response);
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });

        return deferred.promise;
    }

    var _getAssetHistory = function (id) {

        var deferred = $q.defer();

        $http.get(serviceBase + 'api/History/GetAssetHistory?id=' + id)
            .success(function (response) {
                console.log(response);
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });

        return deferred.promise;
    }

    historyServiceFactory.getHistory = _getHistory;
    historyServiceFactory.getStaffHistory = _getStaffHistory;
    historyServiceFactory.getAssetHistory = _getAssetHistory;

    return historyServiceFactory;
}]);