﻿'use strict';
app.factory('oDataService', function ($resource) {
    var serviceBase = 'http://localhost:49966/';
    var oDataUrl = serviceBase + 'odata/';

    var defaultParams = {};

    var actions = {
        lookup: {
            method: 'GET',
            url: oDataUrl + ':entitySet?:filter'
        }
    };

    return $resource(oDataUrl, defaultParams, actions);
})