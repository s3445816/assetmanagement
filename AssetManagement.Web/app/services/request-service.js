﻿'use strict';
app.registerFactory('requestService', ['$http', '$q', 'localStorageService', 'ngAuthSettings', function ($http, $q, localStorageService, ngAuthSettings) {
    var serviceBase = ngAuthSettings.apiServiceResourceBaseUri;
    var serviceFactory = {};
    var LOCALSS_AUTHORIZATIONDATA = 'authorizationData';

    var _createRequest = function (data) {
        return $http.post(serviceBase + 'api/request/create', data)
                    .then(function (success) { return success; },
                          function (error) { return error; });
    }

    var _getRequests = function () {
        return $http.get(serviceBase + 'api/request/events')
                        .then(function (response) {
                            return response;
                        }, function (error) {
                            return error;
                        });
    }

    var _approve = function (id) {
        return $http.get(serviceBase + 'api/request/approve?id=' + id)
                        .then(function (response) {
                            return response;
                        }, function (error) {
                            return error;
                        });
    }

    var _deny = function (id) {
        return $http.get(serviceBase + 'api/request/deny?id=' + id)
                        .then(function (response) {
                            return response;
                        }, function (error) {
                            return error;
                        });
    }

    serviceFactory.createRequest = _createRequest;
    serviceFactory.getRequests = _getRequests;
    serviceFactory.approve = _approve;
    serviceFactory.deny = _deny;

    return serviceFactory;
}]);