﻿'use strict';
app.registerFactory('officeService', ['$http', '$q', 'localStorageService', function ($http, $q, localStorageService) {
    var serviceBase = 'http://localhost:49966/';
    var officeServiceFactory = {};

    var _getAll = function () {
        return $http.get(serviceBase + 'api/office/all')
                        .then(function (response) {
                            console.log(response);
                            return response;
                        });
    };

    officeServiceFactory.getAll = _getAll;

    return officeServiceFactory;
}]);