﻿'use strict';
app.factory('authInterceptorService', ['$q', '$location', 'localStorageService', '$injector', 'ngAuthSettings', function ($q, $location, localStorageService, $injector, ngAuthSettings) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {

        config.headers = config.headers || {};

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            config.headers.Authorization = 'Bearer ' + authData.token;
        }
        config.useXDomain = true;
        config.headers['Access-Control-Allow-Origin'] = '*';
        return config;
    }

    var _responseError = function (rejection) {
        if (rejection.status === 401) {
            var authService = $injector.get('authService');
            var authData = localStorageService.get(ngAuthSettings.LOCALSS_AUTHORIZATIONDATA);

            if (authData && authData.useRefreshToken) {
                authService.refreshToken();
            }

        } else if (rejection.status === 403) {
            $location.path('/error/forbidden');
        }

        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);