﻿'use strict';
app.factory('baseService', ['$http', '$q', 'localStorageService', function ($http, $q, localStorageService) {
    var serviceBase = 'http://localhost:49966/';
    var serviceFactory = {};
    var LOCALSS_AUTHORIZATIONDATA = 'authorizationData';

    var _findItemById = function (collection, id) {
        var rs = _.find(collection, function (item) {
            return item.Id == id || item.id == id;
        });

        return rs;
    };

    serviceFactory.findItemById = _findItemById;

    return serviceFactory;
}]);


