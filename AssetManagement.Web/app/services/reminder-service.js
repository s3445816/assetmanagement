﻿'use strict';
app.registerFactory('reminderService', ['$http', '$q', 'localStorageService', 'oDataService', function ($http, $q, localStorageService, oDataService) {
    var serviceBase = 'http://localhost:49966/';
    var reminderServiceFactory = {};

    var _getIncompleted = function (query) {
        if (!query) {
            return;
        }

        var deferred = $q.defer();

        oDataService.lookup(
            {
                entitySet: 'reminders', filter: query
            },
            function (response) {
                deferred.resolve(response.value);
            },
            function (error) {
                deferred.reject(error);
            }
        );

        return deferred.promise;
    };

    var _complete = function (reminderIds) {
        var deferred = $q.defer();

        $http.post(serviceBase + 'api/Reminder/Complete', reminderIds)
            .success(function (response) {
                deferred.resolve(response);
            }).error(function (error) {
                deferred.reject(error);
            });

        return deferred.promise;
    };

    var _addNewReminder = function (timestamp, duetime, content) {
        var deferred = $q.defer();
        $http.post(serviceBase + 'api/Reminder/AddNewReminder',
            {
                TimeStamp: timestamp,
                DueTime: duetime,
                Content: content
            })
            .success(function (response) {
                deferred.resolve(response);
            })
            .error(function (err) {
                deferred.reject(err);
            });
        return deferred.promise;
    };

    var _updateReminder = function (id, timestamp, duetime, content, completed) {
        var deferred = $q.defer();
        $http.post(serviceBase + 'api/Reminder/UpdateReminder',
            {
                Id: id,
                TimeStamp: timestamp,
                DueTime: duetime,
                Content: content,
                Completed: completed
            })
            .success(function (response) {
                deferred.resolve(response);
            })
            .error(function (err) {
                deferred.reject(err);
            });
        return deferred.promise;
    }

    var _deleteReminder = function (id) {
        var deferred = $q.defer();
        $http.post(serviceBase + 'api/Reminder/DeleteReminder', id)
            .success(function (response) {
                deferred.resolve(response);
            })
            .error(function (err) {
                deferred.reject(err);
            });
        return deferred.promise;
    }

    reminderServiceFactory.getReminders = _getIncompleted;
    reminderServiceFactory.complete = _complete;
    reminderServiceFactory.addNewReminder = _addNewReminder;
    reminderServiceFactory.updateReminder = _updateReminder;
    reminderServiceFactory.deleteReminder = _deleteReminder;

    return reminderServiceFactory;
}]);