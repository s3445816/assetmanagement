﻿'use strict';
app.registerFactory('staffService', ['$http', '$q', 'localStorageService', function ($http, $q, localStorageService) {
    var serviceBase = 'http://localhost:49966/';
    var staffServiceFactory = {};
    var LOCALSS_USEDEXTLISTDATA = 'usedExtListData';

    var _registerStaff = function (staffRegisterData) {
        return $http.post(serviceBase + 'api/staff/create', staffRegisterData, {
                            ignoreLoadingBar: true
                        })
                        .then(function (success) {return success;},
                              function (error) { return error }
                              );
    };

    var _resign = function (id, verificationCode) {

        var deferred = $q.defer();

        $http.post(serviceBase + 'api/staff/resign', id, {
            headers: {
                'X-OTP': verificationCode
            }
        }).success(function (response) {            
            deferred.resolve(response);
        }).error(function (error) {
            deferred.reject(error);
        });

        return deferred.promise;
    };

    var _changeOffice = function (staffOfficeModel) {
        var deferred = $q.defer();

        $http.post(serviceBase + 'api/staff/changeOffice', staffOfficeModel)
            .success(function (response) {
                deferred.resolve(response);
            }).error(function (error) {
                deferred.reject(error);
            });

        return deferred.promise;
    }

    var _createAssignment = function (staffId, assetsId) {
        var deferred = $q.defer();

        var model = {
            staffId: staffId,
            assetsId: assetsId
        }

        $http.post(serviceBase + 'api/staff/assign', model)
            .success(function (response) {
                deferred.resolve(response);
            }).error(function (error) {
                deferred.reject(error);
            });

        return deferred.promise;
    }

    var _editAssignment = function (staffId, assetsId) {
        var deferred = $q.defer();

        var model = {
            staffId: staffId,
            assetsId: assetsId
        }

        $http.post(serviceBase + 'api/staff/editAssign', model)
            .success(function (response) {
                deferred.resolve(response);
            }).error(function (error) {
                deferred.reject(error);
            });

        return deferred.promise;
    }

    var _getAssignments = function (id) {
        var deferred = $q.defer();

        $http.get(serviceBase + 'api/staff/GetAssignments?id=' + id)
            .success(function (response) {
                deferred.resolve(response);
            }).error(function (error) {
                deferred.reject(error);
            });

        return deferred.promise;
    }

    var _getStaffName = function (id) {
        var deferred = $q.defer();

        $http.get(serviceBase + 'api/staff/GetStaffName?id=' + id)
            .success(function (response) {
                deferred.resolve(response);
            }).error(function (error) {
                deferred.reject(error);
            });

        return deferred.promise;
    }

    staffServiceFactory.registerStaff = _registerStaff;
    staffServiceFactory.resign = _resign;
    staffServiceFactory.changeOffice = _changeOffice;
    staffServiceFactory.createAssignment = _createAssignment;
    staffServiceFactory.editAssignment = _editAssignment;
    staffServiceFactory.getAssignments = _getAssignments;
    staffServiceFactory.getStaffName = _getStaffName;

    return staffServiceFactory;
}]);