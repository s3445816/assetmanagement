﻿'use strict';
app.registerFactory('rolesService', ['$http', '$q', 'localStorageService', function ($http, $q, localStorageService) {
    var serviceBase = 'http://localhost:49966/';
    var serviceFactory = {};
    var LOCALSS_AUTHORIZATIONDATA = 'authorizationData';

    var _getAllRoles = function () {
        return $http.get(serviceBase + 'api/roles')
                        .then(function (response) {
                            return response;
                        });
    };

    //var _getAssetsByAssetType = function (id) {
    //    return $http.get(serviceBase + 'api/asset/GetAssetsByAssetType?id=' + id)
    //                    .then(function (response) {
    //                        return response;
    //                    });
    //};

    var _create = function (name) {
        return $http.post(serviceBase + 'api/roles/create', { Name: name })
                        .then(function (response) {
                            return response;
                        });
    }

    //var _getLastAssignmentByAssetId = function (id) {
    //    return $http.get(serviceBase + 'api/asset/GetLastAssignmentByAssetId?id=' + id)
    //                    .then(function (response) {
    //                        return response;
    //                    });
    //};

    var _deactiveOrDelete = function (id) {
        return $http.delete(serviceBase + 'api/roles/', id)
                        .then(function (response) {
                            return response;
                        });
    }

    serviceFactory.getAllRoles = _getAllRoles;
    serviceFactory.create = _create;
    serviceFactory.deactiveOrDelete = _deactiveOrDelete;

    return serviceFactory;
}]);