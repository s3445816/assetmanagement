﻿'use strict';
app.registerFactory('assetTypeService', ['$http', '$q', 'localStorageService', 'oDataService', function ($http, $q, localStorageService, oDataService) {
    var serviceBase = 'http://localhost:49966/';
    var assetTypeServiceFactory = {};
    var LOCALSS_AUTHORIZATIONDATA = 'authorizationData';

    var _getAssets = function () {
        var deferred = $q.defer();
        oDataService.lookup(
            {
                entitySet: 'assets',
                filter: '$filter=IsDeleted eq false'
            },
            function (response) {
                deferred.resolve(response.value);
            },
            function (error) {
                deferred.reject(error);
            }
        );
        return deferred.promise;
    };

    var _getAssetTypes = function () {
        return $http.get(serviceBase + 'api/Asset/GetAssetTypes')
                .then(function (success) { return success; },
                        function (error) { return error; });
    };

    var _addNewAsset = function (name, type, note) {
        return $http.post(serviceBase + 'api/Asset/AddNewAsset',
                {
                    Name: name,
                    Type: type,
                    Note: note
                })
                .then(function (success) { return success; },
                        function (error) { return error; });
    };

    var _editAsset = function (id, name, type, note) {
        return $http.post(serviceBase + 'api/Asset/EditAsset',
                {
                    Id: id,
                    Name: name,
                    Type: type,
                    Note: note
                })
                .then(function (success) { return success; },
                        function (error) { return error; });
    }

    var _deleteAsset = function (id) {
        return $http.post(serviceBase + 'api/Asset/DeleteAsset', id)
                    .then(function (success) { return success; },
                            function (error) { return error; });
    }

    assetTypeServiceFactory.getAssets = _getAssets;
    assetTypeServiceFactory.getAssetTypes = _getAssetTypes;
    assetTypeServiceFactory.addNewAsset = _addNewAsset;
    assetTypeServiceFactory.editAsset = _editAsset;
    assetTypeServiceFactory.deleteAsset = _deleteAsset;

    return assetTypeServiceFactory;
}]);