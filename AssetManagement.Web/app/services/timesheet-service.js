﻿'use strict';
app.registerFactory('timesheetService', ['$http', '$q', 'localStorageService', 'ngAuthSettings', function ($http, $q, localStorageService, ngAuthSettings) {
    var serviceBase = ngAuthSettings.apiServiceResourceBaseUri;
    var serviceFactory = {};
    var LOCALSS_AUTHORIZATIONDATA = 'authorizationData';

    var _getFirstCheckInToday = function () {
        return $http.get(serviceBase + 'api/timesheet/GetFirstCheckInToDay')
                        .then(function (response) { return response; }
                             ,function (error) { return error; });
    }

    var _getCheckInFromBeginingOfMonth = function () {
        return $http.get(serviceBase + 'api/timesheet/GetCheckInFromBeginingOfMonth')
                        .then(function (response) { return response; }
                             , function (error) { return error; });
    }

    serviceFactory.getFirstCheckInToday = _getFirstCheckInToday;
    serviceFactory.getCheckInFromBeginingOfMonth = _getCheckInFromBeginingOfMonth;

    return serviceFactory;
}]);