﻿app.registerDirective('addRequestForm', function () {
    return {
        restrict: 'A',
        scope: {},
        link: function (scope, element, attrs) {
            $(element)
                .form({
                    fields: {
                        reason: {
                            identifier: 'reason',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please enter request\'s reason'
                                },
                                {
                                    type: 'maxLength[49]',
                                    prompt: 'Name must be less than or equal {ruleValue} characters'
                                }
                            ]
                        },
                        ddLeaveType: {
                            identifier: 'ddLeaveType',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please select a leaving type'
                                }
                            ]
                        }
                    },
                    inline: true
                });
        }
    }
});

app.registerDirective('calender', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        scope: {
            events: "=ngModel",
            fn: '&'
        },
        link: function (scope, element, attrs) {
            
            var ngModel = $parse(attrs.ngModel);
            var colorAL = '#ffd08c';
            var textColorAL = '#5C2C00';
            var colorSL = '#FECED6';
            var textColorSL = '#8F3A45';
            var colorPL = '#BBF0D8';
            var textColorPL = '#58A47F';


            scope.$watchCollection("events", function (newVal, oldVal) {
                if (newVal != oldVal) {
                    console.log("![]");
                    ngModel = newVal;

                    _.each(ngModel, function (item) {
                        var color = getColor(item.leaveType);

                        item.start = moment.utc(item.start).toDate();
                        item.end = moment.utc(item.end).toDate();

                        _.extend(item, { color: color.bgColor });
                        _.extend(item, { textColor: color.textColor });
                        
                    });

                }

                init();

            });

            function init() {
                $('#' + attrs.id).fullCalendar('destroy');
                $('#' + attrs.id).fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },
                    displayEventEnd: true,
                    timeZone: true,
                    selectable: true,
                    selectHelper: true,
                    minTime: '08:00:00',
                    maxTime: '19:00:00',
                    select: function (start, end) {

                        var title = "";

                        $('#mdlAddRequest').modal({
                            onApprove: function ($element) {
                                var form = $('#mdlAddRequest').find('.form');

                                var validated = form.form('is valid');
                                if (!validated) return false;

                                var name = form.find('input[type=text]').val();
                                var type = $('#ddLeaveType').dropdown('get value');

                                eventData = createEventData(name, start, end, type);

                                scope.fn({
                                    reason: name,
                                    start: start.format(),
                                    end: end.format(),
                                    leaveType: type
                                }).then(function (response) {
                                    if (response.status == 200) {
                                        $('#' + attrs.id).fullCalendar('renderEvent', eventData, true); // stick? = true
                                    }
                                });
                            }
                        }).modal('setting', 'closable', false).modal('show');

                        $('#' + attrs.id).fullCalendar('unselect');
                    },
                    editable: false,
                    eventLimit: true, // allow "more" link when too many events
                    eventSources: [
                        {
                            events: ngModel,
                            color: '#ffd08c',   // an option!
                            textColor: '#5C2C00' // an option!
                        }
                    ]
                    
                });
            }

            function createEventData(title, start, end, leaveType) {

                var color = getColor(leaveType);

                var eventData = {
                    title: title,
                    start: start,
                    end: end,
                    color: color.bgColor,
                    textColor: color.textColor
                };

                return eventData;
            }

            function getColor(leaveType)
            {
                var color = {
                    bgColor: '',
                    textColor: ''
                };

                if(leaveType == 1 || leaveType == 'AL') {
                    color.bgColor = colorAL;
                    color.textColor = textColorAL;
                } else if (leaveType == 2 || leaveType == 'SL') {
                    color.bgColor = colorSL;
                    color.textColor = textColorSL;
                } else if (leaveType == 3 || leaveType == 'PL') {
                    color.bgColor = colorPL;
                    color.textColor = textColorPL;
                }

                return color;
            }
        }
    }
}]);