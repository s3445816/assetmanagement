﻿/* Directive: change-office */
app.registerDirective('changeOffice', function () {
    return {
        restrict: 'A',
        scope: {
            targetId: '@',
            dropdownId: '@',
            fn: '&'
        },
        link: function (scope, element, attrs) {
            element.bind('click', function () {
                var id = attrs.id;
                var officeId = attrs.officeId;

                console.log("[Change-Office] click event");
                console.log("Id: " + id + ", Office Id: " + officeId);

                var modalObj = $('#' + scope.targetId);
                var formObj = modalObj.find('form');

                formObj.form('reset');

                $('#' + scope.dropdownId).dropdown('set selected', officeId);

                modalObj.modal({
                    onApprove: function () {
                        var newOfficeId = $('#' + scope.dropdownId).dropdown('get value');
                        console.log("newOfficeId: " + newOfficeId);

                        scope.fn({ id: id, officeId: newOfficeId });
                    }
                }).modal('setting', 'closable', false).modal('show');
            });
        }
    };
});

/* Directive assignmentForm */
app.registerDirective('assignmentForm', function () {
    return {
        restrict: 'A',
        scope: {},
        link: function (scope, element, attrs) {
            $(element)
                .form({
                    fields: {
                        staffId: {
                            identifier: 'staffId',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please enter staff name'
                                }
                            ]
                        },
                        assetTypeId: {
                            identifier: 'assetTypeId',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please select an asset type'
                                }
                            ]
                        },
                        assets: {
                            identifier: 'assets',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please select an asset to assign to staff'
                                }
                            ]
                        }
                    },
                    inline: true
                });
        }
    }
})

/* Directive -a bd-staff-search */
app.registerDirective('bdStaffSearch', function ($timeout) {
    return {
        restrict: 'A',
        scope: {
            staffId: '=',
            event: '@'
        },
        link: function (scope, element, attrs) {
            $(element)
                .dropdown({
                    apiSettings: {
                        onResponse: function (dataResponse) {
                            var response = {
                                results: []
                            };

                            $.each(dataResponse.value, function (index, item) {
                                var maxResults = 8;
                                if (index >= maxResults) {
                                    return false;
                                }

                                // add result to category
                                response.results.push({
                                    name: item.Name,
                                    value: item.Id
                                });
                            });

                            return response;
                        },
                        url: attrs.url
                    },
                    onChange: function (result, response) {
                        /* result: Id
                            response: Name */
                        $timeout(function () {
                            scope.staffId = result;
                        });
                    }
                });

            scope.$on(scope.event, function (event, obj) {
                $(element).dropdown('set value', scope.staffId);
                $(element).dropdown('set text', obj.message);
            });
        }
    };
})

/* Directive: <dropdown> */
app.registerDirective("dropdown", function ($timeout) {
    return {
        restrict: 'E',
        scope: {
            defaultText: '@',
            items: '=',
            text: '@',
            value: '@',
            multiple: '@',
            eventTracking: '@',
            bypassEvent: '@',
            fn: '&',
            objValue: '=',
            objName: '=',
            cssClass: '@',
            id: '@'
        },
        templateUrl: '../app/templates/dropdown.html',
        link: function link(scope, element, attrs) {
            var dd = element.children('.dropdown');
            var disabledClass = 'disabled';

            scope.$watch('items', function () {
                scope.items.length == 0 ? dd.addClass(disabledClass) : dd.removeClass(disabledClass);
            });

            scope.$on(scope.eventTracking, function () {
                console.log(scope.eventTracking + ' is triggered and items length: ' + scope.items.length);

                dd.dropdown('restore defaults').dropdown('destroy').dropdown('setting', {
                    onChange: function (val, text, $selectedItem) {
                        if (val == '') return;

                        console.log("[onChange] dropdown event is trigger. Value: " + val);

                        if (scope.objValue != undefined) {
                            $timeout(function () {
                                scope.objValue = val;
                            });
                        }

                        if (scope.objName != undefined) {
                            var texts = text;

                            if (scope.multiple != undefined) {
                                var vals = val.split(',');
                                var tmps = {};

                                _.each(vals, function (v) {
                                    tmps[v] = true;
                                });

                                var selectedItems = _.filter(scope.items, function (item) {
                                    return tmps[item.id];
                                }, vals);

                                texts = [];
                                for (var i = 0; i < selectedItems.length; i++) {
                                    texts.push(selectedItems[i].name + ' ' + selectedItems[i].identityCode);
                                }
                            }

                            $timeout(function () {
                                scope.objName = texts;
                            });
                        }

                        scope.fn({ id: val });
                    }
                });
            });

            scope.$on(scope.bypassEvent, function () {
                console.log(scope.bypassEvent + ' is triggered');
                dd.dropdown('clear');
            });
        }
    };
});

/* Directive: resign-staff */
app.registerDirective('resignStaff', function () {
    return {
        restrict: 'A',
        scope: {
            targetId: '@',
            fn: '&'
        },
        link: function (scope, element, attrs) {
            element.bind('click', function () {
                var id = attrs.id;

                console.log("[Resign-Staff] click event");

                var modalObj = $('#' + scope.targetId);
                var formObj = modalObj.find('form');

                formObj.form('reset');

                modalObj.modal({
                    onApprove: function () {
                        var verificationCode = formObj.find('input[type=text]').val();

                        scope.$apply(function () {
                            var result = scope.fn({ id: id, verificationCode: verificationCode });
                            return result;
                        });
                    }
                }).modal('setting', 'closable', false).modal('show');
            });
        }
    };
});

/* Directive: register-staff-form */
app.registerDirective('registerStaffForm', function () {
    return {
        restrict: 'A',
        scope: {},
        link: function (scope, element, attrs) {
            $(element)
                .form({
                    fields: {
                        name: {
                            identifier: 'name',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please enter staff name'
                                },
                                {
                                    type: 'maxLength[49]',
                                    prompt: 'Name must be less than or equal {ruleValue} characters'
                                }
                            ]
                        },
                        ext: {
                            identifier: 'ext',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please enter staff EXT'
                                },
                                {
                                    type: 'maxLength[4]',
                                    prompt: 'EXT must be less than or equal {ruleValue} characters'
                                }
                            ]
                        },
                        officeId: {
                            identifier: 'officeId',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please select building staff join'
                                }
                            ]
                        }
                    },
                    inline: true
                });
        }
    }
});