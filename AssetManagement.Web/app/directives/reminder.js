﻿/* Lazy Loading for Directives */

app.registerDirective('createReminder', function () {
    return {
        restrict: 'A',
        scope: {
            targetId: '@',
            txtId: '@',
            picker1Id: '@',
            picker2Id: '@',
            fn: '&'
        },
        link: function (scope, element, attrs) {
            element.bind('click', function () {

                var modalObj = $('#' + scope.targetId);
                var dtPicker1 = $('#' + scope.picker1Id);
                var dtPicker2 = $('#' + scope.picker2Id);
                var txtContent = $('#' + scope.txtId);

                dtPicker1.datetimepicker();
                dtPicker2.datetimepicker();

                dtPicker1.data('datetimepicker').setDate(null);
                dtPicker2.data('datetimepicker').setDate(null);
                txtContent.val('')

                modalObj.modal({
                    onApprove: function () {
                        var timeStamp = dtPicker1.data('datetimepicker').getDate();
                        var dueTime = dtPicker2.data('datetimepicker').getDate();
                        var content = txtContent.val();

                        scope.fn({ timestamp: timeStamp, duetime: dueTime, content: content });
                    }
                }).modal('setting', 'closable', false).modal('show');
            });
        }
    };
});

app.registerDirective('editReminder', function () {
    return {
        restrict: 'A',
        scope: {
            targetId: '@',
            txtId: '@',
            picker1Id: '@',
            picker2Id: '@',
            cboId: '@',
            fn: '&'
        },
        link: function (scope, element, attrs) {
            element.bind('click', function () {
                var id = attrs.id;
                var timestamp = attrs.timestamp;
                var duetime = attrs.duetime;
                var content = attrs.content;

                var modalObj = $('#' + scope.targetId);
                var dtPicker1 = $('#' + scope.picker1Id);
                var dtPicker2 = $('#' + scope.picker2Id);
                var txtContent = $('#' + scope.txtId);
                var cboCompleted = $('#' + scope.cboId);

                dtPicker1.datetimepicker();
                dtPicker2.datetimepicker();

                timestamp = timestamp.substring(1, timestamp.length - 1);
                duetime = duetime.substring(1, duetime.length - 1);

                dtPicker1.data('datetimepicker').setLocalDate(new Date(timestamp));
                dtPicker2.data('datetimepicker').setLocalDate(new Date(duetime));
                txtContent.val(content);

                cboCompleted.unbind('click');
                cboCompleted.bind('click', function (event) {
                    var target = $(event.currentTarget);
                    var unCheckClass = 'square outline';
                    var checkClass = 'check square orange';

                    if (!target.hasClass(unCheckClass)) {
                        var tmp = checkClass;
                        checkClass = unCheckClass;
                        unCheckClass = tmp;
                    }

                    target.removeClass(unCheckClass);
                    target.addClass(checkClass);
                });

                modalObj.modal({
                    onApprove: function () {
                        var timeStamp = dtPicker1.data('datetimepicker').getLocalDate();
                        var dueTime = dtPicker2.data('datetimepicker').getLocalDate();
                        var content = txtContent.val();
                        var completed = cboCompleted.hasClass('check square orange');

                        scope.fn({
                            id: id,
                            timestamp: timeStamp,
                            duetime: dueTime,
                            content: content,
                            completed: completed
                        });
                    }
                }).modal('setting', 'closable', false).modal('show');
            });
        }
    };
});