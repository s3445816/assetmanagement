﻿/* Lazy Loading for Directives */

app.registerDirective('editAsset', function () {
    return {
        restrict: 'A',
        scope: {
            targetId: '@',
            txtId: '@',
            txtNoteId: '@',
            dropdownId: '@',
            fn: '&'
        },
        link: function (scope, element, attrs) {
            element.bind('click', function () {
                var id = attrs.id;
                var name = attrs.name;
                var type = attrs.type;
                var note = attrs.note;

                var modalObj = $('#' + scope.targetId);

                var formObj = modalObj.find('form');
                formObj.form('reset');

                $('#' + scope.txtId).val(name);
                $('#' + scope.dropdownId).dropdown('set selected', type);
                $('#' + scope.txtNoteId).val(note);

                modalObj.modal({
                    onApprove: function () {
                        var newAssetName = $('#' + scope.txtId).val();
                        var newAssetType = $('#' + scope.dropdownId).dropdown('get value');
                        var newAssetNote = $('#' + scope.txtNoteId).val();
                        scope.fn({ id: id, name: newAssetName, type: newAssetType, note: newAssetNote });
                    }
                }).modal('setting', 'closable', false).modal('show');
            });
        }
    };
});