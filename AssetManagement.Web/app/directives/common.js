﻿/* This directive allows us to pass a function in on an enter key to do what we want. */
app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind('keypress', function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

/* Simple directive checkbox binding UI */
app.directive('bdCheckbox', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            $('.ui.checkbox').checkbox();
        }
    }
});

/* Simple directive dropdown binding UI */
app.directive('bdDropdown', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            $('.dropdown').dropdown({
                // you can use any ui transition
                transition: 'drop'
            });
        }
    }
});

/* Global search directive */
app.directive('bdGlobalSearch', function () {
    return {
        restrict: 'A',
        scope: {
            fn: '&',
            results: '@',
            title: '@'
        },
        link: function (scope, element, attrs) {
            $(element).search({
                apiSettings: {
                    url: 'http://localhost:49966/odata/staffs?$filter=contains(Name,\'{query}\')'
                },
                fields: {
                    results: scope.results,
                    title: scope.title
                },
                minCharacters: 1,
                onSelect: function (result, response) {
                    var self = $(this);
                    scope.fn(result.Name, self.context.id);
                }
            });
        }
    }
});

/* Delete modal directive */
app.directive('deleteModal', function () {
    return {
        restrict: 'A',
        scope: {
            targetId: '@',
            fn: '&'
        },
        link: function (scope, element, attrs) {
            element.bind('click', function () {
                var id = attrs.id;

                var modalObj = $('#' + scope.targetId);

                modalObj.modal({
                    onApprove: function () {
                        scope.fn({ id: id });
                    }
                }).modal('setting', 'closable', false).modal('show');
            });
        }
    };
});