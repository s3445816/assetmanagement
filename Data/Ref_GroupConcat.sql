USE [AssetManagement]
GO

DECLARE @CountStaff INT
EXEC [uspGetSummaryStaff] 0, '1,2', 2, 10, @CountStaff OUTPUT
SELECT @CountStaff

/****** Object:  StoredProcedure [dbo].[uspGetSummaryStaff]    Script Date: 26-Oct-15 9:00:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[uspGetSummaryStaff]
	 @Resigned				BIT
	,@OfficeIds				VARCHAR(MAX) = NULL
	,@PageIndex				SMALLINT	 = 1
	,@NumberOfItemInPage	SMALLINT	 = 25
	,@CountStaff			INT	OUTPUT
AS
BEGIN
SET NOCOUNT ON

DECLARE @SkipRec INT
DECLARE @Take	 INT

SET @SkipRec = (@PageIndex - 1) * @NumberOfItemInPage;
SET @Take = @NumberOfItemInPage + @SkipRec;

DECLARE @Table	 TABLE (Id INT, [Row] INT)

INSERT INTO @Table
SELECT tmp.Id, ROW_NUMBER() OVER (ORDER BY tmp.Id) AS [Row]
FROM [dbo].[Staffs] tmp
WHERE tmp.Resigned = @Resigned
	AND (@OfficeIds IS NULL OR tmp.OfficeId IN (SELECT OfficeId = Item 
												FROM dbo.SplitInts(@OfficeIds, ',')));

SELECT @CountStaff = COUNT(*)
FROM @Table;

WITH T(Id, OfficeId, OfficeName, Name, Ext, Resigned, IdentityCode, ModelName, ModelType)
AS (
	SELECT s.Id
		  ,s.[OfficeId]
		  ,o.[Name] AS OfficeName
		  ,s.[Name]
		  ,s.[Ext]
		  ,s.[Resigned]
		  ,a.IdentityCode
		  ,am.Name as ModelName
		  ,am.Type as ModelType
	FROM [dbo].[Staffs] s
		LEFT JOIN [dbo].[Assets] a ON a.StaffId = s.Id
		LEFT JOIN [dbo].[AssetModels] am ON am.Id = a.AssetModelId
		INNER JOIN [dbo].[Offices] o ON s.OfficeId = o.Id
	WHERE 
		-- SQL 2008										
		s.Id IN (SELECT Id	
					 FROM @Table					  
					 WHERE [Row] > @SkipRec AND [Row] <= @Take)	
		-- Uncomment If You Use SQL >= 2012
		-- s.Id IN (SELECT tmp.Id	
		--			 FROM [dbo].[Staffs] tmp
		--			 WHERE tmp.Resigned = @Resigned
		--			 ORDER BY tmp.Id ASC
		--			 OFFSET @SkipRec ROWS FETCH NEXT @NumberOfItemInPage ROWS ONLY)
)

SELECT Id, OfficeId, OfficeName, Name, Ext, Resigned, 
	   STUFF((
		SELECT DISTINCT ', ' + (CASE ModelType WHEN 0 THEN 'Case Or Lap'
									WHEN 1 THEN 'Monitor'
									WHEN 2 THEN 'Keyboard'
									WHEN 3 THEN 'Mouse'
									WHEN 4 THEN 'Headphone'
									WHEN 5 THEN 'Server'
									ELSE '' 
							   END) + ': '
							 + CAST((SELECT Total 
									 FROM (SELECT tmp2.Id, tmp2.ModelType, COUNT(*) as Total
										   FROM T tmp2
										   WHERE tmp2.Id = tmp.Id AND tmp2.ModelType = tmp.ModelType
										   GROUP BY tmp2.Id, tmp2.ModelType) a) 
									 AS VARCHAR(2))
		FROM T tmp
		WHERE (tmp.Id = t.Id) 
		FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
		,1,1,'') AS [AssetStatistic]
FROM T t
GROUP BY Id, OfficeId, OfficeName, Name, Ext, Resigned
ORDER BY Id

END