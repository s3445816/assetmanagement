USE [master]
GO
/****** Object:  Database [AssetManagement]    Script Date: 02/15/2016 17:23:09 ******/
CREATE DATABASE [AssetManagement] ON  PRIMARY 
( NAME = N'AssetManagement', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\AssetManagement.mdf' , SIZE = 2304KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'AssetManagement_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\AssetManagement_log.LDF' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [AssetManagement] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [AssetManagement].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [AssetManagement] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [AssetManagement] SET ANSI_NULLS OFF
GO
ALTER DATABASE [AssetManagement] SET ANSI_PADDING OFF
GO
ALTER DATABASE [AssetManagement] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [AssetManagement] SET ARITHABORT OFF
GO
ALTER DATABASE [AssetManagement] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [AssetManagement] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [AssetManagement] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [AssetManagement] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [AssetManagement] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [AssetManagement] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [AssetManagement] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [AssetManagement] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [AssetManagement] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [AssetManagement] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [AssetManagement] SET  ENABLE_BROKER
GO
ALTER DATABASE [AssetManagement] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [AssetManagement] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [AssetManagement] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [AssetManagement] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [AssetManagement] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [AssetManagement] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [AssetManagement] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [AssetManagement] SET  READ_WRITE
GO
ALTER DATABASE [AssetManagement] SET RECOVERY FULL
GO
ALTER DATABASE [AssetManagement] SET  MULTI_USER
GO
ALTER DATABASE [AssetManagement] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [AssetManagement] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'AssetManagement', N'ON'
GO
USE [AssetManagement]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 02/15/2016 17:23:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'4c693cfd-fb79-4905-a0e7-4a56c33832e5', N'Admin')
/****** Object:  Table [dbo].[ActivityLogs]    Script Date: 02/15/2016 17:23:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActivityLogs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DateOccurred] [datetime] NOT NULL,
	[Content] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Assets]    Script Date: 02/15/2016 17:23:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Assets](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](49) NOT NULL,
	[Type] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [nvarchar](256) NULL,
	[IsDeleted] [bit] NOT NULL,
	[Note] [nvarchar](256) NULL,
 CONSTRAINT [PK_dbo.Assets] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Assets] ON
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (1, N'Dell 21''5', 1, CAST(0x0000A58A0071901B AS DateTime), NULL, CAST(0x0000A5AD00927A6E AS DateTime), N'huynhd', 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (2, N'Dell 18''5', 1, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (3, N'SSung 21''5', 1, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (4, N'LG 18''5', 1, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (5, N'SSung 18''5', 1, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (6, N'Wis 18''5', 1, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (7, N'HP 18''5', 1, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (8, N'Dell Vostro V470', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (9, N'Dell Vostro V460', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (10, N'Dell Vostro V270', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (11, N'Dell Vostro V260', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (12, N'Dell Inspiron 3847', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (13, N'Dell 3020MT', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (14, N'HP PRO 3340', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (15, N'HP PRO 3330', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (16, N'Lap HP Pro 4320s', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (17, N'LAP Latitude 3440', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (18, N'Lap Dell 2421', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (19, N'Lap HP 14-R010TU', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (24, N'HP Compaq 8200', 5, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (25, N'Dell Precision', 5, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (26, N'Dell Power Edge R310', 5, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (27, N'Dell Power Edge R710', 5, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (28, N'Dell Power Edge R720', 5, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (29, N'Dell Power Edge R210', 5, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (32, N'Dell Power 48 ports', 5, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (33, N'Dell Power 24 ports', 5, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (34, N'Rack Cabinet', 5, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (35, N'Asus M70AD-VN002D', 5, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (36, N'Server Dell R210 II', 5, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (37, N'Server Wdigital', 5, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (38, N'Server DELL R320 1U', 5, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (39, N'Server DELL R420 1U', 5, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (40, N'LG 21''5', 1, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (42, N'Lap HP 4340S', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (43, N'Lap Dell Inspiron 3443', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (44, N'Rack System Cabinet', 5, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (46, N'Dell Test', 1, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (47, N'Test Case', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (48, N'TEst', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (49, N'Test2', 0, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (50, N'ASWVN-HP001', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (51, N'ASWVN-HP002', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (52, N'ASWVNIMAC001', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (53, N'ASWVNIMAC002', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (54, N'ASWVNIMAC003', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (55, N'ASWVNIMAC004', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (56, N'ASWVNIMAC005', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (57, N'ASWVNIMAC006', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (58, N'ASWVNIMAC007', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (59, N'HP PRO 4300', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (60, N'Lap Latitude 3450', 0, NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[Assets] ([Id], [Name], [Type], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsDeleted], [Note]) VALUES (61, N'Wiscom Digital (CPU)', 0, NULL, NULL, NULL, NULL, 0, NULL)
SET IDENTITY_INSERT [dbo].[Assets] OFF
/****** Object:  Table [dbo].[Reminders]    Script Date: 02/15/2016 17:23:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reminders](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[DueTime] [datetime] NULL,
	[Content] [nvarchar](300) NULL,
	[Completed] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_dbo.Reminders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Reminders] ON
INSERT [dbo].[Reminders] ([Id], [TimeStamp], [DueTime], [Content], [Completed], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1, CAST(0x0000A57700000000 AS DateTime), CAST(0x0000A58000000000 AS DateTime), N'<a>Thai Dinh Phong</a> join to company', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Reminders] ([Id], [TimeStamp], [DueTime], [Content], [Completed], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (2, CAST(0x0000A57700000000 AS DateTime), CAST(0x0000A58000000000 AS DateTime), N'<a>Huynh Le Thai Duong</a> join to company', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Reminders] ([Id], [TimeStamp], [DueTime], [Content], [Completed], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (3, CAST(0x0000A57700000000 AS DateTime), CAST(0x0000A58000000000 AS DateTime), N'Set up asset for staff <a>Tran Huu Chau</a>', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Reminders] ([Id], [TimeStamp], [DueTime], [Content], [Completed], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (4, CAST(0x0000A57900000000 AS DateTime), CAST(0x0000A58000000000 AS DateTime), N'Set up asset for staff <a>Le Vinh Khoa</a>', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Reminders] ([Id], [TimeStamp], [DueTime], [Content], [Completed], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (5, CAST(0x0000A57900000000 AS DateTime), CAST(0x0000A58000000000 AS DateTime), N'Set up asset for staff <a>Ho Van Tam</a>', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Reminders] ([Id], [TimeStamp], [DueTime], [Content], [Completed], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (6, CAST(0x0000A57A0083D600 AS DateTime), CAST(0x0000A58000000000 AS DateTime), N'Request 2 monitors for <a>Huynh Le Thai Duong</a>', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Reminders] OFF
/****** Object:  Table [dbo].[RefreshTokens]    Script Date: 02/15/2016 17:23:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RefreshTokens](
	[Id] [nvarchar](256) NOT NULL,
	[Subject] [nvarchar](50) NOT NULL,
	[ClientId] [nvarchar](50) NOT NULL,
	[IssuedUtc] [datetime] NOT NULL,
	[ExpiresUtc] [datetime] NOT NULL,
	[ProtectedTicket] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[RefreshTokens] ([Id], [Subject], [ClientId], [IssuedUtc], [ExpiresUtc], [ProtectedTicket]) VALUES (N'faGEECgkqclbanof6Lq0ZV+uh4LgmC6nuk5rORcLeJc=', N'huynhd', N'webApp', CAST(0x0000A5AD00A9269B AS DateTime), CAST(0x0000A5B200A9269B AS DateTime), N'3PWirSpPqIfxa2tmEbDsOoqXCzY580oTQZNktE5QynASta13_eO469iQTvPgXqxv-Avbgz4QMiFJreatzYdg1vzRosxqYwtJwNrqpvr8TtBgKT5kMzEVUsChHv_ZUPqbAi8CH7w2rHkdTt8v-3tltyDynSzWaLABESk0P6OPz9HEwYI4nDbbCVU_hwc76AmTL-rF1SGqt-qXNcoFaecOeKbZGlWZs5ik149DJSr6nKFIJrVjxgQ6qeXGLAly46LYg_-01TYm9N8vKLRUh-aeiZAWdYFTjoaRTIDNnBaWc_XrPeKnqNtRZV29KUpA47L6s8AeML-UBcGiG7J7y1mTPL2Amicjg8B0PKxYEyaDCPx0UdWriVQ5v2C0aRSRRAQmQNqRI-I-uK2ah4SBOyURKv9EudDdl2Y68yz8msL1Zo8LK02Kawe1fWilF6Do9amtIx07P2UKsudfgbo5s3nskv-BR2P9zaWN3cxxCAewqkoXzy_67TSv71aAKgLnKZn6fGhc6goRe86xEj919uqG-wCB90EjB3RS1r4E1tw9nRl7cceiY7n8wE8lD07wJ9bEoegUEy7IDF-GZS0IHMwZE9zDQHYccLyKnOjrGLh63FD6zK943nUv9ioxmFJ8xt_RTlAlnRHH9ZvDLeWBOKqkCw')
INSERT [dbo].[RefreshTokens] ([Id], [Subject], [ClientId], [IssuedUtc], [ExpiresUtc], [ProtectedTicket]) VALUES (N'jC8hE+TApvU9Kyi4hUktaFX6Us2Y/P5JAslJ2Wp/TBQ=', N'lamphattai', N'webApp', CAST(0x0000A5AD004724C1 AS DateTime), CAST(0x0000A5B2004724C1 AS DateTime), N'6w8mK9jKG_ClwfdgbnJBHFvoBbk6aTnr-MFrb_Nz-ZmhhT7vwoWGHK6pYcxhippT3-SIpdK35a1iQYDs_Q_riE_pfJk38DRmoWsGGRj0oFTBGOUPw9JJpBog61k2fZYfPKmdqDKCkMWH_TcuMuYatMIumbp89zdPcaXVZIrzwu563fpzxNUHn9-K4lcgczDeBiT8Ho_GI9pmqcoajBgxOEJ83Arzmd0s8fd73jJwIW-7h-JXJNIYH5bVAtWI-eE7XmC_C035cqmdnJpPu-ZiWmo-GGbTPbST3q25AQsZnUi81E5nn42FbKS0F1yv4cbZIDNqN5o7Fj_3G8KzdjMZosupJQYPfKka0hGe6RWYQRzSQYl8MZv92uPcaKpi-VIheEM0t9ZpQX4DbHqV1e_wj2gXG6CNkrbkI9wFlOfv1Q1PBGs4KVdSCvT-kFtV4O0MR839mhkPKNndgahy8qpot2SY_e5xowbcdSRyYIrVEo2jguXXMJGTNzo48HqjqJj5wBK4lrTDmKKjT2LuVQaOdzS2z8ExYIyUj5f-4QeiFdHEB-eSsQe8red9B0zlEpt20q6K3A23zzTZJFKjF9IQhcTbakeAZ8WpQCFjwmesxRtWKYUM98t0Ugo2f_yz98HVZ14W0SAviS7lZkRSfJQNJw')
/****** Object:  Table [dbo].[Offices]    Script Date: 02/15/2016 17:23:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Offices](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](35) NULL,
	[Address] [nvarchar](256) NULL,
	[PhoneNumber] [varchar](25) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_dbo.Offices] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Offices] ON
INSERT [dbo].[Offices] ([Id], [Name], [Address], [PhoneNumber], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1, N'Lim Tower', N'', N'5600-5799', NULL, NULL, NULL, NULL)
INSERT [dbo].[Offices] ([Id], [Name], [Address], [PhoneNumber], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (2, N'AB Tower', N'', N'1900-1999', NULL, NULL, NULL, NULL)
INSERT [dbo].[Offices] ([Id], [Name], [Address], [PhoneNumber], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (3, N'Green Power', N'', N'5600-5799', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Offices] OFF
/****** Object:  Table [dbo].[LeaveTypes]    Script Date: 02/15/2016 17:23:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LeaveTypes](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Code] [varchar](5) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[NumberOfDays] [smallint] NULL,
 CONSTRAINT [PK_LeaveType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[LeaveTypes] ([Id], [Name], [Code], [Description], [NumberOfDays]) VALUES (1, N'Annual Leave', N'AL', N'Annual Leave', 20)
INSERT [dbo].[LeaveTypes] ([Id], [Name], [Code], [Description], [NumberOfDays]) VALUES (2, N'Sick Leave', N'SL', N'Sick Leave', 10)
INSERT [dbo].[LeaveTypes] ([Id], [Name], [Code], [Description], [NumberOfDays]) VALUES (3, N'Paid Leave', N'PL', N'Paid Leave', 0)
/****** Object:  Table [dbo].[Clients]    Script Date: 02/15/2016 17:23:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clients](
	[Id] [nvarchar](50) NOT NULL,
	[Secret] [nvarchar](250) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ApplicationType] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[RefreshTokenLifeTime] [int] NOT NULL,
	[AllowedOrigin] [nvarchar](100) NULL,
 CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Clients] ([Id], [Secret], [Name], [ApplicationType], [Active], [RefreshTokenLifeTime], [AllowedOrigin]) VALUES (N'webApp', N'01c7cbc9bc2d3ea25833b50211dacd0fd08ad045bf1ba885ab104b3445372897', N'Asset Web Application', 0, 1, 7200, N'*')
/****** Object:  Table [dbo].[Staffs]    Script Date: 02/15/2016 17:23:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Staffs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OfficeId] [int] NOT NULL,
	[Name] [nvarchar](70) NULL,
	[Ext] [varchar](4) NULL,
	[Resigned] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [nvarchar](256) NULL,
	[JoinedDate] [datetime] NULL,
	[SupervisorId] [int] NULL,
	[Email] [nvarchar](256) NULL,
	[Note] [nvarchar](256) NULL,
 CONSTRAINT [PK_dbo.Staffs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Staffs] ON
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (1, 3, N'Lê Văn Linh', N'5736', 0, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (2, 1, N'Lê Vĩnh Khánh', N'5755', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (3, 2, N'Võ Thanh Duy_B', N'5743', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (4, 1, N'Phạm Minh Khôi', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (5, 1, N'Lê Thanh Duy', N'5797', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (6, 1, N'Vũ Nhật Tân', N'5741', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (7, 1, N'Phạm Minh Trân', N'5740', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (8, 1, N'Trần Hậu Phan Chí', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (10, 1, N'Nguyễn Thành Nỗi', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (11, 1, N'Trương Thanh Trường Hải', N'5782', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (12, 1, N'Đặng Tuấn Anh Khoa', N'5768', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (13, 1, N'Đặng Hồng Sơn', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (14, 1, N'Huỳnh Lê Anh Tú', N'5733', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (15, 1, N'Vũ Hải Sơn', N'5799', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (16, 1, N'Trần Đàm Kim Long', N'5770', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (17, 1, N'Lê Hoàng Phúc', N'5757', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (18, 1, N'Tsằn Quay Phóng', N'5774', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (19, 1, N'Nguyễn Lê Nam', N'5744', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (20, 1, N'Phạm Ngọc Chính', N'5767', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (21, 1, N'Diệu Kỳ Sơn', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (22, 1, N'Bùi Tuấn Anh', N'5765', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (24, 1, N'Đỗ Minh Tuấn', N'5763', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (25, 1, N'Nguyễn Duy Phượng', N'5796', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (26, 1, N'Nguyễn Hoàng Duy Anh', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (27, 1, N'Trần Mạnh Dũng', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (28, 1, N'Nguyễn Hiếu Triệu Vỹ', N'5773', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (29, 1, N'Trương Dũng', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (30, 1, N'Nguyễn Bình Phi', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (31, 1, N'Nguyễn Khánh Hưng', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (32, 1, N'Nguyễn Cảnh Hưng', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (33, 1, N'Nguyễn Lê Uyên Phi', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (34, 1, N'Phạm Hồng Sang', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (35, 1, N'Hoàng Trần Quang Khải', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (36, 1, N'Vương An', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (37, 1, N'Nguyễn Xuân Sơn', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (39, 1, N'Nguyễn Mạnh Hoàng', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (40, 1, N'Nguyễn Duy Lâm', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (41, 1, N'Nguyễn Hữu Chí', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (42, 1, N'Trần Quang Thái', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (43, 1, N'Hoàng Trung Hiếu', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (45, 1, N'Nguyễn Hữu Hưng', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (46, 1, N'Trần Huỳnh Bảo Long', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (47, 1, N'Trần Nhật Phong-16', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (48, 1, N'Cao Thị Huyền Sa', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (49, 1, N'Nguyễn Thị Thu Sương', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (51, 1, N'Đặng Tiến Phúc', N'5742', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (52, 1, N'Nguyễn Ngọc Thùy Duyên', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (53, 1, N'Lê Hữu Lộc', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (54, 1, N'Thái Ngân Phụng', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (55, 1, N'Huỳnh Đức Anh Huy', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (56, 1, N'Trầm Ngọc Minh', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (57, 1, N'Nguyễn Ngọc Minh', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (58, 1, N'Lê Nguyễn Cao Nguyên', N'5613', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (59, 1, N'Nguyễn Đức Quý', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (60, 1, N'Nguyễn Tuấn Dũng', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (61, 1, N'Nguyễn Thiên Anh', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (62, 1, N'Đào Hồng Công', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (63, 1, N'Nguyễn Thế Quang Linh', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (64, 1, N'Trần Trung Hiếu', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (65, 1, N'Trần Minh Sang', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (66, 1, N'Nguyễn Thảo Nguyên', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (67, 1, N'Lê Quý Đông', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (68, 1, N'Võ Hồng Phi', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (69, 1, N'Lê Thị Thu Hiền', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (70, 1, N'Lê Thị Thùy Trang', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (71, 1, N'Hung Phan', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (72, 1, N'Nguyễn Trần Quang Vinh', N'5730', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (73, 1, N'Trần Minh Trang', N'5752', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (74, 1, N'Võ Huy Thao', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (75, 1, N'Nguyễn Quốc An', N'1967', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (76, 1, N'Hoàng Thiên Tài', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (77, 1, N'Huỳnh Thị Bích Trâm', N'5789', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (78, 1, N'Phạm Ngọc Kim Vy', N'5775', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (79, 1, N'Phạm Thanh Trà', N'5754', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (80, 1, N'Trần Thị Phương Thảo', N'1904', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (81, 1, N'Shane Devlin', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (82, 1, N'Kios1', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (83, 1, N'Kios2', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (84, 1, N'Kios3', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (85, 1, N'Server Lim', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (86, 1, N'Proxy server 50-Lim', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (87, 1, N'CAMERA CONTROL LIM', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (88, 1, N'MT ROOM Lim - NO VC', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (89, 1, N'MT ROOM Lim 2', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (90, 1, N'KHO LIM', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (91, 1, N'KHO AB', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (92, 3, N'Vương Kim Vũ', N'5601', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (93, 3, N'Trần Quang Bình', N'5794', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (94, 3, N'Vũ Huyền Thu', N'5777', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (95, 3, N'Trịnh Minh Công', N'5756', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (96, 3, N'Nguyễn Minh Phương', N'5703', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (97, 3, N'Nguyễn Đăng Vũ', N'5778', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (98, 3, N'Hoàng Minh Tuấn', N'5704', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (99, 3, N'Huỳnh Kiến Nam', N'5735', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (100, 3, N'Nguyễn Quang Thiện', N'5603', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (101, 3, N'Huỳnh Khánh Lân', N'5602', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (102, 3, N'Nguyễn Lương Vy', N'5604', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (103, 3, N'Mai Thị Bích Vy', N'5709', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (104, 3, N'Pat Trần', N'5605', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (105, 3, N'Phạm Quang Trí', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
print 'Processed 100 total records'
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (106, 3, N'Huỳnh Thiên Kim', N'5791', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (107, 3, N'Nguyễn Thị Thùy Trang', N'5772', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (108, 3, N'Bùi Hồng Minh Hiền', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (109, 3, N'Cổ Hoàng Lân', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (110, 3, N'Ngô Đình Quang Huy', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (111, 3, N'Nguyễn Thị Thủy Tiên', N'5611', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (112, 3, N'Trần Chí Hiếu', N'5612', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (113, 3, N'Nguyễn Hồng Châu Giang', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (114, 3, N'Trần Quốc Linh', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (115, 3, N'Hồ Thanh Toàn', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (116, 3, N'Đinh Thùy An', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (117, 3, N'Nguyễn Huy Thắng', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (118, 3, N'Phan Thị Ngọc Phương', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (119, 3, N'Lê Minh Hùng', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (120, 3, N'Nguyễn Đình Minh Trí', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (121, 3, N'Nguyễn Thanh Hoàng', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (122, 3, N'Lại Đình Lộc', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (123, 3, N'Lê Hoàng Phương', N'5610', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (124, 3, N'Võ Minh Cát', N'5788', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (125, 3, N'Văn Quốc Khánh', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (126, 3, N'Lê Ngọc Tín', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (127, 3, N'Đào Anh Nguyên', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (128, 3, N'Mạc Đức Trọng', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (129, 3, N'Võ Ngọc Quý', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (130, 3, N'Võ Thái Sơn', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (131, 3, N'Lê Cao Trí', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (132, 3, N'Nguyễn Triệu Quốc', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (133, 3, N'Trịnh Hoàng Thiện Long', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (134, 3, N'Nguyễn Quang Kỳ', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (135, 3, N'Trần Đình Khang', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (136, 3, N'Wiscom Digital (CPU) GP', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (137, 3, N'MT GREENPOWER', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (138, 3, N'SVER GP ROOM', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (140, 2, N'Đoàn Yên Sơn', N'1902', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (141, 2, N'Vũ Ngọc Chi', N'1938', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (142, 2, N'Nguyễn Công Thiện', N'1938', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (143, 2, N'Nguyễn Hoàng Lan Anh', N'1938', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (144, 2, N'Nguyễn Tường Minh', N'1938', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (145, 2, N'Design''s team tester', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (146, 1, N'Trần Ngọc Quỳnh Trang', N'5761', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (147, 1, N'Nguyễn Viết Quỳnh Anh', N'5760', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (148, 2, N'Phạm Nguyễn Kim Ngọc', N'1936', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (149, 2, N'Nguyễn Thị Kim Ngân', N'1952', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (150, 2, N'Lý Bảo Ngọc', N'1953', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (151, 2, N'Lê Mạnh Cường', N'1949', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (153, 2, N'Nguyễn Hoàng Bích Hằng', N'1950', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (154, 2, N'Nguyễn Thị Ngọc Hiền', N'1959', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (155, 2, N'Trịnh Thị Minh Trang', N'1960', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (157, 2, N'Nguyễn Thanh Hiền', N'1966', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (158, 2, N'Lê Thị Cẩm Lý', N'1945', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (159, 2, N'Trần Thị Thu Thảo', N'1961', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (160, 2, N'Lương Thị Hoàn Kim', N'1954', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (161, 2, N'Khổng Thị Thuý Uyên', N'1964', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (162, 2, N'Lê Thị Diễm Uyên', N'1978', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (163, 2, N'Nguyễn Thị Thùy Chi', N'1909', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (164, 2, N'Phạm Thị Thu Quỳnh', N'1903', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (165, 2, N'Trang Nguyễn', N'1933', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (166, 2, N'Hoàng Đức Minh', N'1963', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (167, 2, N'Lê Thiên Ân', N'1934', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (168, 2, N'Lại Thị Phương Thảo', N'1930', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (169, 2, N'Nguyễn Ngọc Bảo Trâm', N'1946', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (170, 2, N'Nguyễn Thị Kim Hương', N'1947', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (171, 2, N'Phạm Ngọc Nhung', N'1939', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (172, 2, N'Đặng Trần Quốc Bảo', N'5792', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (173, 2, N'Lâm Phát Tài', N'5762', 0, NULL, NULL, NULL, NULL, NULL, 208, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (174, 2, N'Trần Cường', N'5737', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (177, 2, N'SPARE-SPARE', N'1948', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (178, 2, N'SPARE-SPARE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (179, 2, N'Wiscom Digital (CPU) AB', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (180, 2, N'SERVER ROOM AB', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (181, 2, N'CAMERA AB', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (182, 2, N'SVER AB -PBX BOX', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (183, 2, N'MEETING AB', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (186, 2, N'Nguyễn Quốc An (AB)', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (187, 1, N'Lê Thị Thùy Trang', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (205, 1, N'Nguyễn Trung Tín', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (206, 1, N'Phạm Duy Văn', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (207, 1, N'Lê Văn Hoàng', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (208, 2, N'Huỳnh Lê Thái Dương', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'D.LHuynh@aswigsolutions.com', NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (209, 3, N'Bui Duc Chi', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (210, 3, N'Nguyễn Quang Thiệu', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (211, 3, N'Dariusz Wierzbicki', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (212, 3, N'Nguyễn Bình Phi', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (213, 3, N'Hoàng Ngọc Kim', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (217, 2, N'Nguyễn Thị Mộng Tuyền', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (218, 2, N'Hooàng Thúy Oanh', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (219, 2, N'Lê Ngọc Thiên Ân', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (220, 2, N'Nguyễn Bích Ngọc', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (221, 2, N'Phạm Đại Quốc', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (222, 2, N'Lê Thu Thảo', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (223, 2, N'Lê Thị Quyên', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (224, 2, N'Nguyễn Thị Bích Lan', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (225, 2, N'Phạm Vũ Hương Giang', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (226, 2, N'Nguyễn Huỳnh Khánh Vũ', N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (227, 1, N'AN', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (228, 1, N'Đỗ Hải Hồng', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (229, 1, N'HP  PANTRY 1', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (230, 1, N'HP  PANTRY 2', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (231, 1, N'HP  PANTRY 3', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (232, 1, N'Huỳnh Thị Bích Trâm', N'5789', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (233, 1, N'Lâm Tú Mỹ Kiều-QA', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
print 'Processed 200 total records'
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (234, 1, N'Lê Quang Khương Duy', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (235, 1, N'Lim', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (236, 1, N'LIM - Broken hdd', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (237, 1, N'LIM - Broken hdd - Fan', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (238, 1, N'MT ROOM Lim - NO VC', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (239, 1, N'Nguyễn Đăng Khoa', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (240, 1, N'Nguyễn Minh Hiền', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (241, 1, N'Nguyễn Minh Thành', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (242, 1, N'Nguyễn Ngọc Dương', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (243, 1, N'Nguyễn Ngọc Nhã Vy', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (244, 1, N'Nguyễn Quốc An - 5746', N'1967', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (245, 1, N'Nguyễn Tấn Khiêm', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (246, 1, N'Nguyễn Thanh Nam', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (247, 1, N'Nguyễn Thế Duy-QA', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (248, 1, N'Nguyễn Thiên Ân', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (249, 1, N'Nguyễn Xuân Khải', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (250, 1, N'Phạm Ngọc Kim Vy', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (251, 1, N'Phan Ngọc Khánh Thy', N'5775', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (252, 1, N'Server Lim', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (253, 1, N'SPARE-SPARE', N'5753', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (254, 1, N'SPARE-SPARE', N'5747', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (255, 1, N'SPARE-SPARE', N'5792', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (256, 1, N'SPARE-SPARE', N'5739', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (257, 1, N'SPARE-SPARE', N'5764', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (258, 1, N'SPARE-SPARE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (259, 1, N'SPARE-SPARE', N'5790', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (260, 1, N'Trần Quốc Linh', N'5781', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (261, 1, N'Trần Thị Phương Thảo', N'1904', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (262, 1, N'Trần Văn Thịnh', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (263, 1, N'Trần Vũ', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (264, 1, N'Võ Huy Thao-Backupserver', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (265, 3, N'Bùi Đức Chí', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (266, 3, N'Trương Thị Anh Đào', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (267, 3, N'Đặng Quốc Huy', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (268, 3, N'Nguyễn Bình Phi-16', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (269, 3, N'Nguyễn Hoàng Duy Anh-16', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (270, 3, N'Cao Thị Huyền Sa-16', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (271, 3, N'Nguyễn Trung Kiên-16', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (272, 3, N'Trần Nhật Phong', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (273, 3, N'Nguyễn Trọng Hoài', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (274, 3, N'Nguyễn Lương Yến Vy', N'5604', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (275, 3, N'Trần Công Lâm', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (276, 3, N'Lê Hoàng Phương-5759-16', N'5610', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (277, 3, N'Văn Quốc Khánh-16', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (278, 3, N'Nguyễn Triệu Quốc-16', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (279, 3, N'Trịnh Hoàng Thiện Long-16', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (280, 3, N'Lê Cao Trí-16', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (281, 3, N'Spare for QA-QC', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (282, 3, N'Wiscom Digital (CPU)', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (283, 2, N'Đoàn Yên Sơn', N'1902', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (284, 2, N'Vũ Ngọc Chi', N'1938', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (285, 2, N'Nguyễn Công Thiện', N'1938', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (286, 2, N'Hồ Trần Sĩ Hà', N'1938', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (287, 2, N'Đặng Trần Thành Lợi', N'1938', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (288, 2, N'Nguyễn Hoàng Lan Anh', N'1938', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (289, 2, N'Phạm Văn Tung', N'1938', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (290, 2, N'Lê Thị Quyên', N'1921', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (291, 2, N'Phạm Phương Dung', N'1925', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (292, 2, N'Nguyễn Thị Mỹ Dung', N'1949', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (293, 2, N'Hoàng Thúy Oanh', N'1965', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (294, 2, N'Văn Tấn Lộc-6', N'1969', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (295, 2, N'Lê Thu Thảo', N'1920', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (296, 2, N'Phan Thị Hoàng Yến', N'1924', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (297, 2, N'Hồ Trân', N'1929', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (298, 2, N'Lê Bảo Nam', N'1926', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (299, 2, N'Wiscom Digital (CPU)', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (300, 2, N'SERVER ROOM AB', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (301, 2, N'CAMERA AB', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (302, 2, N'SVER AB -PBX BOX ', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (303, 2, N'MEETING AB', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (304, 2, N'Sophia', N'1964', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (305, 2, N'Letty', N'1947', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (306, 2, N'Test_Result1', N'9999', 0, CAST(0x0000A58A00EEAB10 AS DateTime), N'huynhd', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Staffs] ([Id], [OfficeId], [Name], [Ext], [Resigned], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [JoinedDate], [SupervisorId], [Email], [Note]) VALUES (307, 1, N'Test', N'1234', 0, CAST(0x0000A58B00F6179E AS DateTime), N'lamphattai', NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Staffs] OFF
/****** Object:  Table [dbo].[Requests]    Script Date: 02/15/2016 17:23:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Requests](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Reason] [nvarchar](200) NOT NULL,
	[Status] [smallint] NOT NULL,
	[LeaveTypeId] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[ModifiedBy] [nvarchar](256) NULL,
	[ModifiedDate] [datetime] NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[DeniedReason] [nvarchar](200) NULL,
	[ApprovedReason] [nvarchar](200) NULL,
	[SupervisorId] [int] NULL,
 CONSTRAINT [PK_Request] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Requests] ON
INSERT [dbo].[Requests] ([Id], [Reason], [Status], [LeaveTypeId], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate], [StartDate], [EndDate], [DeniedReason], [ApprovedReason], [SupervisorId]) VALUES (32, N'A', 3, 1, CAST(0x0000A5AD003D5E1C AS DateTime), N'd81e85d3-4d13-4a0c-8d98-8d2d8de9db81', N'd81e85d3-4d13-4a0c-8d98-8d2d8de9db81', CAST(0x0000A5AD003D95D0 AS DateTime), CAST(0x0000A5A800107AC0 AS DateTime), CAST(0x0000A5A800A4CB80 AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Requests] OFF
/****** Object:  Table [dbo].[Assignments]    Script Date: 02/15/2016 17:23:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Assignments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StaffId] [int] NULL,
	[AssetId] [int] NOT NULL,
	[IdentityCode] [varchar](49) NULL,
	[NetworkName] [varchar](49) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [nvarchar](256) NULL,
	[Note] [nvarchar](256) NULL,
 CONSTRAINT [PK_Assignment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Assignments] ON
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (1, 1, 1, N'Mon013', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (2, 2, 1, N'Mon099', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (3, 3, 1, N'Mon038', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (4, 4, 1, N'Mon037', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (5, 5, 1, N'Mon068', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (6, 6, 1, N'Mon054', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (7, 7, 1, N'Mon052', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (8, 8, 1, N'Mon041', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (9, 187, 1, N'Mon048', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (10, 10, 1, N'Mon113', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (11, 10, 1, N'Mon105', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (12, 11, 1, N'Mon050', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (13, 12, 1, N'Mon051', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (14, 13, 1, N'Mon062', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (15, 14, 1, N'Mon021', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (16, 15, 1, N'Mon023', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (17, 15, 1, N'Mon119', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (18, 16, 1, N'Mon039', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (19, 17, 1, N'Mon025', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (20, 24, 1, N'Mon012', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (21, 18, 1, N'Mon060', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (22, 19, 1, N'Mon101', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (23, 19, 1, N'Mon015', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (24, 20, 1, N'Mon029', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (25, 21, 1, N'Mon005', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (26, 21, 1, N'Mon106', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (27, 22, 1, N'Mon010', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (28, 22, 1, N'Mon086', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (29, 3, 1, N'Mon046', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (31, 25, 1, N'Mon070', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (32, 26, 1, N'Mon020', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (33, 27, 1, N'Mon053', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (34, 27, 1, N'Mon128', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (35, 28, 1, N'Mon066', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (36, 29, 1, N'Mon063', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (37, 30, 1, N'Mon026', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (38, 31, 1, N'Mon058', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (39, 32, 1, N'Mon107', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (40, 32, 1, N'Mon109', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (41, 33, 1, N'Mon075', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (42, 34, 1, N'Mon065', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (43, 35, 1, N'Mon043', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (44, 36, 1, N'Mon034', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (45, 37, 1, N'Mon067', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (46, 3, 1, N'Mon057', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (47, 39, 1, N'Mon078', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (48, 40, 1, N'Mon069', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (49, 41, 1, N'Mon049', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (50, 42, 1, N'Mon081', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (51, 43, 1, N'Mon077', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (53, 45, 1, N'Mon079', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (54, 46, 1, N'Mon071', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (55, 47, 1, N'Mon036', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (57, 49, 1, N'Mon142', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (59, 51, 1, N'Mon032', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (60, 52, 1, N'Mon091', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (61, 53, 1, N'Mon047', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (62, 54, 1, N'Mon097', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (63, 55, 1, N'Mon094', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (64, 56, 1, N'Mon095', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (65, 57, 1, N'Mon044', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (72, 307, 1, N'Mon103', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (73, 307, 1, N'Mon121', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (74, NULL, 1, N'Mon090', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (75, NULL, 1, N'Mon117', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (76, NULL, 1, N'Mon118', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (77, NULL, 1, N'Mon022', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (78, NULL, 1, N'Mon018', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (79, NULL, 1, N'Mon074', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (80, NULL, 1, N'Mon031', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (81, NULL, 1, N'Mon098', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (82, NULL, 1, N'Mon008', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (83, NULL, 1, N'Mon009', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (84, NULL, 1, N'Mon024', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (85, NULL, 1, N'Mon093', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (86, NULL, 1, N'Mon056', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (87, NULL, 1, N'Mon096', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (88, NULL, 1, N'Mon017', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (89, NULL, 1, N'Mon006', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (90, NULL, 1, N'Mon120', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (91, NULL, 1, N'Mon122', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (92, NULL, 1, N'Mon124', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (93, NULL, 1, N'Mon125', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (94, NULL, 1, N'Mon126', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (95, NULL, 1, N'Mon127', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (96, NULL, 1, N'Mon123', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (97, NULL, 1, N'Mon085', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (98, NULL, 1, N'Mon108', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (99, NULL, 1, N'Mon014', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (100, NULL, 1, N'Mon115', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (101, NULL, 1, N'Mon011', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (102, NULL, 1, N'Mon004', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (103, NULL, 1, N'Mon116', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (104, NULL, 1, N'Mon033', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (105, NULL, 1, N'Mon114', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (106, NULL, 1, N'Mon016', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (107, NULL, 1, N'Mon088', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (108, NULL, 1, N'Mon019', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (109, NULL, 1, N'Mon042', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (110, NULL, 1, N'Mon064', NULL, NULL, NULL, NULL, NULL, NULL)
GO
print 'Processed 100 total records'
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (111, NULL, 1, N'Mon035', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (112, NULL, 1, N'Mon061', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (113, NULL, 1, N'Mon027', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (114, NULL, 1, N'Mon089', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (115, NULL, 1, N'Mon028', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (116, NULL, 1, N'Mon007', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (117, NULL, 1, N'Mon076', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (118, NULL, 1, N'Mon030', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (119, NULL, 1, N'Mon084', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (120, NULL, 1, N'Mon082', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (121, NULL, 1, N'Mon087', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (122, NULL, 1, N'Mon100', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (123, NULL, 1, N'Mon055', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (205, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (206, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (207, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (208, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (209, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (210, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (211, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (212, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (213, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (214, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (215, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (216, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (217, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (218, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (219, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (220, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (221, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (222, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (223, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (224, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (225, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (226, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (227, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (228, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (229, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (230, NULL, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (231, NULL, 6, N'Mon073', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (232, NULL, 6, N'Mon072', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (234, 15, 13, N'CPU045', N'ASWVNWKS129', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (235, 21, 13, N'CPU074', N'ASWVNWKS157', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (236, 22, 13, N'CPU047', N'ASWVNWKS131', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (237, 25, 13, N'CPU025', N'ASWVNWKS109', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (238, 32, 13, N'CPU027', N'ASWVNWKS111', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (239, 33, 13, N'CPU075', N'ASWVNWKS158', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (240, 3, 13, N'CPU038', N'ASWVNWKS122', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (241, 36, 13, N'CPU030', N'ASWVNWKS114', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (242, 42, 13, N'CPU041', N'ASWVNWKS125', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (243, 43, 13, N'CPU040', N'ASWVNWKS124', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (244, 45, 13, N'CPU039', N'ASWVNWKS123', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (245, 46, 13, N'CPU043', N'ASWVNWKS127', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (246, 48, 13, N'CPU052', N'ASWVNWKS136', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (247, 51, 13, N'CPU053', N'ASWVNWKS137', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (248, 52, 13, N'CPU062', N'ASWVNWKS146', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (249, 55, 13, N'CPU063', N'ASWVNWKS147', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (250, 56, 13, N'CPU061', N'ASWVNWKS145', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (253, 307, 13, N'CPU070', N'ASWVNWKS153', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (254, NULL, 13, N'CPU072', N'ASWVNWKS155', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (255, NULL, 13, N'CPU071', N'ASWVNWKS154', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (256, NULL, 13, N'CPU058', N'ASWVNWKS142', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (257, NULL, 13, N'CPU075', N'ASWVNWKS158', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (258, NULL, 13, N'CPU077', N'ASWVNWKS160', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (259, NULL, 13, N'CPU024', N'ASWVNWKS108', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (260, NULL, 13, N'CPU035', N'ASWVNWKS119', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (261, NULL, 13, N'CPU060', N'ASWVNWKS144', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (262, 34, 13, N'CPU081', N'ASWVNWKS164', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (263, 187, 13, N'CPU083', N'ASWVNWKS166', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (264, NULL, 13, N'CPU084', N'ASWVNWKS167', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (265, NULL, 13, N'CPU085', N'ASWVNWKS168', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (266, NULL, 13, N'CPU031', N'ASWVNWKS115', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (267, NULL, 13, N'CPU026', N'ASWVNWKS110', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (268, NULL, 13, N'CPU033', N'ASWVNWKS117', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (269, NULL, 13, N'CPU044', N'ASWVNWKS128', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (270, NULL, 13, N'CPU042', N'ASWVNWKS126', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (271, NULL, 13, N'CPU046', N'ASWVNWKS130', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (272, NULL, 13, N'CPU048', N'ASWVNWKS132', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (273, NULL, 13, N'CPU064', N'ASWVNWKS133', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (274, NULL, 13, N'CPU065', N'ASWVNWKS134', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (275, NULL, 13, N'CPU055', N'ASWVNWKS139', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (276, NULL, 13, N'CPU067', N'ASWVNWKS149', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (277, NULL, 13, N'CPU066', N'ASWVNWKS148', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (278, NULL, 13, N'CPU073', N'ASWVNWKS156', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (279, NULL, 13, N'CPU076', N'ASWVNWKS159', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (280, NULL, 13, N'CPU078', N'ASWVNWKS161', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (281, NULL, 13, N'CPU079', N'ASWVNWKS162', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (282, NULL, 13, N'CPU080', N'ASWVNWKS163', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (283, NULL, 13, N'CPU082', N'ASWVNWKS165', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (284, NULL, 13, N'CPU029', N'ASWVNWKS113', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (285, NULL, 13, N'CPU054', N'ASWVNWKS138', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (286, NULL, 13, N'CPU059', N'ASWVNWKS143', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (287, NULL, 13, N'CPU034', N'ASWVNWKS118', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (288, NULL, 13, N'CPU051', N'ASWVNWKS135', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (289, NULL, 13, N'CPU068', N'ASWVNWKS151', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (290, NULL, 13, N'CPU056', N'ASWVNWKS140', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (291, NULL, 13, N'CPU037', N'ASWVNWKS121', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (292, NULL, 13, N'CPU057', N'ASWVNWKS141', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (293, 1, 12, N'CPU019', N'ASWVNWKS103', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (294, 11, 12, N'CPU018', N'ASWVNWKS102', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (295, 13, 12, N'CPU021', N'ASWVNWKS105', NULL, NULL, NULL, NULL, NULL)
GO
print 'Processed 200 total records'
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (296, 24, 12, N'CPU023', N'ASWVNWKS107', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (297, 30, 12, N'CPU014', N'ASWVNWKS098', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (298, 39, 12, N'CPU020', N'ASWVNWKS104', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (299, NULL, 12, N'CPU012', N'ASWVNWKS095', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (300, NULL, 12, N'CPU 022', N'ASWVNWKS106', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (301, NULL, 12, N'CPU009', N'ASWVNWKS097', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (302, NULL, 12, N'CPU017', N'ASWVNWKS101', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (303, NULL, 12, N'CPU011', N'ASWVNWKS094', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (304, NULL, 12, N'CPU015', N'ASWVNWKS099', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (305, NULL, 12, N'CPU013', N'ASWVNWKS096', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (306, NULL, 12, N'CPU010', N'ASWVNWKS093', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (307, NULL, 12, N'CPU 016', N'ASWVNWKS100', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (308, 3, 11, N'NoCode', N'ASWVNWKS062', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (309, NULL, 11, N'NoCode', N'ASWVNWKS008', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (310, NULL, 11, N'NoCode', N'ASWVNWKS009', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (311, NULL, 11, N'NoCode', N'ASWVNWKS001', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (312, NULL, 11, N'NoCode', N'ASWVNWKS004', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (313, NULL, 11, N'NoCode', N'ASWVNWKS071', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (314, NULL, 11, N'NoCode', N'ASWVNWKS003', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (315, NULL, 11, N'NoCode', N'ASWVNWKS012', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (316, NULL, 11, N'NoCode', N'ASWVNWKS002', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (317, NULL, 11, N'NoCode', N'ASWVNWKS047', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (318, 5, 10, N'CPU007', N'ASWVNWKS091', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (319, 6, 10, N'CPU004', N'ASWVNWKS089', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (320, 26, 10, N'CPU005', N'ASWVNWKS086', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (321, 27, 10, N'CPU008', N'ASWVNWKS092', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (322, 29, 10, N'CPU006', N'ASWVNWKS083', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (323, 37, 10, N'NoCode', N'ASWVNWKS075', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (324, 40, 10, N'NoCode', N'ASWVNWKS084', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (325, NULL, 10, N'NoCode', N'ASWVNWKS076', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (326, 47, 10, N'NoCode', N'ASWVNWKS078', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (327, 54, 10, N'NoCode', N'ASWVNWKS082', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (328, 57, 10, N'NoCode', N'ASWVNWKS073', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (329, NULL, 10, N'NoCode', N'ASWVNWKS085', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (330, NULL, 10, N'NoCode', N'ASWVNWKS088', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (331, NULL, 10, N'NoCode', N'ASWVNWKS081', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (332, NULL, 10, N'NoCode', N'ASWVNWKS080', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (333, NULL, 10, N'NoCode', N'ASWVNWKS079', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (334, NULL, 10, N'NoCode', N'ASWVNWKS077', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (335, NULL, 10, N'NoCode', N'ASWVNWKS090', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (336, NULL, 10, N'NoCode', N'ASWVNWKS074', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (337, NULL, 10, N'NoCode', N'ASWVNWKS087', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (338, 7, 9, N'NoCode', N'ASWVNWKS021', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (339, 10, 9, N'NoCode', N'ASWVNWKS040', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (340, 31, 9, N'NoCode', N'ASWVNWKS018', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (341, NULL, 9, N'NoCode', N'ASWVNWKS005', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (342, NULL, 9, N'NoCode', N'ASWVNWKS016', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (343, NULL, 9, N'NoCode', N'ASWVNWKS025', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (344, NULL, 9, N'NoCode', N'ASWVNWKS072', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (345, NULL, 9, N'NoCode', N'ASWVNWKS029', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (346, NULL, 9, N'NoCode', N'ASWVNWKS030', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (347, NULL, 9, N'NoCode', N'ASWVNWKS031', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (348, NULL, 9, N'NoCode', N'ASWVNWKS028', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (349, NULL, 9, N'NoCode', N'ASWVNWKS006', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (350, NULL, 9, N'NoCode', N'ASWVNWKS032', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (351, NULL, 9, N'NoCode', N'ASWVNWKS013', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (352, NULL, 9, N'NoCode', N'ASWVNWKS034', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (353, NULL, 9, N'NoCode', N'ASWVNWKS024', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (354, NULL, 9, N'NoCode', N'ASWVNWKS033', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (355, NULL, 9, N'NoCode', N'ASWVNWKS049', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (356, NULL, 9, N'NoCode', N'ASWVNWKS027', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (357, NULL, 9, N'NoCode', N'ASWVNWKS026', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (358, 4, 8, N'NoCode', N'ASWVNWKS056', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (359, 8, 8, N'NoCode', N'ASWVNWKS058', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (360, NULL, 8, N'NoCode', N'ASWVNWKS048', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (361, 12, 8, N'NoCode', N'ASWVNWKS064', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (362, 16, 8, N'NoCode', N'ASWVNWKS053', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (363, 17, 8, N'NoCode', N'ASWVNWKS054', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (364, 19, 8, N'NoCode', N'ASWVNWKS059', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (365, 20, 8, N'NoCode', N'ASWVNWKS036', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (366, NULL, 8, N'NoCode', N'ASWVNWKS014', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (367, NULL, 8, N'NoCode', N'ASWVNWKS043', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (368, 41, 8, N'NoCode', N'ASWVNWKS042', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (369, NULL, 8, N'NoCode', N'ASWVNWKS065', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (370, 53, 8, N'NoCode', N'ASWVNWKS041', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (371, NULL, 8, N'NoCode', N'ASWVNWKS070', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (372, NULL, 8, N'NoCode', N'ASWVNWKS055', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (373, NULL, 8, N'NoCode', N'ASWVNWKS044', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (374, NULL, 8, N'NoCode', N'ASWVNWKS068', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (375, 86, 8, N'NoCode', N'ASWVNWKS011', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (376, NULL, 8, N'NoCode', N'ASWVNWKS019', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (377, NULL, 8, N'NoCode', N'ASWVNWKS051', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (378, NULL, 8, N'NoCode', N'ASWVNWKS020', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (379, NULL, 8, N'NoCode', N'ASWVNWKS063', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (380, NULL, 8, N'NoCode', N'ASWVNWKS010', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (381, NULL, 8, N'NoCode', N'ASWVNWKS057', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (382, NULL, 8, N'NoCode', N'ASWVNWKS017', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (383, NULL, 8, N'NoCode', N'ASWVNWKS050', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (384, NULL, 8, N'NoCode', N'ASWVNWKS052', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (385, NULL, 8, N'NoCode', N'ASWVNWKS069', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (386, NULL, 8, N'NoCode', N'ASWVNWKS045', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (387, NULL, 8, N'NoCode', N'ASWVNWKS067', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (388, NULL, 8, N'NoCode', N'ASWVNWKS066', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (390, NULL, 15, N'NoCode', N'ASWVNWKS037', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (391, NULL, 15, N'NoCode', N'ASWVNWKS035', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (392, 18, 14, N'NoCode', N'ASWVNWKS060', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (393, 28, 14, N'NoCode', N'ASWVNWKS038', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (394, 35, 14, N'NoCode', N'ASWVNWKS022', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (395, NULL, 14, N'NoCode', N'ASWVNWKS039', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (396, NULL, 14, N'NoCode', N'ASWVNWKS046', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (397, NULL, 14, N'NoCode', N'ASWVNWKS061', NULL, NULL, NULL, NULL, NULL)
GO
print 'Processed 300 total records'
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (398, NULL, 14, N'NoCode', N'ASWVNWKS023', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (399, NULL, 14, N'NoCode', N'ASWVNWKS015', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (400, NULL, 14, N'NoCode', N'ASWVNWKS007', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (401, NULL, 18, N'NoCode', N'ASWVNLAP019', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (402, NULL, 18, N'NoCode', N'ASWVNLAP022', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (403, 2, 19, N'NoCode', N'ASWVNLAP024', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (405, 1, 16, N'NoCode', N'ASWVNLAP004', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (406, 2, 16, N'NoCode', N'ASWVNLAP018', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (407, 2, 16, N'NoCode', N'ASWVNLAP014', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (408, NULL, 16, N'NoCode', N'ASWVNLAP012', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (409, NULL, 16, N'NoCode', N'ASWVNLAP009', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (410, NULL, 16, N'NoCode', N'ASWVNLAP016', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (411, NULL, 16, N'NoCode', N'ASWVNLAP015', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (412, NULL, 16, N'NoCode', N'ASWVNLAP005', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (413, NULL, 16, N'NoCode', N'ASWVNLAP002', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (414, NULL, 16, N'NoCode', N'ASWVNLAP008', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (415, NULL, 16, N'NoCode', N'ASWVNLAP001', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (416, 49, 17, N'NoCode', N'ASWVNLAP023', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (417, NULL, 17, N'NoCode', N'ASWVNLAP020', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (418, 14, 13, N'CPU028', N'ASWVNWKS112', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (423, NULL, 24, N'ASWVNHP002', N'ASWVNWKS169', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (424, NULL, 24, N'ASWVNHP003', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (425, NULL, 24, N'ASWVNHP004', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (426, NULL, 24, N'ASWVNHP005', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (427, NULL, 24, N'ASWVNHP006', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (428, NULL, 24, N'ASWVNHP007', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (429, NULL, 24, N'ASWVNHP008', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (430, NULL, 24, N'ASWVN-HP001', N'ASWVNWKS150', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (431, NULL, 25, N'ASWVNSERPRE-01', N'Dell Precision', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (432, NULL, 25, N'ASWVNSERPRE-02', N'Dell Precision', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (433, NULL, 25, N'ASWVNSERPRE-03', N'Dell Precision', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (434, NULL, 26, N'ASWVNSER-R310', N'Dell Power Edge R310', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (435, NULL, 27, N'ASWVNSER-R710', N'Dell Power Edge R710', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (436, NULL, 28, N'ASWVNSER-R720', N'Dell Power Edge R720', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (437, NULL, 29, N'ASWVNSER-R210-1', N'Dell Power Edge R210', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (438, NULL, 29, N'ASWVNSER-R210-2', N'Dell Power Edge R210', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (441, NULL, 32, N'ASWVN-SW48-1', N'Dell Power Switch 48 ports', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (442, NULL, 32, N'ASWVN-SW48-2', N'Dell Power Switch 48 ports', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (443, NULL, 33, N'ASWVN-SW28-1', N'Dell Power Switch 24 ports', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (444, NULL, 34, N'ASWVN-RAC001', N'Rack System cabinet', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (445, NULL, 34, N'ASWVN-RAC002', N'Rack System cabinet', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (446, NULL, 34, N'ASWVN-RAC003', N'Rack System cabinet', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (447, NULL, 35, N'ASWVNSER001', N'CPU Asus M70AD-VN002D', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (448, NULL, 36, N'ASWVNSER002', N'Server Dell R210 II', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (449, NULL, 37, N'ASWVNSER003', N'Server Wdigital', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (450, NULL, 37, N'ASWVNSER004', N'Server Wdigital', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (451, NULL, 37, N'ASWVNSER005', N'Server Wdigital', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (452, NULL, 38, N'ASWVNSER006', N'Server DELL R320 1U', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (453, NULL, 38, N'ASWVNSER007', N'Server DELL R320 1U', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (454, NULL, 38, N'ASWVNSER008', N'Server DELL R320 1U', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (455, NULL, 39, N'ASWVNSER009', N'Server DELL R420 1U', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (456, NULL, 40, N'Mon134', N'LG 21''5', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (457, NULL, 40, N'Mon135', N'LG 21''5', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (459, NULL, 13, N'CPU086', N'ASWVNWKS170', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (460, NULL, 13, N'CPU087', N'ASWVNWKS171', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (461, NULL, 40, N'MON136', N'LG 21''5', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (462, NULL, 40, N'MON137', N'LG 21''5', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (463, NULL, 40, N'Mon138', N'LG 21''5', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (464, NULL, 40, N'Mon139', N'LG 21''5', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (465, NULL, 13, N'CPU088', N'ASWVNWKS172', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (466, NULL, 13, N'CPU089', N'ASWVNWKS173', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (467, NULL, 13, N'CPU090', N'ASWVNWKS174', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (468, 17, 1, N'Mon112', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (469, NULL, 1, N'Mon129', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (470, 47, 1, N'Mon130', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (471, 48, 1, N'Mon131', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (472, NULL, 1, N'Mon132', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (473, NULL, 1, N'Mon133', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (476, NULL, 42, N'ASWVNLAP021', N'Vy''s LAP HP 4340S', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (477, NULL, 43, N'', N'ASWVNLAP025', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (478, NULL, 43, N'', N'ASWVNLAP026', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (479, 75, 10, N'NoCode', N'ASWVNWKS074', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (480, 75, 40, N'Mon135', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (481, 65, 10, N'NoCode', N'ASWVNWKS085', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (482, 65, 1, N'Mon121', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (483, 205, 10, N'NoCode', N'ASWVNWKS088', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (484, 205, 1, N'Mon127', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (485, 172, 10, N'NoCode', N'ASWVNWKS090', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (486, 172, 1, N'Mon133', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (487, 172, 1, N'Mon114', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (488, 225, 10, N'NoCode', N'ASWVNWKS082', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (489, 225, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (490, 226, 10, N'NoCode', N'ASWVNWKS076', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (491, 225, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (492, 58, 13, N'CPU069', N'ASWVNWKS152', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (493, 58, 1, N'Mon102', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (494, 59, 13, N'CPU036', N'ASWVNWKS120', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (495, 59, 1, N'Mon059', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (496, 60, 13, N'CPU070', N'ASWVNWKS153', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (497, 60, 1, N'Mon111', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (498, 61, 13, N'CPU072', N'ASWVNWKS155', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (499, 61, 1, N'Mon110', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (500, 62, 13, N'CPU071', N'ASWVNWKS154', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (501, 62, 1, N'Mon104', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (502, 64, 13, N'CPU058', N'ASWVNWKS142', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (503, 64, 1, N'Mon103', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (504, 206, 13, N'CPU089', N'ASWVNWKS173', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (505, 206, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (506, 206, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (507, 68, 13, N'CPU077', N'ASWVNWKS160', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (508, 68, 1, N'Mon118', N'', NULL, NULL, NULL, NULL, NULL)
GO
print 'Processed 400 total records'
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (509, 207, 13, N'CPU038', N'ASWVNWKS122', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (510, 207, 1, N'Mon123', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (511, 72, 13, N'CPU024', N'ASWVNWKS108', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (512, 72, 1, N'Mon022', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (513, 72, 13, N'CPU035', N'ASWVNWKS119', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (514, 72, 1, N'Mon018', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (515, 74, 13, N'CPU060', N'ASWVNWKS144', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (516, 74, 1, N'Mon098', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (517, 74, 1, N'Mon125', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (518, 103, 13, N'CPU033', N'ASWVNWKS117', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (519, 103, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (520, 103, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (521, 116, 13, N'CPU066', N'ASWVNWKS148', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (522, 116, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (523, 116, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (524, 121, 13, N'CPU080', N'ASWVNWKS163', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (525, 121, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (526, 121, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (527, 119, 13, N'CPU078', N'ASWVNWKS161', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (528, 119, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (529, 119, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (530, 114, 13, N'CPU055', N'ASWVNWKS139', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (531, 114, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (532, 114, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (533, 112, 13, N'CPU064', N'ASWVNWKS133', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (534, 112, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (535, 112, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (536, 108, 13, N'CPU044', N'ASWVNWKS128', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (537, 108, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (538, 108, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (539, 109, 13, N'CPU042', N'ASWVNWKS126', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (540, 109, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (541, 109, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (542, 208, 13, N'CPU032', N'ASWVNWKS116', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (543, 208, 40, N'Mon134', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (544, 165, 13, N'CPU037', N'ASWVNWKS121', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (545, 165, 3, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (546, 165, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (547, 169, 13, N'CPU057', N'ASWVNWKS141', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (548, 169, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (549, 169, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (550, 209, 13, N'CPU082', N'ASWVNWKS165', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (551, 209, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (552, 209, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (553, 115, 13, N'CPU067', N'ASWVNWKS149', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (554, 115, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (555, 115, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (556, 98, 13, N'CPU031', N'ASWVNWKS115', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (557, 98, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (558, 98, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (559, 99, 13, N'CPU026', N'ASWVNWKS110', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (560, 99, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (561, 99, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (562, 131, 13, N'CPU051', N'ASWVNWKS135', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (563, 131, 1, N'Mon084', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (564, 126, 13, N'CPU054', N'ASWVNWKS138', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (565, 126, 1, N'Mon089', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (566, 128, 13, N'CPU059', N'ASWVNWKS143', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (567, 128, 1, N'Mon007', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (568, 110, 13, N'CPU046', N'ASWVNWKS130', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (569, 110, 4, N'Mon007', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (570, 110, 4, N'Mon007', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (571, 120, 13, N'CPU079', N'ASWVNWKS162', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (572, 120, 2, N'Mon007', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (573, 120, 2, N'Mon007', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (574, 113, 13, N'CPU065', N'ASWVNWKS134', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (575, 113, 2, N'Mon007', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (576, 113, 2, N'Mon007', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (577, 134, 13, N'CPU068', N'ASWVNWKS151', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (578, 134, 1, N'Mon100', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (579, 134, 13, N'CPU068', N'ASWVNWKS151', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (580, 134, 1, N'Mon100', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (581, 210, 13, N'CPU073', N'ASWVNWKS156', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (582, 210, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (583, 210, 3, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (584, 111, 13, N'CPU048', N'ASWVNWKS132', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (585, 111, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (586, 111, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (587, 118, 13, N'CPU076', N'ASWVNWKS159', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (588, 118, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (589, 118, 3, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (590, 138, 13, N'CPU056', N'ASWVNWKS140', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (591, 138, 3, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (592, 129, 13, N'CPU034', N'ASWVNWKS118', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (593, 129, 1, N'Mon076', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (594, 125, 13, N'CPU029', N'ASWVNWKS113', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (595, 125, 1, N'Mon027', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (596, 63, 8, N'NoCode', N'ASWVNWKS070', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (597, 63, 1, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (598, 76, 8, N'NoCode', N'ASWVNWKS055', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (599, 147, 8, N'NoCode', N'ASWVNWKS050', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (600, 147, 1, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (601, 81, 8, N'NoCode', N'ASWVNWKS044', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (602, 81, 1, N'Mon017', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (603, 87, 8, N'NoCode', N'ASWVNWKS019', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (604, 89, 8, N'NoCode', N'ASWVNWKS051', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (605, 174, 8, N'NoCode', N'ASWVNWKS067', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (606, 174, 1, N'Mon080', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (607, 217, 8, N'NoCode', N'ASWVNWKS066', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (608, 217, 1, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (609, 153, 8, N'NoCode', N'ASWVNWKS052', NULL, NULL, NULL, NULL, NULL)
GO
print 'Processed 500 total records'
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (610, 153, 1, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (611, 161, 8, N'NoCode', N'ASWVNWKS069', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (612, 161, 1, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (613, 173, 8, N'NoCode', N'ASWVNWKS045', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (614, 173, 1, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (615, 173, 1, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (616, 211, 8, N'NoCode', N'ASWVNWKS043', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (617, 211, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (618, 211, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (619, 213, 8, N'NoCode', N'ASWVNWKS014', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (620, 213, 1, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (621, 137, 8, N'NoCode', N'ASWVNWKS017', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (622, 137, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (623, 96, 8, N'NoCode', N'ASWVNWKS020', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (624, 96, 3, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (625, 132, 8, N'NoCode', N'ASWVNWKS063', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (626, 132, 1, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (627, 138, 8, N'NoCode', N'ASWVNWKS068', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (628, 135, 8, N'NoCode', N'ASWVNWKS057', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (629, 135, 1, N'Mon055', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (630, 133, 8, N'NoCode', N'ASWVNWKS010', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (631, 133, 1, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (632, 66, 43, N'NoCode', N'ASWVNLAP026', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (633, 66, 40, N'Mon140', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (634, 69, 43, N'NoCode', N'ASWVNLAP025', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (635, 69, 1, N'Mon124', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (636, 106, 9, N'NoCode', N'ASWVNWKS005', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (637, 106, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (638, 106, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (639, 40, 10, N'NoCode', N'ASWVNWKS084', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (640, 40, 1, N'Mon069', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (641, 102, 10, N'NoCode', N'ASWVNWKS080', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (642, 102, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (643, 100, 10, N'NoCode', N'ASWVNWKS081', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (644, 102, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (645, 104, 10, N'NoCode', N'ASWVNWKS079', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (646, 104, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (647, 104, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (648, 124, 10, N'NoCode', N'ASWVNWKS077', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (649, 124, 1, N'Mon061', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (650, 146, 9, N'NoCode', N'ASWVNWKS072', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (651, 146, 1, N'Mon108', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (652, 146, 1, N'Mon129', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (653, 145, 9, N'NoCode', N'ASWVNWKS025', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (654, 145, 1, N'Mon117', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (655, 149, 9, N'NoCode', N'ASWVNWKS029', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (656, 149, 1, N'Mon019', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (657, 150, 9, N'NoCode', N'ASWVNWKS030', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (658, 150, 1, N'Mon120', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (659, 154, 9, N'NoCode', N'ASWVNWKS028', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (660, 154, 1, N'Mon042', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (661, 218, 9, N'', N'ASWVNWKS027', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (662, 218, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (663, 157, 9, N'NoCode', N'ASWVNWKS006', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (664, 157, 1, N'Mon004', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (665, 157, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (666, 158, 9, N'NoCode', N'ASWVNWKS032', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (667, 158, 1, N'Mon132', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (668, 159, 9, N'NoCode', N'ASWVNWKS013', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (669, 159, 1, N'Mon009', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (670, 162, 9, N'NoCode', N'ASWVNWKS034', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (671, 162, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (672, 162, 5, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (673, 163, 9, N'NoCode', N'ASWVNWKS024', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (674, 163, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (675, 163, 5, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (676, 221, 9, N'NoCode', N'ASWVNWKS033', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (677, 221, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (678, 221, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (679, 170, 9, N'NoCode', N'ASWVNWKS049', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (680, 170, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (681, 170, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (682, 224, 9, N'NoCode', N'ASWVNWKS026', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (683, 170, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (684, 170, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (685, 130, 9, N'NoCode', N'ASWVNWKS016', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (686, 130, 1, N'Mon030', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (687, 107, 11, N'NoCode', N'ASWVNWKS009', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (688, 107, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (689, 107, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (690, 155, 11, N'NoCode', N'ASWVNWKS001', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (691, 155, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (692, 219, 11, N'NoCode', N'ASWVNWKS004', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (693, 219, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (694, 160, 11, N'NoCode', N'ASWVNWKS071', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (695, 160, 1, N'Mon008', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (696, 166, 11, N'NoCode', N'ASWVNWKS003', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (697, 166, 1, N'Mon085', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (698, 167, 11, N'NoCode', N'ASWVNWKS012', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (699, 167, 1, N'Mon014', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (700, 171, 11, N'NoCode', N'ASWVNWKS002', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (701, 171, 1, N'Mon122', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (702, 97, 11, N'NoCode', N'ASWVNWKS008', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (703, 97, 2, N'Mon122', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (704, 97, 2, N'Mon122', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (705, 73, 12, N'NoCode', N'ASWVNWKS095', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (706, 73, 1, N'Mon074', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (707, 73, 1, N'Mon031', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (708, 76, 12, N'CPU022', N'ASWVNWKS106', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (709, 76, 1, N'Mon024', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (710, 93, 12, N'CPU017', N'ASWVNWKS101', NULL, NULL, NULL, NULL, NULL)
GO
print 'Processed 600 total records'
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (711, 93, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (712, 93, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (713, 94, 12, N'CPU011', N'ASWVNWKS094', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (714, 94, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (715, 94, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (716, 92, 12, N'CPU009', N'ASWVNWKS097', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (717, 92, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (718, 92, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (719, 220, 12, N'CPU010', N'ASWVNWKS093', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (720, 220, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (721, 220, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (722, 127, 12, N'CPU013', N'ASWVNWKS096', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (723, 127, 1, N'Mon028', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (724, 123, 12, N'CPU015', N'ASWVNWKS099', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (725, 123, 1, N'Mon035', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (726, 148, 16, N'NoCode', N'ASWVNWKS095', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (727, 148, 1, N'Mon011', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (728, 95, 16, N'NoCode', N'ASWVNLAP015', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (729, 95, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (730, 168, 14, N'NoCode', N'ASWVNWKS007', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (731, 168, 1, N'Mon115', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (732, 164, 15, N'NoCode', N'ASWVNWKS035', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (733, 164, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (734, 164, 5, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (735, 105, 15, N'NoCode', N'ASWVNWKS037', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (736, 105, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (737, 105, 2, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (738, 101, 14, N'NoCode', N'ASWVNWKS023', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (739, 105, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (740, 105, 4, N'', N'', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (741, NULL, 35, N'ASWVNSER002', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (742, NULL, 35, N'ASWVNSER003', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (743, NULL, 35, N'ASWVNSER004', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (744, NULL, 35, N'ASWVNSER005', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (745, NULL, 35, N'ASWVNSER006', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (746, NULL, 35, N'ASWVNSER007', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (747, NULL, 35, N'ASWVNSER008', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (748, NULL, 35, N'ASWVNSER009', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (749, NULL, 35, N'ASWVNSER010', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (750, NULL, 35, N'ASWVNSER011', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (751, 227, 16, N'NoCode', N'ASWVNLAP016', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (752, 227, 40, N'Mon159', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (753, 228, 13, N'CPU086', N'ASWVNWKS170', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (754, 228, 2, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (755, 228, 2, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (756, 229, 59, N'CPU 001', N'Kios 01', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (757, 229, 7, N'Mon001', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (758, 230, 59, N'CPU 002', N'Kios 02', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (759, 230, 7, N'Mon002', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (760, 231, 59, N'CPU 003', N'Kios 03', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (761, 231, 7, N'Mon003', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (762, 232, 50, N'NoCode', N'ASWVNWKS150', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (763, 232, 40, N'Mon141', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (764, 232, 40, N'Mon151', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (765, 233, 13, N'CPU094', N'ASWVNWKS178', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (766, 233, 2, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (767, 233, 2, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (768, 234, 13, N'CPU032', N'ASWVNWKS116', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (769, 234, 40, N'Mon136', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (770, 235, 16, N'NoCode', N'ASWVNLAP009', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (771, 236, 16, N'NoCode', N'ASWVNLAP015', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (772, 237, 16, N'NoCode', N'ASWVNLAP012', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (773, 238, 14, N'NoCode', N'ASWVNWKS061', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (774, 239, 12, N'CPU021', N'ASWVNWKS105', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (775, 239, 1, N'Mon062', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (776, 240, 13, N'CPU097', N'ASWVNWKS181', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (777, 240, 40, N'Mon160', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (778, 241, 13, N'CPU096', N'ASWVNWKS180', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (779, 241, 40, N'Mon165', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (780, 242, 13, N'CPU099', N'ASWVNWKS183', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (781, 242, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (782, 242, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (783, 243, 1, N'Mon079', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (784, 244, 18, N'NoCode', N'ASWVNLAP019', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (785, 244, 40, N'Mon161', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (786, 244, 40, N'Mon162', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (787, 245, 13, N'CPU092', N'ASWVNWKS176', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (788, 245, 1, N'Mon092', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (789, 246, 13, N'CPU071', N'ASWVNWKS154', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (790, 246, 1, N'Mon051', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (791, 247, 8, N'NoCode', N'ASWVNWKS064', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (792, 247, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (793, 247, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (794, 248, 13, N'CPU091', N'ASWVNWKS175', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (795, 248, 40, N'Mon134', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (796, 249, 10, N'NoCode', N'ASWVNWKS088', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (797, 249, 1, N'Mon127', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (798, 250, 42, N'NoCode', N'ASWVNLAP021', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (799, 251, 60, N'NoCode', N'ASWVNLAP027', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (800, 251, 40, N'Mon144', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (801, 252, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (802, 253, 13, N'CPU100', N'ASWVNWKS184', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (803, 253, 40, N'Mon168', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (804, 254, 13, N'CPU101', N'ASWVNWKS185', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (805, 255, 13, N'CPU102', N'ASWVNWKS186', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (806, 255, 40, N'Mon174', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (807, 256, 13, N'CPU103', N'ASWVNWKS187', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (808, 256, 40, N'Mon175', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (809, 257, 13, N'CPU104', N'ASWVNWKS188', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (810, 257, 40, N'Mon176', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (811, 258, 13, N'CPU105', N'ASWVNWKS189', NULL, NULL, NULL, NULL, NULL)
GO
print 'Processed 700 total records'
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (812, 259, 40, N'Mon171', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (813, 260, 13, N'CPU055', N'ASWVNWKS139', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (814, 260, 2, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (815, 260, 2, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (816, 261, 51, N'NoCode', N'ASWVNWKS169', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (817, 261, 40, N'Mon148', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (818, 261, 40, N'Mon149', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (819, 262, 13, N'CPU098', N'ASWVNWKS182', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (820, 262, 40, N'Mon163', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (821, 263, 12, N'CPU017', N'ASWVNWKS101', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (822, 263, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (823, 263, 2, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (824, 264, 14, N'NoCode', N'ASWVNWKS046', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (825, 265, 13, N'CPU082', N'ASWVNWKS165', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (826, 265, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (827, 265, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (828, 266, 13, N'CPU088', N'ASWVNWKS172', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (829, 266, 2, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (830, 266, 2, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (831, 267, 10, N'NoCode', N'ASWVNWKS078', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (832, 267, 1, N'Mon130', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (833, 268, 12, N'CPU014', N'ASWVNWKS098', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (834, 268, 1, N'Mon026', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (835, 269, 10, N'CPU005', N'ASWVNWKS086', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (836, 269, 40, N'Mon155', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (837, 270, 13, N'CPU052', N'ASWVNWKS136', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (838, 270, 1, N'Mon063', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (839, 271, 10, N'CPU006', N'ASWVNWKS083', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (840, 271, 1, N'Mon046', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (841, 272, 13, N'CPU093', N'ASWVNWKS177', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (842, 272, 40, N'Mon156', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (843, 273, 8, N'NoCode', N'ASWVNWKS014', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (844, 273, 1, N'Mon131', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (845, 274, 10, N'NoCode', N'ASWVNWKS080', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (846, 274, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (847, 274, 2, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (848, 275, 8, N'NoCode', N'ASWVNWKS065', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (849, 275, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (850, 275, 5, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (851, 276, 12, N'CPU015', N'ASWVNWKS099', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (852, 276, 40, N'Mon154', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (853, 276, 40, N'Mon145', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (854, 277, 13, N'CPU029', N'ASWVNWKS113', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (855, 277, 1, N'Mon027', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (856, 278, 8, N'NoCode', N'ASWVNWKS063', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (857, 278, 1, N'Mon082', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (858, 279, 8, N'NoCode', N'ASWVNWKS010', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (859, 279, 1, N'Mon087', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (860, 280, 13, N'CPU051', N'ASWVNWKS135', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (861, 280, 1, N'Mon084', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (862, 281, 2, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (863, 282, 61, N'NoCode', N'Wiscom Digital', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (864, 282, 6, N'Mon072', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (865, 283, 52, N'NoCode', N'ASWVNIMAC001', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (866, 284, 54, N'NoCode', N'ASWVNIMAC003', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (867, 285, 55, N'NoCode', N'ASWVNIMAC004', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (868, 286, 53, N'NoCode', N'ASWVNIMAC002', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (869, 287, 56, N'NoCode', N'ASWVNIMAC005', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (870, 288, 57, N'MK142ZP/A', N'ASWVNIMAC006', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (871, 289, 58, N'MK142ZP/A', N'ASWVNIMAC007', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (872, 290, 13, N'CPU084', N'ASWVNWKS167', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (873, 290, 1, N'Mon099', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (874, 291, 9, N'NoCode', N'ASWVNWKS028', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (875, 291, 1, N'Mon120', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (876, 292, 9, N'NoCode', N'ASWVNWKS031', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (877, 292, 1, N'Mon083', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (878, 293, 9, N'NoCode', N'ASWVNWKS027', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (879, 293, 1, N'Mon040', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (880, 294, 14, N'NoCode', N'ASWVNWKS039', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (881, 294, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (882, 294, 5, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (883, 295, 14, N'NoCode', N'ASWVNWKS015', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (884, 295, 2, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (885, 295, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (886, 296, 14, N'NoCode', N'ASWVNWKS038', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (887, 296, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (888, 296, 5, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (889, 297, 15, N'NoCode', N'ASWVNWKS037', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (890, 297, 2, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (891, 297, 5, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (892, 298, 13, N'CPU085', N'ASWVNWKS168', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (893, 298, 1, N'Mon017', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (894, 298, 40, N'Mon157', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (895, 299, 61, N'NoCode', N'Wiscom Digital', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (896, 299, 6, N'Mon073', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (897, 300, 4, N'NoCode', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (898, 301, 11, N'NoCode', N'ASWVNWKS047', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (899, 302, 10, N'NoCode', N'ASWVNWKS087', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (900, 303, 16, N'NoCode', N'ASWVNLAP002', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (901, 304, 16, N'NoCode', N'ASWVNLAP008', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Assignments] ([Id], [StaffId], [AssetId], [IdentityCode], [NetworkName], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Note]) VALUES (902, 305, 16, N'NoCode', N'ASWVNLAP001', NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Assignments] OFF
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 02/15/2016 17:23:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[StaffId] [int] NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [StaffId]) VALUES (N'2c9bf90c-7687-4efc-b64f-f5851a3791c3', NULL, 0, N'AO/Rt2IEEe9HnwgbEH1wY7Wfa9llQC0VP7Ke7wtGta6VB7U9i3M35AShYLin5SlaTg==', N'19f55863-b90f-4d8e-b4df-a94b80cb113b', NULL, 0, 0, NULL, 0, 0, N'huynhd', 208)
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [StaffId]) VALUES (N'd81e85d3-4d13-4a0c-8d98-8d2d8de9db81', NULL, 0, N'AO/Rt2IEEe9HnwgbEH1wY7Wfa9llQC0VP7Ke7wtGta6VB7U9i3M35AShYLin5SlaTg==', N'42e9ad88-c2c5-4ecd-9288-e975dc8f9ccf', NULL, 0, 0, NULL, 0, 0, N'lamphattai', 173)
/****** Object:  Table [dbo].[AssetLogs]    Script Date: 02/15/2016 17:23:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssetLogs](
	[AssignmentId] [int] NOT NULL,
	[StaffId] [int] NOT NULL,
	[LogDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AssetLogs] PRIMARY KEY CLUSTERED 
(
	[AssignmentId] ASC,
	[StaffId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 02/15/2016 17:23:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'd81e85d3-4d13-4a0c-8d98-8d2d8de9db81', N'4c693cfd-fb79-4905-a0e7-4a56c33832e5')
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 02/15/2016 17:23:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 02/15/2016 17:23:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF__Assets__IsDelete__1FCDBCEB]    Script Date: 02/15/2016 17:23:09 ******/
ALTER TABLE [dbo].[Assets] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Reminders__Compl__20C1E124]    Script Date: 02/15/2016 17:23:09 ******/
ALTER TABLE [dbo].[Reminders] ADD  DEFAULT ((0)) FOR [Completed]
GO
/****** Object:  Default [df_Staffs_Resigned]    Script Date: 02/15/2016 17:23:09 ******/
ALTER TABLE [dbo].[Staffs] ADD  CONSTRAINT [df_Staffs_Resigned]  DEFAULT ((0)) FOR [Resigned]
GO
/****** Object:  ForeignKey [FK_Staffs_Offices_OfficeId]    Script Date: 02/15/2016 17:23:09 ******/
ALTER TABLE [dbo].[Staffs]  WITH CHECK ADD  CONSTRAINT [FK_Staffs_Offices_OfficeId] FOREIGN KEY([OfficeId])
REFERENCES [dbo].[Offices] ([Id])
GO
ALTER TABLE [dbo].[Staffs] CHECK CONSTRAINT [FK_Staffs_Offices_OfficeId]
GO
/****** Object:  ForeignKey [FK_Staffs_Staffs]    Script Date: 02/15/2016 17:23:09 ******/
ALTER TABLE [dbo].[Staffs]  WITH CHECK ADD  CONSTRAINT [FK_Staffs_Staffs] FOREIGN KEY([SupervisorId])
REFERENCES [dbo].[Staffs] ([Id])
GO
ALTER TABLE [dbo].[Staffs] CHECK CONSTRAINT [FK_Staffs_Staffs]
GO
/****** Object:  ForeignKey [FK_Request_LeaveType]    Script Date: 02/15/2016 17:23:09 ******/
ALTER TABLE [dbo].[Requests]  WITH CHECK ADD  CONSTRAINT [FK_Request_LeaveType] FOREIGN KEY([LeaveTypeId])
REFERENCES [dbo].[LeaveTypes] ([Id])
GO
ALTER TABLE [dbo].[Requests] CHECK CONSTRAINT [FK_Request_LeaveType]
GO
/****** Object:  ForeignKey [FK_Requests_Staffs]    Script Date: 02/15/2016 17:23:09 ******/
ALTER TABLE [dbo].[Requests]  WITH CHECK ADD  CONSTRAINT [FK_Requests_Staffs] FOREIGN KEY([SupervisorId])
REFERENCES [dbo].[Staffs] ([Id])
GO
ALTER TABLE [dbo].[Requests] CHECK CONSTRAINT [FK_Requests_Staffs]
GO
/****** Object:  ForeignKey [FK_Assignment_Asset]    Script Date: 02/15/2016 17:23:09 ******/
ALTER TABLE [dbo].[Assignments]  WITH CHECK ADD  CONSTRAINT [FK_Assignment_Asset] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Assets] ([Id])
GO
ALTER TABLE [dbo].[Assignments] CHECK CONSTRAINT [FK_Assignment_Asset]
GO
/****** Object:  ForeignKey [FK_Assignments_Staff]    Script Date: 02/15/2016 17:23:09 ******/
ALTER TABLE [dbo].[Assignments]  WITH CHECK ADD  CONSTRAINT [FK_Assignments_Staff] FOREIGN KEY([StaffId])
REFERENCES [dbo].[Staffs] ([Id])
GO
ALTER TABLE [dbo].[Assignments] CHECK CONSTRAINT [FK_Assignments_Staff]
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUsers_dbo.Staffs_StaffId]    Script Date: 02/15/2016 17:23:09 ******/
ALTER TABLE [dbo].[AspNetUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUsers_dbo.Staffs_StaffId] FOREIGN KEY([StaffId])
REFERENCES [dbo].[Staffs] ([Id])
GO
ALTER TABLE [dbo].[AspNetUsers] CHECK CONSTRAINT [FK_dbo.AspNetUsers_dbo.Staffs_StaffId]
GO
/****** Object:  ForeignKey [FK_AssetLogs_Assignments]    Script Date: 02/15/2016 17:23:09 ******/
ALTER TABLE [dbo].[AssetLogs]  WITH CHECK ADD  CONSTRAINT [FK_AssetLogs_Assignments] FOREIGN KEY([AssignmentId])
REFERENCES [dbo].[Assignments] ([Id])
GO
ALTER TABLE [dbo].[AssetLogs] CHECK CONSTRAINT [FK_AssetLogs_Assignments]
GO
/****** Object:  ForeignKey [FK_Assets_Staffs]    Script Date: 02/15/2016 17:23:09 ******/
ALTER TABLE [dbo].[AssetLogs]  WITH CHECK ADD  CONSTRAINT [FK_Assets_Staffs] FOREIGN KEY([StaffId])
REFERENCES [dbo].[Staffs] ([Id])
GO
ALTER TABLE [dbo].[AssetLogs] CHECK CONSTRAINT [FK_Assets_Staffs]
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]    Script Date: 02/15/2016 17:23:09 ******/
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]    Script Date: 02/15/2016 17:23:09 ******/
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]    Script Date: 02/15/2016 17:23:09 ******/
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]    Script Date: 02/15/2016 17:23:09 ******/
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
