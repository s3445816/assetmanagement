Features:

As IT Admin
======================================================================================================
- Admin wants to view which assets that staff 'Vo Thanh Duy B' is using, so Admin can change those assets
	Feature:
		+ Remove current assets base on Type
		+ Assign new asset to that staff
	Missing:
		+ Cannot remove specified asset

- If company wants to move staff 'Vo Thanh Duy B' from Lim Tower to Green Power, Admin can update staff 
  information for new change.
	Feature:
		+ Change staff's office
	Missing:
		+ Cannot edit remaining information such as Extension Number, Email Address, ...

- If company has new staff:
	Admin can add new staff into system. Then system asks if Admin want assign assets for that staff
	Admin can assign assets for new staff
		Feature:
			+ Add multiple assets base on type at the same time
		Missing: 
			+ Cannot delete specified asset
			
- Admin wants to add new asset or manage assets based on type
	Feature:
		+ Add new asset (provide information such as Name, Type, Note)
		+ View the list of assets in statistics grid
		+ Add batch of items of an asset (identity codes are auto-generated)
		
- Admin wants to modify information or take a note about asset
	Feature:
		+ Edit asset
	Missing:
		+ Cannot edit specified asset
		+ Cannot edit notes of specified asset

- Admin wants to view the details of asset
	Feature:
		+ View the details about all items of an asset (current owner, previous owners)
	Missing:
		+ References to redirect to related staffs' screens
		+ Missing note for assignments
		
- Admin wants to view the history about staffs, assets
	Feature:
		+ Activity logs
		+ History screen to view information about staffs, assets
	Missing:
		+ Just log and show the information between staff - asset, need the implementation for 
		  other information
		
- Admin wants to create/manage reminders
	Feature:
		+ Functionalities for managing reminders (create, view, delete, edit, check complete)
	Missing:
		+ Auto-generated reminders: watch the changes in information about staffs, assets to 
		  auto-create reminders

		


As Normal Staff User
Normal staff user login account is generated base on prefix email. A random password is generated will 
be sent to staff email for accessing system.
======================================================================================================
- He/she (called X) can view CHECK-IN time today
- X can view CHECK-IN list which show lacking time
	Feature: 
		+ Display lacking time
	Missing:
		+ Matching lacking time with X's approved requests.

- X can create Request (Annual Leave, Sick Leave, Paid Leave). Step by step
	+ X create a Request for leaving at specified days
	+ An email is sent to X's supervisor contains Approve and Deny links
	+ The supervisor click on one of those links and it opens page for process (Approve or Deny).
		. If supervisor doesn't login to system, it will redirect to Login page.

	Feature:
		+ Select leave type
		+ Select specified date and time for leaving
	Missing:
		+ Check if the user click on Approve or Deny link is Supervisor of that request or not
		+ Supervisor can view list request that is approved or denied




Wish List
======================================================================================================
- IT Admin 
	+ Remove specified asset when editing or adding a staff
	+ Auto send new staff account email such as Window Account, Email for appropriated leader before 
	  specified time.
	+ Edit specified asset
	+ Edit notes of specified asset
	+ References to redirect to related staffs' screens in screen of asset's details 
	+ Add note for assignments
	+ Just log and show the information between staff - asset, need the implementation for other 
	  information
	+ Auto-generated reminders: watch the changes in information about staffs, assets to auto-create 
	  reminders

- HR Admin 
	+ Edit staff information
	+ View report time sheet in every month
		. Auto list out staffs who have lacking time but it's not approved and send mail for those 
		  staffs		
		. Auto send welcome email before 1 week for new staff
		. Auto send welcome on board email which inform new staffs for company		

- Normal User
	+ View time sheet base on month.
	+ Auto matching lacking time with user's approved requests.
	+ If user is supervisor
		. Check if the user click on Approve or Deny link is Supervisor of that request or not
		. View list request that is approved or denied



Technologies
======================================================================================================
Database: 
	SQL Server 2008 or newer
	Access Database (Read only)

Back-end 
	Language: C#
	Technology: Json Web Token(JWT), Entity Framework(EF)
	Library: OWIN

Front-end 
	Language: HTML, CSS(3), jQuery
	Library: AngularJS, Smart-Table, FullCallendar, MomentJS, Underscore, Bootstrap-DateTimePicker,
	         Semantic-UI
