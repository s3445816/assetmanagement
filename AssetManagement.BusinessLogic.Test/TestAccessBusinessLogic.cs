﻿using AssetManagement.Common.Extensions;
using AssetManagement.DomainModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace AssetManagement.BusinessLogic.Test
{
    [TestClass]
    public class TestAccessBusinessLogic
    {
        public TestAccessBusinessLogic()
        {

        }

        [TestMethod]
        public void TestCheckInToDay()
        {
            var dateTime = new DateTime(2015, 10, 21);
            TimeSheetBusinessLogic bl = new TimeSheetBusinessLogic();
            bl.GetFirstCheckIn("Lam Phat Tai", dateTime);
        }
    }
}
