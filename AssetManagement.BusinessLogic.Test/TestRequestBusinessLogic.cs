﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AssetManagement.Repositories.Interfaces;
using AssetManagement.Repositories;
using AssetManagement.Common.Extensions;
using System.Diagnostics;
using AssetManagement.Common.Services;

namespace AssetManagement.BusinessLogic.Test
{
    [TestClass]
    public class TestRequestBusinessLogic
    {
        protected IUnitOfWork unitOfWork;
        protected RequestBusinessLogic bl;
        protected readonly string createdBy = "d81e85d3-4d13-4a0c-8d98-8d2d8de9db81";

        public TestRequestBusinessLogic()
        {
            unitOfWork = new UnitOfWork();
            bl = new RequestBusinessLogic(unitOfWork);
        }

        [TestMethod]
        public void CannotCreateRequestInSameDate()
        {
            // Please adjust date time to appropriate your DB
            DateTime startDate = new DateTime(2016, 2, 9); // 2016-02-09 12:00:00 AM
            DateTime endDate = new DateTime(2016, 2, 10); // 2016-02-10 12:00:00 AM

            // Adjust start date and end date
            startDate = startDate.AdjustToStartFrom8AM(); // 2016-02-09 08:00:00 AM
            endDate = endDate.AdjustToEndAt5PM(); // 2016-02-09 17:00:00 PM

            Console.WriteLine(startDate.ToString());
            Console.WriteLine(endDate.ToString());

            bool rs = bl.IsRequestExisted(startDate, endDate, createdBy);

            Assert.AreEqual(rs, true);
        }

        [TestMethod]
        public void CannotCreateRequestWithOverlapedTime()
        {
            // Please adjust date time to appropriate your DB
            DateTime startDate = new DateTime(2016, 2, 9);
            DateTime endDate = new DateTime(2016, 2, 11);

            // Adjust start date and end date
            startDate = startDate.AdjustToStartFrom8AM(); // 2016-02-09 8:00:00 AM
            endDate = endDate.AdjustToEndAt5PM(); // 2016-02-10 5:00:00 PM

            Console.WriteLine(startDate.ToString());
            Console.WriteLine(endDate.ToString());

            bool rs = bl.IsRequestExisted(startDate, endDate, createdBy);

            Assert.AreEqual(rs, true);
        }

        [TestMethod]
        public async void TestSendMail()
        {
            EmailMessageService ems = new EmailMessageService();
            //await ems.Send("noreply@mail.com", "lamphattai100590@gmail.com", "Test", "This is a test body");
        }
    }
}
