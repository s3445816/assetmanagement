﻿using AssetManagement.DomainModels;
using AssetManagement.Common.Extensions;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AssetManagement.Common;
using System.Security.Principal;

namespace AssetManagement.Repositories
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public const string ConnectionStringKey = "AssetManagement";
        
        public DbSet<Office> Offices { get; set; }
        public DbSet<Asset> Assets { get; set; }
        public DbSet<Assignment> Assignments { get; set; }
        public DbSet<Staff> Staffs { get; set; }
        public DbSet<AssetLog> AssetLogs { get; set; }
        public DbSet<Reminder> Reminders { get; set; }
        public DbSet<ActivityLog> ActivityLogs { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<LeaveType> LeaveTypes { get; set; }

        protected IIdentity Identity { get; set; }

        public ApplicationDbContext()
            : base(ConnectionStringKey, throwIfV1Schema: false)
        {
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
        }

        /// <summary>
        /// Return a db context with auditing mechanism.
        /// </summary>
        /// <param name="identity"></param>
        public ApplicationDbContext(IIdentity identity)
            : this()
        {
            Identity = identity;
        }

        // If a table has primary key on multiple columns, EF doesn't recognize by itself
        // Instead of that, we override this method to map key in manually.
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<AssetLog>().HasKey(p => new { p.StaffId, p.AssignmentId });
            modelBuilder.Entity<AspNetUserLogin>().HasKey(p => new { p.LoginProvider, p.ProviderKey, p.UserId });
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public static ApplicationDbContext Create(IIdentity identity)
        {
            return new ApplicationDbContext(identity);
        }

        public override async Task<int> SaveChangesAsync()
        {
            if (Identity != null)
            {
                var entities = ChangeTracker.Entries().Where(x => x.Entity is IAuditEntity && 
                                                                 (x.State == EntityState.Added || x.State == EntityState.Modified));
            
                string currentUser = Identity.GetClaimValueByType(ConstantNames.Claim.UserId);

                foreach (var entity in entities)
                {
                    IAuditEntity auditEntity = entity.Entity as IAuditEntity;
                    if (auditEntity == null) continue;

                    if(entity.State == EntityState.Added)
                    {
                        auditEntity.CreatedBy = currentUser;
                        auditEntity.CreatedDate = DateTime.Now.ToUniversalTime();
                    } 
                    else
                    {
                        auditEntity.ModifiedBy = currentUser;
                        auditEntity.ModifiedDate = DateTime.Now.ToUniversalTime();
                    }
                }
            }

            int rs = await base.SaveChangesAsync();

            return rs;
        }
    }
}
