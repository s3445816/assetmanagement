﻿using AssetManagement.DomainModels;
using AssetManagement.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Repositories
{
    public class AuthRepository : IDisposable
    {
        protected ApplicationDbContext dbContext;

        public AuthRepository() : 
            this (new ApplicationDbContext())
        {
        }

        public AuthRepository(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public Client FindClient(string clientId)
        {
            var client = dbContext.Clients.Find(clientId);

            return client;
        }

        public async Task<bool> AddRefreshToken(RefreshToken token)
        {
            var existingToken = dbContext.RefreshTokens.Where(r => r.Subject == token.Subject && r.ClientId == token.ClientId)
                                                       .SingleOrDefault();

            if (existingToken != null)
            {
                var result = await RemoveRefreshToken(existingToken.Id);
            }

            dbContext.RefreshTokens.Add(token);

            return await dbContext.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveRefreshToken(string refreshTokenId)
        {
            var refreshToken = await dbContext.RefreshTokens.FindAsync(refreshTokenId);

            if (refreshToken != null)
            {
                dbContext.RefreshTokens.Remove(refreshToken);
                return await dbContext.SaveChangesAsync() > 0;
            }

            return false;
        }

        public async Task<RefreshToken> FindRefreshToken(string refreshTokenId)
        {
            var refreshToken = await dbContext.RefreshTokens.FindAsync(refreshTokenId);

            return refreshToken;
        }

        public List<RefreshToken> GetAllRefreshTokens()
        {
            return dbContext.RefreshTokens.ToList();
        }

        #region IDisposable Members

        public void Dispose()
        {
            this.dbContext.Dispose();
        }

        #endregion
    }
}
