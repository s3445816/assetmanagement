﻿using AssetManagement.DomainModels;
using AssetManagement.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Repositories
{
    public class AssetRepository : GenericRepository<Asset>, IAssetRepository
    {
        public AssetRepository(ApplicationDbContext dbContext)
            : base(dbContext)
        {
        }

        public List<AssetType> GetAssetTypes()
        {
            return Enum.GetValues(typeof(AssetType)).OfType<AssetType>().ToList();
        }

        public AssetType GetAssetTypeById(int id)
        {
            return (AssetType)(this.GetById(id).Type);
        }

        public void Delete(Asset asset)
        {
            asset.IsDeleted = true;
            this.Update(asset);
        }

        public void Delete(object id)
        {
            Asset asset = this.GetById(id);
            this.Delete(asset);
        } 
    }
}
