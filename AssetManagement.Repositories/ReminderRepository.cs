﻿using AssetManagement.DomainModels;
using AssetManagement.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Repositories
{
    public class ReminderRepository : GenericRepository<Reminder>, IReminderRepository
    {
        public ReminderRepository(ApplicationDbContext dbContext)
            : base(dbContext)
        {
        }
    }
}
