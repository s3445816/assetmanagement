﻿using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Repositories.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        bool SaveChanges();

        Task<bool> SaveChangesAsync();

        IGenericRepository<Asset> AssetRepository { get; }
        IGenericRepository<Assignment> AssignmentRepository { get; }
        IGenericRepository<Office> OfficeRepository { get; }
        IGenericRepository<Staff> StaffRepository { get; }
        IGenericRepository<AssetLog> AssetLogRepository { get; }
        IGenericRepository<Reminder> ReminderRepository { get; }
        IGenericRepository<ActivityLog> ActivityLogRepository { get; }
        IGenericRepository<Request> RequestRepository { get; }
    }
}
