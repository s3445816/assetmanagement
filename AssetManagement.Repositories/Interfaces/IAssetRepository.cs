﻿using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Repositories.Interfaces
{
    public interface IAssetRepository : IGenericRepository<Asset>
    {
        List<AssetType> GetAssetTypes();

        AssetType GetAssetTypeById(int id);
    }
}
