﻿using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Repositories.Interfaces
{
    public interface IStaffRepository : IGenericRepository<Staff>
    {
    }
}
