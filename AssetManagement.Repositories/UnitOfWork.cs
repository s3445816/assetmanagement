﻿using AssetManagement.DomainModels;
using AssetManagement.Repositories;
using AssetManagement.Repositories.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Security.Principal;
using System.Threading.Tasks;

namespace AssetManagement.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationDbContext dbContext;
        private bool disposed = false;
        private IAssetRepository assetRepository;
        private IGenericRepository<Assignment> assignmentRepository;
        private IGenericRepository<Office> officeRepository;
        private IGenericRepository<Staff> staffRepository;
        private IGenericRepository<AssetLog> assetLogRepository;
        private IReminderRepository reminderRepository;
        private IGenericRepository<ActivityLog> activityLogRepository;
        private IGenericRepository<Request> requestRepository;

        public UnitOfWork()
        {
            dbContext = new ApplicationDbContext();
        }

        public UnitOfWork(IIdentity identity)
        {
            dbContext = new ApplicationDbContext(identity);
        }

        public bool SaveChanges()
        {
            return dbContext.SaveChanges() > 0;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await dbContext.SaveChangesAsync() > 0;
        }

        public IGenericRepository<Asset> AssetRepository
        {
            get
            {
                if (assetRepository == null)
                    assetRepository = new AssetRepository(dbContext);
                return assetRepository;
            }
        }

        public IGenericRepository<Assignment> AssignmentRepository
        {
            get
            {
                if (assignmentRepository == null)
                    assignmentRepository = new GenericRepository<Assignment>(dbContext);
                return assignmentRepository;
            }
        }

        public IGenericRepository<AssetLog> AssetLogRepository
        {
            get
            {
                if (assetLogRepository == null)
                    assetLogRepository = new GenericRepository<AssetLog>(dbContext);
                return assetLogRepository;
            }
        }

        public IGenericRepository<Office> OfficeRepository
        {
            get
            {
                if (officeRepository == null)
                    officeRepository = new GenericRepository<Office>(dbContext);
                return officeRepository;
            }
        }

        public IGenericRepository<Staff> StaffRepository
        {
            get
            {
                if (staffRepository == null)
                    staffRepository = new StaffRepository(dbContext);
                return staffRepository;
            }
        }

        public IGenericRepository<Reminder> ReminderRepository
        {
            get
            {
                if (reminderRepository == null)
                    reminderRepository = new ReminderRepository(dbContext);
                return reminderRepository;
            }
        }

        public IGenericRepository<ActivityLog> ActivityLogRepository
        {
            get
            {
                if (activityLogRepository == null)
                    activityLogRepository = new ActivityLogRepository(dbContext);
                return activityLogRepository;
            }
        }

        public IGenericRepository<Request> RequestRepository
        {
            get
            {
                if (requestRepository == null)
                    requestRepository = new GenericRepository<Request>(dbContext);
                return requestRepository;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if(!disposed && disposing)
                dbContext.Dispose();

            disposed = true;
        }

    }
}
