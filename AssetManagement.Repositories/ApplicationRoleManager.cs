﻿using AssetManagement.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetManagement.Repositories
{
    public class ApplicationRoleManager : RoleManager<IdentityRole>
    {
        public ApplicationRoleManager(IRoleStore<IdentityRole, string> store) 
            : base(store)
        {
        }

        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            var dbContext = context.Get<ApplicationDbContext>();
            var roleStore = new RoleStore<IdentityRole>(dbContext);

            var appRoleManager = new ApplicationRoleManager(roleStore);

            return appRoleManager;
        }
    }
}