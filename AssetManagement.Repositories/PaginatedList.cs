﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Repositories
{
    public class PaginatedList<T> : List<T>
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public int TotalPages { get; set; }

        public PaginatedList(IQueryable<T> source, int pageIndex, int pageSize)
        {
            this.PageIndex = pageIndex;
            this.PageSize = PageSize;
            this.TotalCount = source.Count();
            this.TotalPages = (int)Math.Ceiling(this.TotalCount / (double)this.PageSize);

            this.AddRange(source.Skip(PageIndex * PageSize)
                                .Take(this.PageSize));
        }

        public bool HasPreviousPage
        {
            get { return this.PageIndex > 0; }
        }

        public bool HasNextPage
        {
            get { return this.PageIndex + 1 < this.TotalCount; }
        }
    }
}
