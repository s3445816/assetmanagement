﻿using AssetManagement.Repositories.Interfaces;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AssetManagement.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected ApplicationDbContext dbContext;
        protected DbSet<TEntity> dbSet;

        public GenericRepository(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = this.dbContext.Set<TEntity>();
        }

        public virtual IQueryable<TEntity> Data
        {
            get { return dbSet.AsQueryable(); }
        }

        public TEntity GetById(object id)
        {
            return dbSet.Find(id);
        }

        public async Task<TEntity> GetByIdAsync(object id)
        {
            var entity = await dbSet.FindAsync(id);
            return entity;
        }

        public TEntity Single(Expression<Func<TEntity, bool>> predicate)
        {
            return dbSet.Where(predicate).SingleOrDefault();
        }

        public async Task<TEntity> SingleAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var entity = await dbSet.Where(predicate).SingleOrDefaultAsync();
            return entity;
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return dbSet.Where(predicate).FirstOrDefault();
        }

        public async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var entity = await dbSet.Where(predicate).FirstOrDefaultAsync();
            return entity;
        }

        public int Count()
        {
            return dbSet.Count();
        }

        public int Count(Expression<Func<TEntity, bool>> predicate)
        {
            return dbSet.Where(predicate).Count();
        }

        public async Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate)
        {
            int rs = await dbSet.Where(predicate).CountAsync();
            return rs;
        }
        
        public async Task<int> CountAsync()
        {
            int rs = await dbSet.CountAsync();
            return rs;
        }

        public virtual TEntity Insert(TEntity entity)
        {
            var addedEntity = dbSet.Add(entity);
            return addedEntity;
        }

        public virtual void Update(TEntity entity)
        {
            dbSet.Attach(entity);
            dbContext.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(TEntity entity)
        {
            if (dbContext.Entry(entity).State == EntityState.Detached)
                dbSet.Attach(entity);

            dbSet.Remove(entity);
        }

        public virtual async void Delete(object id)
        {
            TEntity entity = await GetByIdAsync(id);
            Delete(entity);
        }

        public TResult Query<TResult>(Func<IQueryable<TEntity>, TResult> query)
        {
            return query(Data);
        }

        public bool SaveChanges()
        {
            return dbContext.SaveChanges() > 0;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await dbContext.SaveChangesAsync() > 0;
        }

    }
}
