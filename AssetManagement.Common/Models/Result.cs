﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Common.Models
{
    public class Result
    {
        public bool Succeeded { get; set; }
        public string Error { get; set; }
    }
}
