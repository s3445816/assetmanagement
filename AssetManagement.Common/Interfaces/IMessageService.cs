﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Common.Interfaces
{
    public interface IMessageService
    {
        Task Send();
    }
}
