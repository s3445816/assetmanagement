﻿using AssetManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Common.Extensions
{
    public static class IdentityExtensions
    {
        public static string GetClaimValueByType(this IIdentity identity, string type)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst(type);
            return claim == null ? string.Empty : claim.Value;
        }
    }
}
