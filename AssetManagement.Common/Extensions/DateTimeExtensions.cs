﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Common.Extensions
{
    public static class DateTimeExtensions
    {
        private static double threshold = 2;

        public static int Compare(this DateTime firstObj, DateTime secondObj)
        {
            var diff = firstObj.Subtract(secondObj).TotalSeconds;
            
            if (Math.Abs(diff) < threshold)
                return 0;

            return diff > 0 ? 1 : -1;
        }

        public static bool IsEqual(this DateTime firstObj, DateTime secondObj)
        {
            return Compare(firstObj, secondObj) == 0;
        }

        /// <summary>
        /// Return DateTime which start from 8 AM.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime AdjustToStartFrom8AM(this DateTime dateTime)
        {
            if (dateTime.Hour < 8)
            {
                dateTime = dateTime.AddHours(8 - dateTime.Hour);
            }

            return dateTime;
        }

        /// <summary>
        /// Return DateTime which end at 5 PM.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime AdjustToEndAt5PM(this DateTime dateTime)
        {
            if (dateTime.Hour < 8)
            {
                dateTime = dateTime.AddDays(-1);
                dateTime = dateTime.AddHours(17 - dateTime.Hour);
            }

            return dateTime;
        }
    }
}
