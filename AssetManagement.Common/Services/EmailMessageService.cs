﻿using AssetManagement.Common.Interfaces;
using System;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;

namespace AssetManagement.Common.Services
{
    public class EmailMessageService : IMessageService
    {
        public string From { get; set; }
        public string DisplayNameFrom { get; set; }
        public string To { get; set; }
        public string DisplayNameTo { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        public EmailMessageService()
        {
        }

        public EmailMessageService(string from, string displayNameFrom, 
                                   string to, string displayNameTo, 
                                   string subject, string body)
            : this (from, to, subject, body)
        {
            DisplayNameFrom = displayNameFrom;
            DisplayNameTo = displayNameTo;
        }

        public EmailMessageService(string from, string to, string subject, string body)
        {
            From = from;
            To = to;
            Subject = subject;
            Body = body;
        }

        public async Task Send()
        {
            CheckToThrowInvalidArguments();

            MailMessage mailMsg = new MailMessage();

            // To
            mailMsg.To.Add(new MailAddress(To, DisplayNameTo));

            // From
            mailMsg.From = new MailAddress(From, DisplayNameFrom);

            // Subject and multipart/alternative Body
            mailMsg.Subject = Subject;
            mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(Body, null, MediaTypeNames.Text.Html));

            // Init SmtpClient and send
            SmtpClient smtpClient = new SmtpClient();

            try
            {
                await smtpClient.SendMailAsync(mailMsg);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        protected void CheckToThrowInvalidArguments()
        {
            if (string.IsNullOrEmpty(From))
                throw new ArgumentNullException("[From] Email is missing");

            if (string.IsNullOrEmpty(To))
                throw new ArgumentNullException("[To] Email is missing");

            if (string.IsNullOrEmpty(Subject) && string.IsNullOrEmpty(Body))
                throw new ArgumentNullException("[Subject] and [Body] of Email is missing");

        }
    }
}
