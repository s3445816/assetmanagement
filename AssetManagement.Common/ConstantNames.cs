﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Common
{
    public static class ConstantNames
    {
        public static class Claim
        {
            public static string UserId { get { return "UserId"; } }
            public static string Email { get { return "Email"; } }
            public static string SupervisorId { get { return "SupervisorId"; } }
            public static string StaffName { get { return "StaffName"; } }
        }
        
    }
}
