﻿using Microsoft.Owin.Security.DataHandler.Encoder;
using System;
using System.Collections.Concurrent;
using System.Configuration;
using System.Security.Cryptography;

namespace AssetManagement.AuthorizationServer.API.Providers
{
    public static class AudienceProvider
    {
        public static ConcurrentDictionary<string, Audience> AudiencesList = new ConcurrentDictionary<string, Audience>();

        static AudienceProvider()
        {
            // For testing, will use register flow to register a new audience in practical scenario
            AudiencesList.TryAdd(ConfigurationManager.AppSettings["as:DummyResourceServer_ClientId"],
                                new Audience
                                {
                                    ClientId = ConfigurationManager.AppSettings["as:DummyResourceServer_ClientId"],
                                    SymmetricKey = ConfigurationManager.AppSettings["as:DummyResourceServer_SymmetricKey"],
                                    ServerName = ConfigurationManager.AppSettings["as:DummyResourceServer_ServerName"]
                                });
        }

        public static Audience AddAudience(string name)
        {
            var clientId = Guid.NewGuid().ToString("N");

            var key = new byte[32];
            RandomNumberGenerator.Create().GetBytes(key);
            var base64Secret = TextEncodings.Base64Url.Encode(key);

            Audience newAudience = new Audience { ClientId = clientId, SymmetricKey = base64Secret, ServerName = name };
            AudiencesList.TryAdd(clientId, newAudience);
            return newAudience;
        }

        public static Audience FindAudience(string clientId)
        {
            Audience audience = null;
            if (AudiencesList.TryGetValue(clientId, out audience))
            {
                return audience;
            }
            return null;
        }
    }
}