﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using System;
using Thinktecture.IdentityModel.Tokens;
using System.IdentityModel.Tokens;
using System.Configuration;
using System.IdentityModel.Protocols.WSTrust;

namespace AssetManagement.AuthorizationServer.API.Providers
{
    public class CustomJwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private readonly string issuer = string.Empty;

        public CustomJwtFormat(string issuer)
        {
            this.issuer = issuer;
        }

        public string Protect(AuthenticationTicket data)
        {
            string audienceId = data.Properties.Dictionary.ContainsKey(ConfigurationManager.AppSettings["as:AudiencePropertyKey"]) 
                ? data.Properties.Dictionary[ConfigurationManager.AppSettings["as:AudiencePropertyKey"]] 
                : null;

            Audience audience = AudienceProvider.FindAudience(audienceId);

            string symmetricKeyAsBase64 = audience.SymmetricKey;

            var keyByteArray = TextEncodings.Base64Url.Decode(symmetricKeyAsBase64);
            var signingKey = new HmacSigningCredentials(keyByteArray);
            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;
            var lifeTime = new Lifetime(issued.Value.UtcDateTime, expires.Value.UtcDateTime);

            var token = new JwtSecurityToken(this.issuer, audienceId,
                                             data.Identity.Claims, lifeTime, signingKey);

            var handler = new JwtSecurityTokenHandler();
            var jwt = handler.WriteToken(token);

            return jwt;
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }
}