﻿using AssetManagement.Common;
using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace AssetManagement.AuthorizationServer.API.Providers
{
    public class ClaimProvider
    {
        public static IEnumerable<Claim> GetClaims(ApplicationUser user)
        {
            List<Claim> claims = new List<Claim>();

            claims.Add(CreateClaim(ConstantNames.Claim.UserId, user.Id));
            claims.Add(CreateClaim(ConstantNames.Claim.Email, user.Email ?? string.Empty));

            string supervisorId = (user.Staff.SupervisorId ?? 0).ToString();

            claims.Add(CreateClaim(ConstantNames.Claim.SupervisorId, supervisorId));
            claims.Add(CreateClaim(ConstantNames.Claim.StaffName, user.Staff.Name));

            return claims;
        }

        public static Claim CreateClaim(string type, string value)
        {
            return new Claim(type, value, ClaimValueTypes.String);
        }
    }
}