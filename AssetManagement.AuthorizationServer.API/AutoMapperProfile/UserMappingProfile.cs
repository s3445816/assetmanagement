﻿using AssetManagement.AuthorizationServer.API.Models;
using AssetManagement.DomainModels;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetManagement.ResourceServer.API.AutoMapperProfile
{
    public class UserMappingProfile : Profile
    {
        public const string ViewModel = "UserMappingProfile";

        public new string ProfileName
        {
            get { return ViewModel; }
        }

        protected override void Configure()
        {
            CreateMap<ApplicationUser, UserReturnModel>();
            CreateMap<UserReturnModel, ApplicationUser>();
        }
    }
}