﻿using System.ComponentModel.DataAnnotations;

namespace AssetManagement.AuthorizationServer.API
{
    public class Audience
    {
        [Key]
        [MaxLength(32)]
        public string ClientId { get; set; }

        [MaxLength(80)]
        [Required]
        public string SymmetricKey { get; set; }

        [MaxLength(100)]
        [Required]
        public string ServerName { get; set; }
    }
}