﻿using AssetManagement.AuthorizationServer.API.Models;
using AssetManagement.AuthorizationServer.API.Providers;
using AssetManagement.DomainModels;
using AssetManagement.Repositories;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace AssetManagement.AuthorizationServer.API.Controllers
{
    [RoutePrefix("api/account")]
    public class AccountController : BaseApiController
    {
        public AccountController() { }

        public AccountController(ApplicationUserManager appUserManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            this.AppUserManager = appUserManager;
        }

        [Route("users")]
        public IHttpActionResult GetUsers()
        {
            var users = this.AppUserManager.Users.ToList().Select(u => Mapper.Map<ApplicationUser, UserReturnModel>(u));
            return Ok(users);
        }

        [Route("user/{id:guid}", Name = "GetUserById")]
        public async Task<IHttpActionResult> GetUser(string id)
        {
            var appUser = await this.AppUserManager.FindByIdAsync(id);

            if (appUser == null)
                return NotFound();

            var user = Mapper.Map<ApplicationUser, UserReturnModel>(appUser);

            return Ok(user);
        }

        [Route("user/{username}")]
        public async Task<IHttpActionResult> GetUserByName(string username)
        {
            var appUser = await this.AppUserManager.FindByNameAsync(username);

            if (appUser == null)
                return NotFound();

            var user = Mapper.Map<ApplicationUser, UserReturnModel>(appUser);

            return Ok(user);
        }

        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(UserRegisterModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            ApplicationUser user = new ApplicationUser
            {
                UserName = model.UserName,
                StaffId = model.StaffId
            };

            IdentityResult result = await this.AppUserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
                return GetErrorResult(result);

            return Ok();
        }

        [Route("user/{id:guid}/roles")]
        [HttpPut]
        public async Task<IHttpActionResult> AssignRolesToUser([FromUri] string id,
                                                               [FromBody] string[] rolesToAssign)
        {
            var appUser = await this.AppUserManager.FindByIdAsync(id);

            if (appUser == null)
                return NotFound();

            var currentRoles = await this.AppUserManager.GetRolesAsync(appUser.Id);
            var rolesNotExists = rolesToAssign.Except(this.AppRoleManager.Roles.Select(x => x.Name)).ToArray();

            if(rolesNotExists.Count() > 0)
            {
                ModelState.AddModelError("", string.Format("Roles '{0}' does not exist in the system", string.Join(",", rolesNotExists)));
                return BadRequest(ModelState);
            }

            IdentityResult removeResult = await this.AppUserManager.RemoveFromRolesAsync(appUser.Id, currentRoles.ToArray());

            if (!removeResult.Succeeded)
            {
                ModelState.AddModelError("", "Failed to remove user roles");
                return BadRequest(ModelState);
            }

            IdentityResult addResult = await this.AppUserManager.AddToRolesAsync(appUser.Id, rolesToAssign);

            if (!addResult.Succeeded)
            {
                ModelState.AddModelError("", "Failed to add user roles");
                return BadRequest(ModelState);
            }

            return Ok();
        }

        [Route("user/{id:guid}/assignclaims")]
        [HttpPut]
        public async Task<IHttpActionResult> AssignClaimsToUser([FromUri] string id,
                                                                [FromBody] List<ClaimModel> claimsToAssign)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var appUser = await this.AppUserManager.FindByIdAsync(id);

            if (appUser == null)
                return NotFound();

            foreach (ClaimModel claimModel in claimsToAssign)
            {
                if(appUser.Claims.Any(c => c.ClaimType == claimModel.Type)) 
                    await this.AppUserManager.RemoveClaimAsync(id, ClaimProvider.CreateClaim(claimModel.Type, claimModel.Value));

                await this.AppUserManager.AddClaimAsync(id, ClaimProvider.CreateClaim(claimModel.Type, claimModel.Value));
            }

            return Ok();
        }

        [Route("user/{id:guid}/assignclaims")]
        [HttpPut]
        public async Task<IHttpActionResult> RemoveClaimsToUser([FromUri] string id,
                                                                [FromBody] List<ClaimModel> claimsToRemove)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var appUser = await this.AppUserManager.FindByIdAsync(id);

            if (appUser == null)
                return NotFound();

            foreach (ClaimModel claimModel in claimsToRemove)
            {
                if (appUser.Claims.Any(c => c.ClaimType == claimModel.Type))
                    await this.AppUserManager.RemoveClaimAsync(id, ClaimProvider.CreateClaim(claimModel.Type, claimModel.Value));
            }

            return Ok();
        }
    }
}