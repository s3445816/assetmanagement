﻿using AssetManagement.DomainModels;
using AssetManagement.Repositories;
using System.Threading.Tasks;
using System.Web.Http;

namespace AssetManagement.AuthorizationServer.API.Controllers
{
    [RoutePrefix("api/clients")]
    public class ClientController : BaseApiController
    {
        ApplicationDbContext context = new ApplicationDbContext();

        [Route("Create")]
        public async Task<IHttpActionResult> Create(Client model)
        {
            var client = await context.Clients.FindAsync(model.Id);

            if(client != null)
                return BadRequest("Client is existed");

            context.Clients.Add(model);

            int result = await context.SaveChangesAsync();

            if (result > 0)
                return Ok();

            return BadRequest("An error is occurred");
        }
    }
}