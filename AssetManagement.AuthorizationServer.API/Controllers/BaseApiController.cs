﻿using AssetManagement.AuthorizationServer.API.Models;
using AssetManagement.DomainModels;
using AssetManagement.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace AssetManagement.AuthorizationServer.API.Controllers
{
    public class BaseApiController : ApiController
    {
        private ApplicationUserManager userManager;
        private ApplicationRoleManager roleManager;

        public ApplicationUserManager AppUserManager
        {
            get
            {
                return this.userManager ??
                        Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            protected set
            {
                this.userManager = value;
            }
        }

        public ApplicationRoleManager AppRoleManager
        {
            get
            {
                return this.roleManager ?? Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
            protected set
            {
                this.roleManager = value;
            }
        }

        protected IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
                return InternalServerError();

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                // No ModelState errors are available to send, so just return an empty BadRequest.
                if (ModelState.IsValid)
                    return BadRequest();

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}