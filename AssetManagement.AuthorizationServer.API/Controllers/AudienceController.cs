﻿using AssetManagement.AuthorizationServer.API.Models;
using AssetManagement.AuthorizationServer.API.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AssetManagement.AuthorizationServer.API.Controllers
{
    [RoutePrefix("api/audience")]
    public class AudienceController : ApiController
    {
        [Route("")]
        public IHttpActionResult Register(AudienceModel audienceModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Audience newAudience = AudienceProvider.AddAudience(audienceModel.Name);

            return Ok<Audience>(newAudience);
        }
    }
}
