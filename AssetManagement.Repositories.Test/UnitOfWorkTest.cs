﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using AssetManagement.Repositories.Interfaces;

namespace AssetManagement.Repositories.Test
{
    [TestClass]
    public class UnitOfWorkTest
    {
        [TestMethod]
        public void Test_Asset_Item_Count()
        {
            UnitOfWork unitOfWork = new UnitOfWork();
            var count = unitOfWork.AssetRepository.Count();

            Assert.AreEqual(count, 465);
        }

        [TestMethod]
        public void Test_Staff_Dynamic_Query_BaseOn_Resigned_And_Office()
        {
            UnitOfWork unitOfWork = new UnitOfWork();
            List<int> listOfficeId = null;
            int count = 0;
            
            listOfficeId = new List<int>() { 1 };
            count = unitOfWork.StaffRepository.Count(p => (p.Resigned ?? false) == false
                                                            && listOfficeId.Contains(p.OfficeId));
            Assert.AreEqual(count, 89);

            listOfficeId = new List<int>() { 1, 2 };
            count = unitOfWork.StaffRepository.Count(p => (p.Resigned ?? false) == false
                                                            && listOfficeId.Contains(p.OfficeId));
            Assert.AreEqual(count, 128);

            listOfficeId = new List<int>() { 1, 2, 3 };
            count = unitOfWork.StaffRepository.Count(p => (p.Resigned ?? false) == false
                                                            && listOfficeId.Contains(p.OfficeId));
            Assert.AreEqual(count, 175);
        }

        [TestMethod]
        public void Test_Custom_Select_Staff_Dynamic_Query_BaseOn_Resigned_And_Office()
        {
            UnitOfWork unitOfWork = new UnitOfWork();
            List<int> listOfficeId = null;

            listOfficeId = new List<int>() { 1 };
            var result = unitOfWork.StaffRepository.Query(e => e.Where(p => (p.Resigned ?? false) == false
                                                            && listOfficeId.Contains(p.OfficeId)).Select(s => new { s.Id, s.Name}).ToList());

            Assert.AreEqual(result.Count, 89);
        }

        [TestMethod]
        public void Test_SP_Return_List_And_Output()
        {
            UnitOfWork unitOfWork = new UnitOfWork();
            IStaffRepository repo = (IStaffRepository)unitOfWork.StaffRepository;

            //repo.GetSummaryStaff(false, string.Empty, 1, 25);
        }
    }
}
