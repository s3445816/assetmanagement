//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AssetManagement.DomainModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class AssetLog
    {
        public int AssignmentId { get; set; }
        public int StaffId { get; set; }
        public System.DateTime LogDate { get; set; }
    
        public virtual Assignment Assignment { get; set; }
        public virtual Staff Staff { get; set; }
    }
}
