﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.DomainModels
{
    public interface IAuditEntity
    {
        Nullable<System.DateTime> CreatedDate { get; set; }
        string CreatedBy { get; set; }
        string ModifiedBy { get; set; }
        Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
