﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.DomainModels
{
    public enum AssetType
    {
        CaseOrLap,
        Monitor,
        Keyboard,
        Mouse,
        Headphone,
        Server,
        None
    }
    
    public enum ReminderType
    {
        Email,
        Popup
    }

    /// <summary>
    /// It's used for Refresh Token.
    /// </summary>
    public enum ApplicationType
    {
        JavaScript = 0,
        NativeConfidential = 1
    }

    public enum RequestStatus : short
    {
        New = 0,
        Sent = 1,
        Approved = 2,
        Denied = 3
    }
}
