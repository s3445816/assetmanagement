﻿using AssetManagement.BusinessLogic.Events;
using AssetManagement.DomainModels;
using AssetManagement.Repositories;
using AssetManagement.Repositories.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace AssetManagement.BusinessLogic
{
    public class AssetBusinessLogic
    {
        private IUnitOfWork _unitOfWork;

        public AssetBusinessLogic() : this(new UnitOfWork()) { }

        public AssetBusinessLogic(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public Asset GetById(int id)
        {
            return this._unitOfWork.AssetRepository.GetById(id);
        }

        public int AddNewAsset(Asset asset)
        {
            Asset inactiveAsset = this._unitOfWork.AssetRepository.Data.SingleOrDefault(a => a.Type == asset.Type && a.Name == asset.Name && a.IsDeleted);
            if (inactiveAsset == null)
            {
                return Add(asset);
            }
            else
            {
                inactiveAsset.Note = asset.Note;
                return Activate(inactiveAsset);
            }
        }

        private int Add(Asset asset)
        {
            asset.CreatedDate = DateTime.Now.ToUniversalTime();

            DomainEvents.Raise(new PreAssetCreatedEvent());

            this._unitOfWork.AssetRepository.Insert(asset);
            this._unitOfWork.SaveChanges();

            DomainEvents.Raise(new AssetCreatedEvent(asset));

            var newAsset = this._unitOfWork.AssetRepository.Single(a => a.Type == asset.Type && a.Name == asset.Name);
            return newAsset != null ? newAsset.Id : -1;
        }

        private int Activate(Asset asset)
        {
            asset.IsDeleted = false;
            asset.CreatedDate = DateTime.Now.ToUniversalTime();
            asset.ModifiedDate = asset.CreatedDate;
            asset.ModifiedBy = asset.CreatedBy;

            DomainEvents.Raise(new PreAssetActivatedEvent());

            this._unitOfWork.AssetRepository.Update(asset);
            this._unitOfWork.SaveChanges();

            DomainEvents.Raise(new AssetActivatedEvent(asset));

            return asset.Id;
        }
        
        public void EditAsset(Asset asset)
        {
            asset.ModifiedDate = DateTime.Now.ToUniversalTime();

            DomainEvents.Raise(new PreAssetEditedEvent());

            this._unitOfWork.AssetRepository.Update(asset);
            this._unitOfWork.SaveChanges();

            DomainEvents.Raise(new AssetEditedEvent(asset));
        }

        public void DeleteAsset(int id)
        {
            DomainEvents.Raise(new PreAssetDeactivatedEvent());

            var deletedAsset = this.GetById(id);
            deletedAsset.ModifiedDate = DateTime.Now.ToUniversalTime();
            this._unitOfWork.AssetRepository.Delete(id);
            this._unitOfWork.SaveChanges();

            DomainEvents.Raise(new AssetDeactivatedEvent(this.GetById(id)));
        }

        public IEnumerable<Asset> Query()
        {
            var queryAsset = this._unitOfWork.AssetRepository.Data.Include(a => a.Assignments);
            return queryAsset.ToList();
        }

        public IEnumerable GetAssetsByAssetType(int id)
        {
            var rs = this._unitOfWork.AssignmentRepository.Data
                                                          .Where(p => p.Asset.Type == id && p.Staff == null)
                                                          .Select(p => new {
                                                              Id = p.Id,
                                                              Name = p.Asset.Name,
                                                              IdentityCode = p.IdentityCode
                                                          });
            
            return rs.ToList();
        }

        public List<AssetType> GetAssetTypes()
        {
            var repository = this._unitOfWork.AssetRepository as AssetRepository;
            return repository.GetAssetTypes();
        }
         
        public AssetType GetAssetTypeById(int id)
        {
            var repository = this._unitOfWork.AssetRepository as AssetRepository;
            return repository.GetAssetTypeById(id);
        }

        public bool Exist(int type, string name)
        {
            return this._unitOfWork.AssetRepository.Data.Any(asset => asset.Type == type && asset.Name.Equals(name, StringComparison.OrdinalIgnoreCase) && !asset.IsDeleted);
        }

        public bool Exist(int id)
        {
            return this._unitOfWork.AssetRepository.Data.Any(asset => asset.Id == id && !asset.IsDeleted);
        }

        public void CreateAssetsByBatch(int assetId, List<string> identityCodes, string createdBy)
        {
            DomainEvents.Raise(new PreAssignmentCreatedByBatchEvent());

            foreach (string code in identityCodes)
            {
                this._unitOfWork.AssignmentRepository.Insert(new Assignment
                {
                    AssetId = assetId,
                    IdentityCode = code,
                    CreatedBy = createdBy,
                    CreatedDate = DateTime.Now.ToUniversalTime()
                });
            }
            this._unitOfWork.SaveChanges();

            DomainEvents.Raise(new AssignmentCreatedByBatchEvent(
                this.GetById(assetId), 
                identityCodes.Select(code => new Assignment
                                    {
                                        AssetId = assetId,
                                        IdentityCode = code,
                                        CreatedBy = createdBy
                                    })
            ));
        }

        public string GetLastAssignmentByAssetId(int id)
        {
            var result = this._unitOfWork.AssignmentRepository.Data
                                                          .Where(p => p.AssetId == id)
                                                          .OrderByDescending(p => p.IdentityCode)
                                                          .FirstOrDefault();
            return result != null ? result.IdentityCode : string.Empty;
        }

        public string GetAssetTypeName(int id)
        {
            var asset = this._unitOfWork.AssetRepository.GetById(id);
            var assetType = (AssetType)asset.Type;
            return assetType.ToString();
        }
    }
}
