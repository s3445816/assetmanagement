﻿using AssetManagement.Common.Models;
using AssetManagement.DomainModels;
using AssetManagement.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Intefaces
{
    public interface IRequestBusinessLogic
    {
        Task<Result> AddNewRequest(Request request);
        List<Request> GetRequests(string createdBy);

        bool IsRequestExisted(DateTime startDate, DateTime endDate);
        bool IsRequestExisted(DateTime startDate, DateTime endDate, string createdBy);

        Task<Result> UpdateStatus(int id, RequestStatus requestStatus, string reason);
    }
}
