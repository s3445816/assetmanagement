﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Intefaces
{
    public interface IEventContainer
    {
        IEnumerable<IEventHandler<T>> Handlers<T>(T TEvent) where T : IEvent;
    }
}
