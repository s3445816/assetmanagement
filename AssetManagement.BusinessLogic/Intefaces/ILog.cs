﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Intefaces
{
    public interface ILog
    {
        void Add(DateTime dataLogged, string content);

        void Delete(int id);
    }
}
