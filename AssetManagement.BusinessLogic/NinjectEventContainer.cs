﻿using AssetManagement.BusinessLogic.Intefaces;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic
{
    public class NinjectEventContainer : IEventContainer
    {
        private readonly IKernel _kernel;

        public NinjectEventContainer(IKernel kernel)
        {
            _kernel = kernel;
        }

        public IEnumerable<IEventHandler<T>> Handlers<T>(T TEvent) where T : IEvent
        {
            return _kernel.GetAll<IEventHandler<T>>();
        }
    }
}
