﻿using AssetManagement.DomainModels;
using AssetManagement.Repositories;
using AssetManagement.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic
{
    public class AssetLogBusinessLogic
    {
        private IUnitOfWork _unitOfWork;

        public AssetLogBusinessLogic() : this(new UnitOfWork()) { }

        public AssetLogBusinessLogic(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public Type GetHistoryType(int Id, string Name)
        {
            var staff = this._unitOfWork.StaffRepository.GetById(Id);
            if (staff.Name == Name)
                return typeof(Staff);

            var asset = this._unitOfWork.AssetRepository.GetById(Id);
            if (asset.Name == Name)
                return typeof(Asset);

            return null;
        }

        public Staff GetStaffById(int Id)
        {
            return this._unitOfWork.StaffRepository.GetById(Id);
        }

        public List<string> GetCurrentAssignments(Staff staff)
        {
            var assetRepository = this._unitOfWork.AssetRepository;

            var list = new List<string>();

            this._unitOfWork.AssignmentRepository.Data
                .Where(p => p.StaffId == staff.Id)
                .ToList()
                .ForEach(p =>
                {
                    list.Add(string.Format("{0} - {1}", assetRepository.GetById(p.AssetId).Name, p.IdentityCode));
                });

            return list;
        }

        public List<string> GetPreviousAssignments(Staff staff)
        {
            var history = this._unitOfWork.AssetLogRepository.Data
                .Where(p => p.StaffId == staff.Id)
                .Select(p => p.AssignmentId);

            var assetRepository = this._unitOfWork.AssetRepository;

            var list = new List<string>();

            this._unitOfWork.AssignmentRepository.Data
                .Where(p => history.Contains(p.Id) && p.StaffId != staff.Id)
                .ToList()
                .ForEach(p =>
                {
                    list.Add(string.Format("{0} - {1}", assetRepository.GetById(p.AssetId).Name, p.IdentityCode));
                });

            return list;
        }

        public Asset GetAssetById(int Id)
        {
            return this._unitOfWork.AssetRepository.GetById(Id);
        }

        public List<Assignment> GetAssignmentsOfAsset(int Id)
        {
            var staff = GetAssetById(Id);
            return GetAssignmentsOfAsset(staff);
        }

        public List<Assignment> GetAssignmentsOfAsset(Asset asset)
        {
            return this._unitOfWork.AssignmentRepository.Data
                .Where(p => p.AssetId == asset.Id)
                .Include(p => p.Asset)
                .Include(p => p.Staff)
                .ToList();
        }

        public List<string> GetPreviousOwnersOfAssignment(Assignment assignment)
        {
            var staffIds = this._unitOfWork.AssetLogRepository.Data
                .Where(p => p.AssignmentId == assignment.Id && p.StaffId != assignment.StaffId)
                .Select(p => p.StaffId)
                .ToList();

            var list = new List<string>();
            staffIds.ForEach(id =>
            {
                list.Add(this._unitOfWork.StaffRepository.GetById(id).Name);
            });

            return list;
        }
    }
}
