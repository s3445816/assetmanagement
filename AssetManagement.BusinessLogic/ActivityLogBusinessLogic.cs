﻿using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.DomainModels;
using AssetManagement.Repositories;
using AssetManagement.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic
{
    public class ActivityLogBusinessLogic : ILog
    {
        private IUnitOfWork _unitOfWork;

        public ActivityLogBusinessLogic() : this(new UnitOfWork()) { }

        public ActivityLogBusinessLogic(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public List<ActivityLog> GetAll()
        {
            var query = this._unitOfWork.ActivityLogRepository.Data;
            return query.ToList();
        }

        public void Add(DateTime dataLogged, string content)
        {
            ActivityLog activityLog = this.Create(dataLogged, content);

            this._unitOfWork.ActivityLogRepository.Insert(activityLog);
            this._unitOfWork.SaveChanges();
        }
        public void Delete(int id)
        {
            this._unitOfWork.ActivityLogRepository.Delete(id);
            this._unitOfWork.SaveChanges();
        }

        protected ActivityLog Create(DateTime dataLogged, string content)
        {
            ActivityLog activityLog = new ActivityLog
            {
                DateOccurred = dataLogged,
                Content = content
            };

            return activityLog;
        }
    }
}
