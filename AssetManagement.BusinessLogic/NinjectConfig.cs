﻿using AssetManagement.BusinessLogic.Events;
using AssetManagement.BusinessLogic.Handlers;
using AssetManagement.BusinessLogic.Handlers.Request;
using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.DomainModels;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic
{
    public sealed class NinjectConfig
    {
        public static void Start()
        {
            CreateKernel();
        }

        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();

            // Register EventHandlers
            kernel.Bind<IEventHandler<StaffCreatedEvent>>().To<LogAfterStaffCreated>();
            kernel.Bind<IEventHandler<AssetActivatedEvent>>().To<LogAfterAssetActivated>();
            kernel.Bind<IEventHandler<AssetCreatedEvent>>().To<LogAfterAssetCreated>();
            kernel.Bind<IEventHandler<AssetDeactivatedEvent>>().To<LogAfterAssetDeactivated>();
            kernel.Bind<IEventHandler<AssetEditedEvent>>().To<LogAfterAssetEdited>();
            kernel.Bind<IEventHandler<AssignmentCreatedByBatchEvent>>().To<LogAfterAssignmentCreatedByBatch>();
            kernel.Bind<IEventHandler<ReminderCreatedEvent>>().To<LogAfterReminderCreated>();
            kernel.Bind<IEventHandler<ReminderDeletedEvent>>().To<LogAfterReminderDeleted>();
            kernel.Bind<IEventHandler<ReminderEditedEvent>>().To<LogAfterReminderEdited>();
            kernel.Bind<IEventHandler<StaffAssetAssignedEvent>>().To<LogAfterStaffAssetAssigned>();
            kernel.Bind<IEventHandler<StaffAssignmentEditedEvent>>().To<LogAfterStaffAssignmentEdited>();
            kernel.Bind<IEventHandler<StaffOfficeChangedEvent>>().To<LogAfterStaffOfficeChanged>();
            kernel.Bind<IEventHandler<StaffResignedEvent>>().To<LogAfterStaffResigned>();

            // -> Request
            kernel.Bind<IEventHandler<PreBaseEvent<Request, string[]>>>().To<LogPreRequestCreated>();
            kernel.Bind<IEventHandler<PostBaseEvent<Request, string[]>>>().To<LogPostRequestCreated>();
            kernel.Bind<IEventHandler<PostBaseEvent<Request, string[]>>>().To<EmailRequest>();

            // Register ILog
            kernel.Bind<ILog>().To<ActivityLogBusinessLogic>();

            DomainEvents.Container = new NinjectEventContainer(kernel);

            return kernel;
        }
    }
}
