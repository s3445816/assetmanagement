﻿using AssetManagement.DomainModels;
using AssetManagement.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic
{
    public class TimeSheetBusinessLogic
    {
        public TimeSheetBusinessLogic()
        {

        }

        public DateTime? GetFirstCheckIn(string staffName, DateTime dateTime)
        {
            var normalizeStaffName = staffName.ConvertToUnsign().ToUpper();
            try
            {
                using (var db = new FingerPrintDbContext())
                {
                    var user = db.USERINFOes.FirstOrDefault(p => p.Name.ToUpper() == normalizeStaffName);

                    if (user == null)
                        throw new NotImplementedException("Staff cannot be found");

                    var checkInTime = db.CHECKINOUTs.Where(p => p.USERID == user.USERID && p.CHECKTIME.Date == dateTime.Date)
                                                    .OrderBy(p => p.CHECKTIME)
                                                    .FirstOrDefault();

                    if (checkInTime == null)
                        return null;

                    return checkInTime.CHECKTIME;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHECKINOUT> GetTimeSheetUntil(string staffName, DateTime dateTime)
        {
            var normalizeStaffName = staffName.ConvertToUnsign().ToUpper();
            try
            {
                using (var db = new FingerPrintDbContext())
                {
                    var user = db.USERINFOes.FirstOrDefault(p => p.Name.ToUpper() == normalizeStaffName);

                    if (user == null)
                        throw new NotImplementedException("Staff cannot be found");

                    var checkInTimes = db.CHECKINOUTs.Where(p => p.USERID == user.USERID && p.CHECKTIME.Month == dateTime.Month
                                                                                         && p.CHECKTIME.Year == dateTime.Year)
                                                     .OrderByDescending(p => p.CHECKTIME)
                                                     .ToList();

                    return checkInTimes;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
