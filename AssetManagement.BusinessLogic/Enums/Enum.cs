﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Enums
{
    public enum ActivityLogMarkup
    {
        Actor,
        Staff,
        Asset,
        Office,
        List,
        Item
    }
}
