﻿using AssetManagement.BusinessLogic.Events;
using AssetManagement.DomainModels;
using AssetManagement.Repositories;
using AssetManagement.Repositories.Interfaces;
using AssetManagement.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using AssetManagement.Common.Models;
using System.Security.Principal;
using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.Common;

namespace AssetManagement.BusinessLogic
{
    public class RequestBusinessLogic : IRequestBusinessLogic
    {
        protected IUnitOfWork unitOfWork { get; set; }
        protected IIdentity identity { get; set; }
        
        public RequestBusinessLogic(IIdentity identity) 
            : this(new UnitOfWork(identity))
        {
            this.identity = identity;
        }

        public RequestBusinessLogic(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<Result> AddNewRequest(Request request)
        {
            string supervisorId = identity.GetClaimValueByType(ConstantNames.Claim.SupervisorId);
            string creator = identity.GetClaimValueByType(ConstantNames.Claim.StaffName);

            string[] kObject = new string[2] { supervisorId, creator };
            DomainEvents.Raise(new PreBaseEvent<Request, string[]>(request, kObject));

            Result result = new Result();

            // Adjust start date and end date
            request.StartDate = request.StartDate.AdjustToStartFrom8AM();
            request.EndDate = request.EndDate.AdjustToEndAt5PM();

            var existedRequest = IsRequestExisted(request.StartDate, request.EndDate);

            if (existedRequest)
            {
                result.Succeeded = false;
                result.Error = "Cannot create multiple requests in a same time range";

                return result;
            }

            // Convert to UTC Time
            request.StartDate = request.StartDate.ToUniversalTime();
            request.EndDate = request.EndDate.ToUniversalTime();

            request.Status = RequestStatus.New;

            unitOfWork.RequestRepository.Insert(request);

            result.Succeeded = await unitOfWork.SaveChangesAsync();

            DomainEvents.Raise(new PostBaseEvent<Request, string[]>(request, kObject));

            return result;
        }

        public List<Request> GetRequests(string createdBy)
        {
            var listRequest = unitOfWork.RequestRepository.Data.Include(i => i.LeaveType)
                                                               .Where(p => p.CreatedBy == createdBy)
                                                               .ToList();

            return listRequest;
        }

        public bool IsRequestExisted(DateTime startDate, DateTime endDate)
        {
            var createdBy = identity.GetClaimValueByType(ConstantNames.Claim.UserId);

            return IsRequestExisted(startDate, endDate, createdBy);
        }

        public bool IsRequestExisted(DateTime startDate, DateTime endDate, string createdBy)
        {
            if (endDate < startDate)
                throw new InvalidOperationException("End date is smaller than start date");

            // Convert to UTC Time before comparing to DB
            startDate = startDate.ToUniversalTime();
            endDate = endDate.ToUniversalTime();

            var existed = unitOfWork.RequestRepository
                                    .Data
                                    .Any(p => p.CreatedBy == createdBy && 
                                             (startDate <= p.EndDate && endDate >= p.StartDate));

            return existed;
        }

        public async Task<Result> UpdateStatus(int id, RequestStatus requestStatus, string reason)
        {
            Result result = new Result();

            var request = await unitOfWork.RequestRepository.GetByIdAsync(id);

            if (request == null)
            {
                result.Succeeded = false;
                result.Error = "Request is not found";
                return result;
            }

            request.Status = requestStatus;

            if(!string.IsNullOrEmpty(reason))
            {
                switch (requestStatus)
                {
                    case RequestStatus.Approved:
                        request.ApprovedReason = reason;
                        break;
                    case RequestStatus.Denied:
                        request.DeniedReason = reason;
                        break;
                    default:
                        break;
                }
            }

            unitOfWork.RequestRepository.Update(request);
            result.Succeeded = await unitOfWork.RequestRepository.SaveChangesAsync();
            return result;
        }
    }
}
