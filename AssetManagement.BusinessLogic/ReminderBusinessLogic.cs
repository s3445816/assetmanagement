﻿using AssetManagement.BusinessLogic.Events;
using AssetManagement.Common.Extensions;
using AssetManagement.DomainModels;
using AssetManagement.Repositories;
using AssetManagement.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AssetManagement.BusinessLogic
{
    public class ReminderBusinessLogic
    {
        private IUnitOfWork _unitOfWork;

        public ReminderBusinessLogic() : this(new UnitOfWork()) { }

        public ReminderBusinessLogic(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public Reminder GetById(int id)
        {
            return this._unitOfWork.ReminderRepository.GetById(id);
        }

        public int AddNewReminder(Reminder reminder)
        {
            reminder.CreatedDate = DateTime.Now.ToUniversalTime();

            DomainEvents.Raise(new PreReminderCreatedEvent());

            this._unitOfWork.ReminderRepository.Insert(reminder);
            this._unitOfWork.SaveChanges();

            DomainEvents.Raise(new ReminderCreatedEvent(reminder));

            var list = this._unitOfWork.ReminderRepository.Data.Where(r => r.Content == reminder.Content).ToList();
            var newReminder = list.Single(r => r.TimeStamp.IsEqual(reminder.TimeStamp) && r.DueTime.Value.IsEqual(reminder.DueTime.Value));
            return newReminder != null ? newReminder.Id : -1;
        }

        public void UpdateReminder(Reminder reminder, string modifiedBy = "")
        {
            reminder.ModifiedDate = DateTime.Now.ToUniversalTime();
            if (!string.IsNullOrEmpty(modifiedBy))
            {
                reminder.ModifiedBy = modifiedBy;
            }

            DomainEvents.Raise(new PreReminderEditedEvent());

            this._unitOfWork.ReminderRepository.Update(reminder);
            this._unitOfWork.SaveChanges();

            DomainEvents.Raise(new ReminderEditedEvent(reminder));
        }

        public void DeleteReminder(int id)
        {
            DomainEvents.Raise(new PreReminderDeletedEvent());

            this._unitOfWork.ReminderRepository.Delete(id);
            this._unitOfWork.SaveChanges();

            DomainEvents.Raise(new ReminderDeletedEvent(this.GetById(id)));
        }

        public IEnumerable<Reminder> Query()
        {
            var queryReminder = this._unitOfWork.ReminderRepository.Data;
            return queryReminder.ToList();
        }
    }
}
