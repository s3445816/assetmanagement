﻿using AssetManagement.BusinessLogic.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Helpers
{
    public static class ActivityLogMarkupHelper
    {
        public static string Create(this ActivityLogMarkup markup, string content, string extra = "")
        {
            if (string.IsNullOrEmpty(extra))
            {
                return string.Format("[{0}]{1}[/{2}]", markup.ToString(), content, markup.ToString());
            }

            return string.Format("[{0}]{1}{{{2}}}[/{3}]", markup.ToString(), content, extra, markup.ToString());
        }
    }
}
