﻿using AssetManagement.BusinessLogic.Events;
using AssetManagement.DomainModels;
using AssetManagement.Repositories;
using AssetManagement.Repositories.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic
{
    public class StaffBusinessLogic
    {
        private IUnitOfWork unitOfWork;

        public StaffBusinessLogic() : this (new UnitOfWork()) { }

        public StaffBusinessLogic(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public Staff GetById(int id)
        {
            return this.unitOfWork.StaffRepository.GetById(id);
        }
        
        public int AddNewStaff(Staff staff)
        {
            DomainEvents.Raise(new PreStaffCreatedEvent());

            this.unitOfWork.StaffRepository.Insert(staff);
            this.unitOfWork.SaveChanges();

            DomainEvents.Raise(new StaffCreatedEvent(staff));

            return staff.Id;
        }

        public void ResignedStaff(Staff staff)
        {
            staff.Resigned = true;
            staff.ModifiedDate = DateTime.Now.ToUniversalTime();

            var assignments = this.unitOfWork.AssignmentRepository.Data.Where(p => p.StaffId == staff.Id);
            foreach (var assignment in assignments)
            {
                assignment.StaffId = null;
                assignment.ModifiedBy = staff.ModifiedBy;
                assignment.ModifiedDate = staff.ModifiedDate;
                this.unitOfWork.AssignmentRepository.Update(assignment);
            }

            DomainEvents.Raise(new PreStaffResignedEvent());

            this.EditStaff(staff);

            DomainEvents.Raise(new StaffResignedEvent(staff));
        }

        public void EditStaff(Staff staff)
        {
            this.unitOfWork.StaffRepository.Update(staff);
            this.unitOfWork.SaveChanges();
        }

        public void ChangingOffice(int staffId, int officeId, string modifiedBy = null)
        {
            var staff = this.GetById(staffId);

            if (staff == null)
                return;

            if (staff.OfficeId == officeId)
                return;

            var previousOfficeId = staff.OfficeId;
            staff.OfficeId = officeId;
            staff.ModifiedBy = modifiedBy;
            staff.ModifiedDate = DateTime.Now.ToUniversalTime();

            DomainEvents.Raise(new PreStaffOfficeChangedEvent());

            this.unitOfWork.SaveChanges();

            var officeRepository = this.unitOfWork.OfficeRepository;

            DomainEvents.Raise(new StaffOfficeChangedEvent(
                this.GetById(staffId), 
                officeRepository.GetById(officeId), 
                officeRepository.GetById(previousOfficeId))
                );
        }

        public void AssignAsset(int staffId, List<int> assetsId, string modifiedBy = null)
        {
            var staff = this.GetById(staffId);
            if (staff == null)
                return;

            staff.ModifiedBy = modifiedBy;
            staff.ModifiedDate = DateTime.Now.ToUniversalTime();

            var list = this.unitOfWork.AssignmentRepository.Data.Where(p => assetsId.Contains(p.Id) && p.StaffId == null).ToList();

            foreach (var item in list)
            {
                item.StaffId = staffId;
                item.ModifiedBy = staff.ModifiedBy;
                item.ModifiedDate = staff.ModifiedDate;
            }

            DomainEvents.Raise(new PreStaffAssetAssignedEvent());

            this.unitOfWork.SaveChanges();

            DomainEvents.Raise(new StaffAssetAssignedEvent(this.GetById(staffId), list));

            // Write asset log after assign
            var logs = list.Select(item => new AssetLog
            {
                StaffId = staffId,
                AssignmentId = item.Id,
                LogDate = item.ModifiedDate.Value
            });
            this.LogAssignment(logs);
        }

        public void EditAssignmentAsset(int staffId, List<int> assetsId, string modifiedBy = null)
        {
            var staff = this.GetById(staffId);
            if (staff == null)
                return;

            staff.ModifiedBy = modifiedBy;
            staff.ModifiedDate = DateTime.Now.ToUniversalTime();

            var listCurrentAsset = this.unitOfWork.AssignmentRepository.Data
                                            .Where(p => p.StaffId == staffId).ToList();

            foreach (var item in listCurrentAsset)
            {
                if (!assetsId.Contains(item.Id))
                {
                    item.StaffId = null;
                    item.ModifiedBy = staff.ModifiedBy;
                    item.ModifiedDate = staff.ModifiedDate;
                }
            }

            var list = this.unitOfWork.AssignmentRepository.Data
                                            .Where(p => assetsId.Contains(p.Id) && p.StaffId == null).ToList();

            foreach (var item in list)
            {
                item.StaffId = staffId;
                item.ModifiedBy = staff.ModifiedBy;
                item.ModifiedDate = staff.ModifiedDate;
            }

            DomainEvents.Raise(new PreStaffAssignmentEditedEvent());

            this.unitOfWork.SaveChanges();

            var currentAssets = this.unitOfWork.AssignmentRepository.Data.Where(p => p.StaffId == staffId).ToList();

            DomainEvents.Raise(new StaffAssignmentEditedEvent(this.GetById(staffId), currentAssets));

            // Write asset log after assign
            var logs = list.Select(item => new AssetLog
            {
                StaffId = staffId,
                AssignmentId = item.Id,
                LogDate = item.ModifiedDate.Value
            });
            this.LogAssignment(logs);
        }

        public IEnumerable GetAssignments(int id)
        {
            var list = this.unitOfWork.AssignmentRepository.Data.Where(p => p.StaffId == id).ToList();

            var rs = list.Select(p => new
                        {
                            p.Id,
                            AssetName = p.Asset.Name,
                            AssetType = Enum.GetName(typeof(AssetType), p.Asset.Type),
                            p.IdentityCode
                        });
            return rs;
        }

        public IEnumerable GetUsedExtList()
        {
            var query = this.unitOfWork.StaffRepository.Data.Select(p => p.Ext).Distinct();
            return query.ToList();
        }
        //Check existed ext number of staff
        public bool Exist(string extNumber)
        {
            if (string.IsNullOrEmpty(extNumber))
            {
                throw new System.ArgumentException("ExtNumber cannot be null", "extNumber");
            }
            return this.unitOfWork.StaffRepository.Data.Any(staff => staff.Ext.Equals(extNumber));
        }
        private void LogAssignment(IEnumerable<AssetLog> logs)
        {
            logs.ToList().ForEach(log =>
            {
                this.unitOfWork.AssetLogRepository.Insert(log);
            });

            this.unitOfWork.SaveChanges();
        }
    }
}
