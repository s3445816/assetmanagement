﻿using AssetManagement.BusinessLogic.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class PreAssignmentCreatedByBatchEvent : IEvent
    {
        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public PreAssignmentCreatedByBatchEvent(DateTime dateOccurred)
        {
            DateOccurred = dateOccurred;
        }

        public PreAssignmentCreatedByBatchEvent()
            : this(DateTime.Now)
        {
        }
    }
}
