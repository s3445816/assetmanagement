﻿using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class AssignmentCreatedByBatchEvent : IEvent
    {
        public Asset Asset { get; set; }
        public IEnumerable<Assignment> Assignments { get; set; }

        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public AssignmentCreatedByBatchEvent(Asset asset, IEnumerable<Assignment> assignments, DateTime dateOccurred)
        {
            Asset = asset;
            Assignments = assignments;
            DateOccurred = dateOccurred;
        }

        public AssignmentCreatedByBatchEvent(Asset asset, IEnumerable<Assignment> assignments)
            : this(asset, assignments, DateTime.Now)
        {
        }
    }
}
