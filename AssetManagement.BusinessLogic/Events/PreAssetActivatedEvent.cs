﻿using AssetManagement.BusinessLogic.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class PreAssetActivatedEvent : IEvent
    {
        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public PreAssetActivatedEvent(DateTime dateOccurred)
        {
            DateOccurred = dateOccurred;
        }

        public PreAssetActivatedEvent()
            : this(DateTime.Now)
        {
        }
    }
}
