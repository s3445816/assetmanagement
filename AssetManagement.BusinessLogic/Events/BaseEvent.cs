﻿using AssetManagement.BusinessLogic.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class BaseEvent<T> : IEvent where T : class
    {
        public T TObject { get; set; }

        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public BaseEvent(T tObject, DateTime dateOccurred)
        {
            TObject = tObject;
            DateOccurred = dateOccurred;
        }

        public BaseEvent(T tObject)
            : this(tObject, DateTime.Now)
        {
        }
    }

    public class PreBaseEvent<T> : BaseEvent<T> where T : class
    {
        public PreBaseEvent(T tObject)
            : base(tObject)
        {
        }

        public PreBaseEvent(T tObject, DateTime dateOccurred)
            : base(tObject, dateOccurred)
        {
        }
    }

    public class PreBaseEvent<T, K> : BaseEvent<T> where T : class
    {
        public K KObject { get; set; }

        public PreBaseEvent(T tObject, K kObject)
            : base(tObject)
        {
            KObject = kObject;
        }

        public PreBaseEvent(T tObject, K kObject, DateTime dateOccurred)
            : base(tObject, dateOccurred)
        {
            KObject = kObject;
        }
    }

    public class PostBaseEvent<T> : BaseEvent<T> where T : class
    {
        public PostBaseEvent(T tObject)
            : base(tObject)
        {
        }

        public PostBaseEvent(T tObject, DateTime dateOccurred)
            : base(tObject, dateOccurred)
        {
        }
    }

    public class PostBaseEvent<T,K> : BaseEvent<T> where T : class
    {
        public K KObject { get; set; }

        public PostBaseEvent(T tObject, K kObject)
            : base(tObject)
        {
            KObject = kObject;
        }

        public PostBaseEvent(T tObject, K kObject, DateTime dateOccurred)
            : base(tObject, dateOccurred)
        {
            KObject = kObject;
        }
    }
}
