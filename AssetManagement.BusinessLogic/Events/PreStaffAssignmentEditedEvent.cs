﻿using AssetManagement.BusinessLogic.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class PreStaffAssignmentEditedEvent : IEvent
    {
        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public PreStaffAssignmentEditedEvent(DateTime dateOccurred)
        {
            DateOccurred = dateOccurred;
        }

        public PreStaffAssignmentEditedEvent()
            : this(DateTime.Now)
        {
        }
    }
}
