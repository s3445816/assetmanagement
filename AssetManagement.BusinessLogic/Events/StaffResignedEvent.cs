﻿using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class StaffResignedEvent : IEvent
    {
        public Staff Staff { get; set; }

        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public StaffResignedEvent(Staff staff, DateTime dateOccurred)
        {
            Staff = staff;
            DateOccurred = dateOccurred;
        }

        public StaffResignedEvent(Staff staff)
            : this(staff, DateTime.Now)
        {
        }
    }
}
