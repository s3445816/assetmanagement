﻿using AssetManagement.BusinessLogic.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class PreStaffResignedEvent : IEvent
    {
        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public PreStaffResignedEvent(DateTime dateOccurred)
        {
            DateOccurred = dateOccurred;
        }

        public PreStaffResignedEvent()
            : this(DateTime.Now)
        {
        }
    }
}
