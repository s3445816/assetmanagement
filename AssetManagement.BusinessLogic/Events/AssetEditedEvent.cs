﻿using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class AssetEditedEvent : IEvent
    {
        public Asset Asset { get; set; }

        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public AssetEditedEvent(Asset asset, DateTime dateOccurred)
        {
            Asset = asset;
            DateOccurred = dateOccurred;
        }

        public AssetEditedEvent(Asset newAsset)
            : this(newAsset, DateTime.Now)
        {
        }
    }
}
