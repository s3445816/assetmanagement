﻿using AssetManagement.BusinessLogic.Intefaces;
using System;
using System.Collections.Generic;

namespace AssetManagement.BusinessLogic.Events
{
    public static class DomainEvents
    {
        [ThreadStatic]
        // Each thread has its own callbacks
        private static List<Delegate> actions;

        public static IEventContainer Container { get; set; }

        // Registers a callback for the given domain event
        public static void Register<T>(Action<T> callback) where T : IEvent
        {
            if (actions == null)
                actions = new List<Delegate>();

            actions.Add(callback);
        }

        // Clears callbacks passed to Register on the current thread
        public static void ClearCallbacks()
        {
            actions = null;
        }

        public static void Raise<T>(T agrs) where T : IEvent
        {
            if (Container != null)
            {
                foreach (var handler in Container.Handlers(agrs))
                {
                    handler.Handle(agrs);
                }
            }

            // Registered actions, typically sed for unit test
            if (actions != null)
            {
                foreach (var action in actions)
                {
                    if(action is Action<T>)
                        ((Action<T>)action)(agrs);
                }
            }
        }
    }
}
