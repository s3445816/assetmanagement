﻿using AssetManagement.BusinessLogic.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class PreAssetDeactivatedEvent : IEvent
    {
        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public PreAssetDeactivatedEvent(DateTime dateOccurred)
        {
            DateOccurred = dateOccurred;
        }

        public PreAssetDeactivatedEvent()
            : this(DateTime.Now)
        {
        }
    }
}
