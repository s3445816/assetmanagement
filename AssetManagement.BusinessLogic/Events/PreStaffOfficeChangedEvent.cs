﻿using AssetManagement.BusinessLogic.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class PreStaffOfficeChangedEvent : IEvent
    {
        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public PreStaffOfficeChangedEvent(DateTime dateOccurred)
        {
            DateOccurred = dateOccurred;
        }

        public PreStaffOfficeChangedEvent()
            : this(DateTime.Now)
        {
        }
    }
}
