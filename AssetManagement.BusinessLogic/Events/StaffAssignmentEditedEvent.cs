﻿using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class StaffAssignmentEditedEvent : IEvent
    {
        public Staff Staff { get; set; }
        public IEnumerable<Assignment> Assignments { get; set; }

        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public StaffAssignmentEditedEvent(Staff staff, IEnumerable<Assignment> assignments, DateTime dateOccurred)
        {
            Staff = staff;
            Assignments = assignments;
            DateOccurred = dateOccurred;
        }

        public StaffAssignmentEditedEvent(Staff staff, IEnumerable<Assignment> assignments)
            : this(staff, assignments, DateTime.Now)
        {
        }
    }
}
