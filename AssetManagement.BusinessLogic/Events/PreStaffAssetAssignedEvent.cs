﻿using AssetManagement.BusinessLogic.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class PreStaffAssetAssignedEvent : IEvent
    {
        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public PreStaffAssetAssignedEvent(DateTime dateOccurred)
        {
            DateOccurred = dateOccurred;
        }

        public PreStaffAssetAssignedEvent()
            : this(DateTime.Now)
        {
        }
    }
}
