﻿using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class ReminderEditedEvent : IEvent
    {
        public Reminder Reminder { get; set; }

        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public ReminderEditedEvent(Reminder reminder, DateTime dateOccurred)
        {
            Reminder = reminder;
            DateOccurred = dateOccurred;
        }

        public ReminderEditedEvent(Reminder newReminder)
            : this(newReminder, DateTime.Now)
        {
        }
    }
}
