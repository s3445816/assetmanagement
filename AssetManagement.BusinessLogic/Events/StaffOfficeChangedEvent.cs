﻿using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class StaffOfficeChangedEvent : IEvent
    {
        public Staff Staff { get; set; }
        public Office NewOffice { get; set; }
        public Office PreviousOffice { get; set; }

        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public StaffOfficeChangedEvent(Staff staff, Office newOffice, Office previousOffice, DateTime dateOccurred)
        {
            Staff = staff;
            NewOffice = newOffice;
            PreviousOffice = previousOffice;
            DateOccurred = dateOccurred;
        }

        public StaffOfficeChangedEvent(Staff staff, Office newOffice, Office previousOffice)
            : this(staff, newOffice, previousOffice, DateTime.Now)
        {
        }
    }
}
