﻿using AssetManagement.BusinessLogic.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class PreReminderEditedEvent : IEvent
    {
        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public PreReminderEditedEvent(DateTime dateOccurred)
        {
            DateOccurred = dateOccurred;
        }

        public PreReminderEditedEvent()
            : this(DateTime.Now)
        {
        }
    }
}
