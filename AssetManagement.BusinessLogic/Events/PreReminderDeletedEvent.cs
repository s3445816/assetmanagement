﻿using AssetManagement.BusinessLogic.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class PreReminderDeletedEvent : IEvent
    {
        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public PreReminderDeletedEvent(DateTime dateOccurred)
        {
            DateOccurred = dateOccurred;
        }

        public PreReminderDeletedEvent()
            : this(DateTime.Now)
        {
        }
    }
}
