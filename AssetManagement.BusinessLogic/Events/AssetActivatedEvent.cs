﻿using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class AssetActivatedEvent : IEvent
    {
        public Asset Asset { get; set; }

        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public AssetActivatedEvent(Asset asset, DateTime dateOccurred)
        {
            Asset = asset;
            DateOccurred = dateOccurred;
        }

        public AssetActivatedEvent(Asset asset)
            : this(asset, DateTime.Now)
        {
        }
    }
}
