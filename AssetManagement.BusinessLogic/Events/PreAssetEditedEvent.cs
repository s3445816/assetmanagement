﻿using AssetManagement.BusinessLogic.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class PreAssetEditedEvent : IEvent
    {
        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public PreAssetEditedEvent(DateTime dateOccurred)
        {
            DateOccurred = dateOccurred;
        }

        public PreAssetEditedEvent()
            : this(DateTime.Now)
        {
        }
    }
}
