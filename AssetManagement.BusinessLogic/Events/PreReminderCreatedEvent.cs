﻿using AssetManagement.BusinessLogic.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class PreReminderCreatedEvent : IEvent
    {
        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public PreReminderCreatedEvent(DateTime dateOccurred)
        {
            DateOccurred = dateOccurred;
        }

        public PreReminderCreatedEvent()
            : this(DateTime.Now)
        {
        }
    }
}
