﻿using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class ReminderCreatedEvent : IEvent
    {
        public Reminder Reminder { get; set; }

        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public ReminderCreatedEvent(Reminder reminder, DateTime dateOccurred)
        {
            Reminder = reminder;
            DateOccurred = dateOccurred;
        }

        public ReminderCreatedEvent(Reminder reminder)
            : this(reminder, DateTime.Now)
        {
        }
    }
}
