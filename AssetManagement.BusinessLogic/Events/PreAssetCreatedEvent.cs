﻿using AssetManagement.BusinessLogic.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Events
{
    public class PreAssetCreatedEvent : IEvent
    {
        public DateTime DateOccurred
        {
            get;
            private set;
        }

        public PreAssetCreatedEvent(DateTime dateOccurred)
        {
            DateOccurred = dateOccurred;
        }

        public PreAssetCreatedEvent()
            : this(DateTime.Now)
        {
        }
    }
}
