﻿using AssetManagement.BusinessLogic.Events;
using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.DomainModels;
using AssetManagement.BusinessLogic.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssetManagement.BusinessLogic.Enums;

namespace AssetManagement.BusinessLogic.Handlers
{
    public class LogAfterAssignmentCreatedByBatch : LogEventHandler<AssignmentCreatedByBatchEvent>
    {
        public LogAfterAssignmentCreatedByBatch(ILog logger)
            : base(logger) { }

        public override void LogInfo(AssignmentCreatedByBatchEvent args)
        {
            var entity = args.Asset;
            var childEntities = args.Assignments;

            if (entity == null || childEntities == null || childEntities.Count() == 0)
                return;

            var actor = ActivityLogMarkup.Actor.Create(childEntities.First().CreatedBy);

            var asset = ActivityLogMarkup.Asset.Create(entity.Name, entity.Id.ToString());

            var content = string.Format("{0} created a batch of asset {1} with identity code from {2} to {3}", actor, asset, childEntities.First().IdentityCode, childEntities.Last().IdentityCode);

            this.Log.Add(args.DateOccurred, content);
        }
    }
}
