﻿using AssetManagement.BusinessLogic.Enums;
using AssetManagement.BusinessLogic.Events;
using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.BusinessLogic.Helpers;
using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Handlers
{
    public class LogAfterStaffOfficeChanged : LogEventHandler<StaffOfficeChangedEvent>
    {
        public LogAfterStaffOfficeChanged(ILog logger)
            : base(logger) { }

        public override void LogInfo(StaffOfficeChangedEvent args)
        {
            var entity = args.Staff;

            if (entity == null)
                return;

            var actor = ActivityLogMarkup.Actor.Create(entity.ModifiedBy);

            var staff = ActivityLogMarkup.Staff.Create(entity.Name, entity.Id.ToString());

            var previousOffice = ActivityLogMarkup.Office.Create(args.PreviousOffice.Name);

            var newOffice = ActivityLogMarkup.Office.Create(args.NewOffice.Name);

            var content = string.Format("{0} changed office for staff {1} from {2} to {3}", actor, staff, previousOffice, newOffice);

            this.Log.Add(args.DateOccurred, content);
        }
    }
}
