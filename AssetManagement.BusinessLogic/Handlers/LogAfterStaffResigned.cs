﻿using AssetManagement.BusinessLogic.Enums;
using AssetManagement.BusinessLogic.Events;
using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.BusinessLogic.Helpers;
using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Handlers
{
    public class LogAfterStaffResigned : LogEventHandler<StaffResignedEvent>
    {
        public LogAfterStaffResigned(ILog logger)
            : base(logger) { }

        public override void LogInfo(StaffResignedEvent args)
        {
            var entity = args.Staff;

            if (entity == null)
                return;

            var actor = ActivityLogMarkup.Actor.Create(entity.ModifiedBy);

            var staff = ActivityLogMarkup.Staff.Create(entity.Name, entity.Id.ToString());

            var content = string.Format("{0} resigned staff {1}", actor, staff);

            this.Log.Add(args.DateOccurred, content);
        }
    }
}
