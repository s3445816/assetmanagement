﻿using AssetManagement.BusinessLogic.Enums;
using AssetManagement.BusinessLogic.Events;
using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.BusinessLogic.Helpers;

namespace AssetManagement.BusinessLogic.Handlers
{
    public class LogAfterStaffCreated : LogEventHandler<StaffCreatedEvent>
    {
        public LogAfterStaffCreated(ILog logger)
            : base(logger) { }

        public override void LogInfo(StaffCreatedEvent args)
        {
            var entity = args.Staff;

            if (entity == null)
                return;

            var actor = ActivityLogMarkup.Actor.Create(entity.CreatedBy);

            var staff = ActivityLogMarkup.Staff.Create(entity.Name, entity.Id.ToString());

            var content = string.Format(
                "{0} created new staff {1} {2}",
                actor,
                staff,
                !string.IsNullOrEmpty(entity.Ext) ? string.Format("with ext number {0}", entity.Ext) : string.Empty);

            this.Log.Add(args.DateOccurred, content);
        }
    }
}
