﻿using AssetManagement.BusinessLogic.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Handlers
{
    public abstract class LogEventHandler<T> : IEventHandler<T> where T : IEvent
    {
        protected ILog Log;

        public LogEventHandler(ILog log)
        {
            this.Log = log;
        }

        public void Handle(T args)
        {
            this.LogInfo(args);
        }

        public abstract void LogInfo(T args);
    }
}
