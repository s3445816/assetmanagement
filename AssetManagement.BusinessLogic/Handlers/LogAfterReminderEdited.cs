﻿using AssetManagement.BusinessLogic.Enums;
using AssetManagement.BusinessLogic.Events;
using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.BusinessLogic.Helpers;
using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Handlers
{
    public class LogAfterReminderEdited : LogEventHandler<ReminderEditedEvent>
    {
        public LogAfterReminderEdited(ILog logger)
            : base(logger) { }

        public override void LogInfo(ReminderEditedEvent args)
        {
            var reminder = args.Reminder;

            if (reminder == null)
                return;

            var actor = ActivityLogMarkup.Actor.Create(reminder.ModifiedBy);

            var reminderStr = string.Format("{0} from {1} to {2} - status {3}",
                reminder.Content, reminder.TimeStamp.ToShortDateString(), reminder.DueTime.Value.ToShortDateString(), reminder.Completed ? "Completed" : "Incompleted");

            var content = string.Format("{0} edited reminder to {1}", actor, reminderStr);

            this.Log.Add(args.DateOccurred, content);
        }
    }
}
