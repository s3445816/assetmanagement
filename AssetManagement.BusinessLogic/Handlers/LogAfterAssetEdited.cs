﻿using AssetManagement.BusinessLogic.Enums;
using AssetManagement.BusinessLogic.Events;
using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.BusinessLogic.Helpers;
using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Handlers
{
    public class LogAfterAssetEdited : LogEventHandler<AssetEditedEvent>
    {
        public LogAfterAssetEdited(ILog logger)
            : base(logger) { }

        public override void LogInfo(AssetEditedEvent args)
        {
            var asset = args.Asset;

            if (asset == null)
                return;

            var actor = ActivityLogMarkup.Actor.Create(asset.ModifiedBy);

            var assetStr = ActivityLogMarkup.Asset.Create(asset.Name, asset.Id.ToString());

            var content = string.Format("{0} edited asset to {1}", actor, assetStr);

            this.Log.Add(args.DateOccurred, content);
        }
    }
}
