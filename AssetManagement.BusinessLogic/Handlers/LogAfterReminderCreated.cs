﻿using AssetManagement.BusinessLogic.Enums;
using AssetManagement.BusinessLogic.Events;
using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.BusinessLogic.Helpers;
using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Handlers
{
    public class LogAfterReminderCreated : LogEventHandler<ReminderCreatedEvent>
    {
        public LogAfterReminderCreated(ILog logger)
            : base(logger) { }

        public override void LogInfo(ReminderCreatedEvent args)
        {
            var entity = args.Reminder;

            if (entity == null)
                return;

            var actor = ActivityLogMarkup.Actor.Create(entity.CreatedBy);

            var content = string.Format("{0} created new reminder with content: {1}", actor, args.Reminder.Content);

            this.Log.Add(args.DateOccurred, content);
        }
    }
}
