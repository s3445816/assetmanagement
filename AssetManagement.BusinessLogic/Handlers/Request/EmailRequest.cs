﻿using AssetManagement.BusinessLogic.Events;
using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.Common.Services;
using AssetManagement.Repositories;

namespace AssetManagement.BusinessLogic.Handlers.Request
{
    public class EmailRequest : IEventHandler<PostBaseEvent<DomainModels.Request, string[]>>
    {
        public EmailRequest()
        { 
        }

        public void Handle(PostBaseEvent<DomainModels.Request, string[]> args)
        {
            int supervisorId = int.Parse(args.KObject[0]);
            string creator = args.KObject[1];

            DomainModels.Request request = args.TObject;

            UnitOfWork unitOfWork = new UnitOfWork();
            var staff = unitOfWork.StaffRepository.GetById(supervisorId);

            string from = "from@mail.com";
            string to = "to@mail.com";
            string subject = creator + " creates a new request";
            string body =
                "Hi " + staff.Name + "<br/>" +
                creator + " creates a new request" + "<br/>" +
                "- From: " + request.StartDate.ToLocalTime() +
                "- To: " + request.EndDate.ToLocalTime() +
                "- Reason: " + request.Reason +
                "<br/><br/>" +
                "<a href='http://localhost:8090/#/request/approve/" + request.Id + "'>Approve</a>" + "  |  " +
                "<a href='http://localhost:8090/#/request/deny/" + request.Id + "'>Deny</a>" +
                "<br/><br/>" +
                "Best regards"
                ;

            EmailMessageService emailService = new EmailMessageService(from, to, subject, body);
            emailService.Send();
        }
    }

}
