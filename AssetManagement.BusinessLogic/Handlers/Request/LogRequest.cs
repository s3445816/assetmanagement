﻿using AssetManagement.BusinessLogic.Enums;
using AssetManagement.BusinessLogic.Events;
using AssetManagement.BusinessLogic.Helpers;
using AssetManagement.BusinessLogic.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Handlers.Request
{    
    public class LogPreRequestCreated : LogEventHandler<PreBaseEvent<DomainModels.Request, string[]>>
    {
        public LogPreRequestCreated(ILog logger)
            : base(logger)
        { }

        public override void LogInfo(PreBaseEvent<DomainModels.Request, string[]> args)
        {
            var entity = args.TObject;

            if (entity == null)
                return;

            var actor = ActivityLogMarkup.Actor.Create(entity.CreatedBy);

            var content = string.Format("Pre {0} created new request", actor);

            this.Log.Add(args.DateOccurred, content);
        }

    }

    public class LogPostRequestCreated : LogEventHandler<PostBaseEvent<DomainModels.Request, string[]>>
    {
        public LogPostRequestCreated(ILog logger)
            : base(logger)
        { }

        public override void LogInfo(PostBaseEvent<DomainModels.Request, string[]> args)
        {
            var entity = args.TObject;

            if (entity == null)
                return;

            var actor = ActivityLogMarkup.Actor.Create(entity.CreatedBy);

            var content = string.Format("Post {0} created new request", actor);

            this.Log.Add(args.DateOccurred, content);
        }

    }

}
