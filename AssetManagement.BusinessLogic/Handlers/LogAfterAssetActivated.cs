﻿using AssetManagement.BusinessLogic.Enums;
using AssetManagement.BusinessLogic.Events;
using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.BusinessLogic.Helpers;
using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Handlers
{
    public class LogAfterAssetActivated : LogEventHandler<AssetActivatedEvent>
    {
        public LogAfterAssetActivated(ILog logger)
            : base(logger) { }

        public override void LogInfo(AssetActivatedEvent args)
        {
            var entity = args.Asset;

            if (entity == null)
                return;

            var actor = ActivityLogMarkup.Actor.Create(entity.ModifiedBy);

            var asset = ActivityLogMarkup.Asset.Create(entity.Name, entity.Id.ToString());

            var content = string.Format("{0} re-created asset {1}", actor, asset);

            this.Log.Add(args.DateOccurred, content);
        }
    }
}
