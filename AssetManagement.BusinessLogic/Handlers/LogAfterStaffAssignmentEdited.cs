﻿using AssetManagement.BusinessLogic.Enums;
using AssetManagement.BusinessLogic.Events;
using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.BusinessLogic.Helpers;
using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Handlers
{
    public class LogAfterStaffAssignmentEdited : LogEventHandler<StaffAssignmentEditedEvent>
    {
        public LogAfterStaffAssignmentEdited(ILog logger)
            : base(logger) { }

        public override void LogInfo(StaffAssignmentEditedEvent args)
        {
            var entity = args.Staff;

            if (entity == null)
                return;

            var assetBusinessLogic = new AssetBusinessLogic();

            var actor = ActivityLogMarkup.Actor.Create(entity.ModifiedBy);

            var staff = ActivityLogMarkup.Staff.Create(entity.Name, entity.Id.ToString());
            
            var assignments = "";
            args.Assignments.ToList().ForEach(item => 
                {
                    var asset = assetBusinessLogic.GetById(item.AssetId);

                    assignments = string.Format(
                        "{0}{1}",
                        assignments,
                        ActivityLogMarkup.Item.Create(string.Format(
                            "{0} - {1}",
                            ActivityLogMarkup.Asset.Create(asset.Name, asset.Id.ToString()),
                            item.IdentityCode)));
                });
            assignments = ActivityLogMarkup.List.Create(assignments);

            var content = string.Format(
                "{0} edited assignments of staff {1} to {2}",
                actor, staff, assignments);

            this.Log.Add(args.DateOccurred, content);
        }
    }
}
