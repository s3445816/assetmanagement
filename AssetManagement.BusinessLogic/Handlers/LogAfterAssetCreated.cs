﻿using AssetManagement.BusinessLogic.Enums;
using AssetManagement.BusinessLogic.Events;
using AssetManagement.BusinessLogic.Intefaces;
using AssetManagement.BusinessLogic.Helpers;
using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.BusinessLogic.Handlers
{
    public class LogAfterAssetCreated : LogEventHandler<AssetCreatedEvent>
    {
        public LogAfterAssetCreated(ILog logger)
            : base(logger) { }

        public override void LogInfo(AssetCreatedEvent args)
        {
            var entity = args.Asset;

            if (entity == null)
                return;

            var actor = ActivityLogMarkup.Actor.Create(entity.CreatedBy);

            var asset = ActivityLogMarkup.Asset.Create(entity.Name, entity.Id.ToString());

            var content = string.Format("{0} created new asset {1}", actor, asset);

            this.Log.Add(args.DateOccurred, content);
        }

    }
}
