﻿How to use Identity with existing database
1. Naming database name
	- Change connection string name in Web.config
	  It's database name when use type update-database using Package Manager Console
	- Update connection string in referenced places
1. Open Package Manager Console
2. type:
	enable-mirgations
	add-migration InitialCreate
   
   enable-migrations command creates a "Migrations" folder in the project.
   It creates a file named "Configuration", this file contains method named “Seed” 
   which is used to allow us to insert or update test/initial data after code first 
   creates or updates the database.

3. type:
	update-database