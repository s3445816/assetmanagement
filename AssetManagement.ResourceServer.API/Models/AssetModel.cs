﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetManagement.API.Models
{
    public class AssetBatchModel
    {
        public int AssetId { get; set; }

        public List<string> IdentityCodes { get; set; }
    }

    public class AssetViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }

        public int Type { get; set; }
    }
}