﻿using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetManagement.ResourceServer.API.Models
{
    public class StaffHistoryModel
    {
        public int StaffId { get; set; }

        public string StaffName { get; set; }

        public IEnumerable<string> PreviousAssignments { get; set; }

        public IEnumerable<string> CurrentAssignments { get; set; }
    }

    public class AssetHistoryItem
    {
        public int AssignmentId { get; set; }

        public string IdentityCode { get; set; }

        public string CurrentOwner { get; set; }

        public IEnumerable<string> PreviousOwners { get; set; }
    }

    public class AssetHistoryModel
    {
        public int AssetId { get; set; }

        public string AssetName { get; set; }

        public IEnumerable<AssetHistoryItem> Items { get; set; }
    }
}