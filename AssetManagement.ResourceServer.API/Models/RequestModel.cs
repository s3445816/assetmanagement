﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetManagement.ResourceServer.API.Models
{
    public class RequestAddNewModel
    {
        public string Reason { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int LeaveType { get; set; }
    }

    public class RequestCalendarModel
    {
        public string Title { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string LeaveType { get; set; }
    }
}