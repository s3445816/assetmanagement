﻿using AssetManagement.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetManagement.ResourceServer.API.Models
{
    public class ReminderGroupModel
    {
        public DateTime TimeStamp 
        {
            get
            {
                if (Reminders.Count > 0)
                    return Reminders.Single().TimeStamp;
                return default(DateTime);
            }
        }

        public List<Reminder> Reminders { get; set; }
    }

    public class ReminderGroupViewModel
    {
        public int Count
        {
            get { return Items != null ? Items.Count : 0; }
        }

        public List<ReminderGroupModel> Items { get; set; }
    }

    public class ReminderViewModel
    {
        public int Id { get; set; }

        public DateTime TimeStamp { get; set; }

        public DateTime DueTime { get; set; }

        public string Content { get; set; }

        public bool Completed { get; set; }
    }
}