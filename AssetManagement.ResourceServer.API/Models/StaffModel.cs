﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AssetManagement.ResourceServer.API.Models
{
    public class StaffRegisterModel
    {
        public string Name { get; set; }
        public string Ext { get; set; }
        public string Note { get; set; }

        public int OfficeId { get; set; }
    }

    public class StaffOfficeModel
    {
        public int Id { get; set; }
        public int OfficeId { get; set; }
    }

    public class StaffAssignmentModel
    {
        public int StaffId { get; set; }
        public List<int> AssetsId { get; set; }
    }

    public class StaffReportModel
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Ext { get; set; }
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
        public bool Resigned { get; set; }
        public IDictionary<string, object> DynamicProperty { get; set; }
    }
}