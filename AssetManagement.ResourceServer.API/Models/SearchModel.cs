﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetManagement.ResourceServer.API.Models
{
    public class SearchModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Ext { get; set; }
        public OfficeSearchModel Office { get; set; }
        public List<AssetSearchModel> Assets { get; set; }
    }

    public class OfficeSearchModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class AssetSearchModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}