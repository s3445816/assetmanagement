﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetManagement.ResourceServer.API.Models
{
    public class RoleReturnModel
    {
        public string Id { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
    }
}