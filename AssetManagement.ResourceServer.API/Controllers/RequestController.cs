﻿using AssetManagement.BusinessLogic;
using AssetManagement.BusinessLogic.Helpers;
using AssetManagement.Common;
using AssetManagement.Common.Extensions;
using AssetManagement.DomainModels;
using AssetManagement.ResourceServer.API.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AssetManagement.ResourceServer.API.Controllers
{
    [Attributes.Authorize(Roles="Admin")]
    [RoutePrefix("api/request")]
    public class RequestController : ApiController
    {
        protected RequestBusinessLogic _requestBusinessLogic;

        public RequestController()
        {
            _requestBusinessLogic = new RequestBusinessLogic(User.Identity);
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IHttpActionResult> Create([FromBody] RequestAddNewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            DomainModels.Request entity = Mapper.Map<DomainModels.Request>(model);
            
            var result = await _requestBusinessLogic.AddNewRequest(entity);

            if (!result.Succeeded)
                return BadRequest(result.Error);

            return Ok();
        }

        [HttpGet]
        [Route("Events", Name = "GetEvents")]
        public IHttpActionResult GetEvents()
        {
            var staffId = User.Identity.GetClaimValueByType(ConstantNames.Claim.UserId);

            var listRequest = _requestBusinessLogic.GetRequests(staffId);

            List<RequestCalendarModel> rs = Mapper.Map<List<RequestCalendarModel>>(listRequest);

            return Ok(rs);
        }

        [HttpGet]
        [Route("Approve")]
        public async Task<IHttpActionResult> AprroveRequest([FromUri] int id)
        {
            var result = await _requestBusinessLogic.UpdateStatus(id, RequestStatus.Approved, string.Empty);

            if (!result.Succeeded)
                return BadRequest(result.Error);

            return Ok();
        }

        [HttpGet]
        [Route("Deny")]
        public async Task<IHttpActionResult> DenyRequest([FromUri] int id)
        {
            var result = await _requestBusinessLogic.UpdateStatus(id, RequestStatus.Denied, string.Empty);

            if (!result.Succeeded)
                return BadRequest(result.Error);

            return Ok();
        }
    }
}
