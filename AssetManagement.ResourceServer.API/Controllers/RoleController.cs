﻿using AssetManagement.ResourceServer.API.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace AssetManagement.ResourceServer.API.Controllers
{
    [RoutePrefix("api/roles")]
    [Attributes.Authorize(Roles = "Admin")]
    public class RoleController : BaseApiController
    {
        public RoleController()
        {
        }

        [Route("{id:guid}", Name = "GetRoleById")]
        public async Task<IHttpActionResult> GetRole(string id)
        {
            var role = await this.AppRoleManager.FindByIdAsync(id);

            if (role == null)
                return NotFound();

            return Ok(new RoleReturnModel
            {
                Id = role.Id,
                Name = role.Name
            });
        }

        [Route("", Name = "GetAllRoles")]
        public IHttpActionResult GetAllRoles()
        {
            var roles = this.AppRoleManager.Roles;

            return Ok(roles);
        }

        [Route("Create")]
        public async Task<IHttpActionResult> Create(RoleModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var role = new IdentityRole
            {
                Name = model.Name
            };

            var result = await this.AppRoleManager.CreateAsync(role);

            if (!result.Succeeded)
                return GetErrorResult(result);

            Uri location = new Uri(Url.Link("GetRoleById", new { id = role.Id }));
            return Created(location, new RoleReturnModel
            {
                Id = role.Id,
                Name = role.Name
            });
        }

        [Route("{id:guid}")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteRole(string id)
        {
            var role = await this.AppRoleManager.FindByIdAsync(id);

            if (role == null)
                return NotFound();

            IdentityResult result = await this.AppRoleManager.DeleteAsync(role);

            if (!result.Succeeded)
                return GetErrorResult(result);

            return Ok();
        }

        [Route("ManageUsersInRole")]
        [HttpPost]
        public async Task<IHttpActionResult> ManageUsersInRole(UsersInRoleModel model)
        {
            var role = await this.AppRoleManager.FindByIdAsync(model.Id);

            if(role == null)
            {
                ModelState.AddModelError("", "Role does not exist");
                return BadRequest(ModelState);
            }

            foreach (string user in model.EnrolledUsers)
            {
                if (string.IsNullOrEmpty(user)) continue;

                var appUser = await this.AppUserManager.FindByIdAsync(user);

                if(appUser == null)
                {
                    ModelState.AddModelError("", string.Format("User: {0} does not exist.", user));
                    continue;
                }

                if(!this.AppUserManager.IsInRole(user, role.Name))
                {
                    IdentityResult result = await this.AppUserManager.AddToRoleAsync(user, role.Name);

                    if (!result.Succeeded)
                        ModelState.AddModelError("", string.Format("User: {0} could not be added to role", user));
                }
            }

            foreach (string user in model.RemovedUsers)
            {
                if (string.IsNullOrEmpty(user)) continue;

                var appUser = await this.AppUserManager.FindByIdAsync(user);

                if (appUser == null)
                {
                    ModelState.AddModelError("", string.Format("User: {0} does not exist.", user));
                    continue;
                }

                IdentityResult result = await this.AppUserManager.RemoveFromRoleAsync(user, role.Name);

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", String.Format("User: {0} could not be removed from role", user));
                }
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok();
        }
    }
}