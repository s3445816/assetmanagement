﻿using AssetManagement.BusinessLogic;
using AssetManagement.BusinessLogic.Helpers;
using AssetManagement.Common;
using AssetManagement.Common.Extensions;
using AssetManagement.DomainModels;
using AssetManagement.ResourceServer.API.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AssetManagement.ResourceServer.API.Controllers
{
    [RoutePrefix("api/timesheet")]
    public class TimeSheetController : ApiController
    {
        protected TimeSheetBusinessLogic timeSheetBusinessLogic;

        public TimeSheetController()
        {
            timeSheetBusinessLogic = new TimeSheetBusinessLogic();
        }
        
        [HttpGet]
        [Route("GetFirstCheckInToDay")]
        public IHttpActionResult GetFirstCheckInToDay()
        {
            string name = User.Identity.GetClaimValueByType(ConstantNames.Claim.StaffName);

            if (string.IsNullOrEmpty(name))
                return Unauthorized();

            DateTime date = new DateTime(2015, 10, 21);
            var result = timeSheetBusinessLogic.GetFirstCheckIn(name, date);

            return Ok(result);
        }

        [HttpGet]
        [Route("GetCheckInFromBeginingOfMonth")]
        public IHttpActionResult GetCheckInFromBeginingOfMonth()
        {
            string name = User.Identity.GetClaimValueByType(ConstantNames.Claim.StaffName);

            if (string.IsNullOrEmpty(name))
                return Unauthorized();

            DateTime date = new DateTime(2015, 10, 21);
            var result = timeSheetBusinessLogic.GetTimeSheetUntil(name, date);

            return Ok(result);
        }

    }
}
