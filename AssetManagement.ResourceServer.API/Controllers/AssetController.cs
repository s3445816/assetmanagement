﻿using AssetManagement.API.Models;
using AssetManagement.BusinessLogic;
using AssetManagement.DomainModels;
using AutoMapper;
using System;
using System.Linq;
using System.Web.Http;

namespace AssetManagement.ResourceServer.API.Controllers
{
    [Attributes.Authorize]
    [RoutePrefix("api/Asset")]
    public class AssetController : ApiController
    {
        private AssetBusinessLogic assetBusinessLogic;

        public AssetController()
        {
            this.assetBusinessLogic = new AssetBusinessLogic();
        }

        [HttpGet]
        [Route("GetAssetTypes")]
        public IHttpActionResult GetAssetTypes()
        {
            var result = this.assetBusinessLogic.GetAssetTypes().Select(type => new { Value = type, Display = type.ToString() });
            return Ok(result);            
        }

        [HttpGet]
        [Route("GetAssetsByAssetType")]
        public IHttpActionResult GetAssetsByAssetType([FromUri] int id)
        {
            var rs = this.assetBusinessLogic.GetAssetsByAssetType(id);
            return Ok(rs);
        }

        [HttpPost]
        [Route("AddNewAsset")]
        public IHttpActionResult Create([FromBody] AssetViewModel asset)
        {
            var exist = this.assetBusinessLogic.Exist(asset.Type, asset.Name);
            if (exist)
            {
                return BadRequest();
            }

            Asset entity = Mapper.Map<Asset>(asset);
            entity.CreatedBy = User.Identity.Name;

            var result = this.assetBusinessLogic.AddNewAsset(entity);
            return Ok(result);
        }

        [HttpPost]
        [Route("EditAsset")]
        public IHttpActionResult Edit([FromBody] AssetViewModel asset)
        {
            var exist = this.assetBusinessLogic.Exist(asset.Id);
            if (!exist)
            {
                return BadRequest();
            }

            Asset entity = this.assetBusinessLogic.GetById(asset.Id);
            Mapper.Map(asset, entity);
            entity.ModifiedBy = User.Identity.Name;

            this.assetBusinessLogic.EditAsset(entity);
            return Ok();
        }

        [HttpPost]
        [Route("DeleteAsset")]
        public IHttpActionResult Delete([FromBody] int id)
        {
            var exist = this.assetBusinessLogic.Exist(id);
            if (!exist)
            {
                return BadRequest();
            }

            Asset entity = this.assetBusinessLogic.GetById(id);
            entity.ModifiedBy = User.Identity.Name;

            this.assetBusinessLogic.DeleteAsset(id);
            return Ok();
        }

        [HttpPost]
        [Route("CreateAssetsByBatch")]
        public IHttpActionResult CreateAssetsByBatch([FromBody] AssetBatchModel model)
        {
            this.assetBusinessLogic.CreateAssetsByBatch(model.AssetId, model.IdentityCodes, User.Identity.Name);
            return Ok();
        }

        [HttpGet]
        [Route("GetLastAssignmentByAssetId")]
        public IHttpActionResult GetLastAssignmentByAssetId([FromUri] int id)
        {
            var rs = this.assetBusinessLogic.GetLastAssignmentByAssetId(id);
            return Ok(rs);
        }

        [HttpGet]
        [Route("GetAssetTypeName")]
        public IHttpActionResult GetAssetTypeName([FromUri] int id)
        {
            var rs = this.assetBusinessLogic.GetAssetTypeName(id);
            return Ok(rs);
        }
    }
}