﻿using AssetManagement.BusinessLogic;
using System.Linq;
using System.Web.Http;
using System.Web.OData;

namespace AssetManagement.ResourceServer.API.Controllers.ODataControllers
{
    [Attributes.Authorize]
    public class AssetsController : ODataController
    {
        private AssetBusinessLogic assetBL;
        
        public AssetsController()
        {
            this.assetBL = new AssetBusinessLogic();
        }

        [EnableQuery]
        public IHttpActionResult Get()
        {
            var query = this.assetBL.Query();
            return Ok(query.AsQueryable());
        }
    }
}
