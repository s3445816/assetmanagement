﻿using AssetManagement.Repositories;
using System.Linq;
using System.Web.Http;
using System.Web.OData;

namespace AssetManagement.ResourceServer.API.Controllers.ODataControllers
{
    public class AssetModelsController : ODataController
    {
        UnitOfWork unitOfWork;
        
        public AssetModelsController()
        {
            this.unitOfWork = new UnitOfWork();
        }

        [EnableQuery]
        public IHttpActionResult Get()
        {
            return Ok(this.unitOfWork.AssetRepository.Data.AsQueryable());
        }
    }
}
