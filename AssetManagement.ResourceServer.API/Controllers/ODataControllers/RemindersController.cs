﻿using AssetManagement.Repositories;
using System.Web.Http;
using System.Web.OData;
using System.Linq;
using AssetManagement.BusinessLogic;
using AssetManagement.DomainModels;
using System.Collections.Generic;
using AssetManagement.ResourceServer.API.Helpers;

namespace AssetManagement.ResourceServer.API.Controllers.ODataControllers
{
    [Attributes.Authorize]
    public class RemindersController : ODataController
    {
        private ReminderBusinessLogic reminderBL;

        public RemindersController()
        {
            this.reminderBL = new ReminderBusinessLogic();
        }

        [EnableQuery]
        public IHttpActionResult Get()
        {
            var query = this.reminderBL.Query();
            return Ok(query.AsQueryable());
        }
	}
}