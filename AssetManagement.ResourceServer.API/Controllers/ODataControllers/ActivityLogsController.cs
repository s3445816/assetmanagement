﻿using AssetManagement.BusinessLogic;
using System.Linq;
using System.Web.Http;
using System.Web.OData;

namespace AssetManagement.ResourceServer.API.Controllers.ODataControllers
{
    [Attributes.Authorize]
    public class ActivityLogsController : ODataController
    {
        private ActivityLogBusinessLogic activityLogBL;

        public ActivityLogsController()
        {
            this.activityLogBL = new ActivityLogBusinessLogic();
        }

        [EnableQuery]
        public IHttpActionResult Get()
        {
            var query = this.activityLogBL.GetAll();
            return Ok(query.AsQueryable());
        }
    }
}