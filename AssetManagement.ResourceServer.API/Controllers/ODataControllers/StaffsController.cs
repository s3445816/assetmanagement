﻿using AssetManagement.Repositories;
using System.Linq;
using System.Web.Http;
using System.Web.OData;
using System.Data.Entity;

namespace AssetManagement.ResourceServer.API.Controllers.ODataControllers
{
    public class StaffsController : ODataController
    {
        UnitOfWork unitOfWork;
        
        public StaffsController()
        {
            this.unitOfWork = new UnitOfWork();
        }

        [EnableQuery]
        public IHttpActionResult Get()
        {
            var q = this.unitOfWork.StaffRepository.Data.Include(p => p.Assignments)
                                                        .Include(p => p.Office);
            
            return Ok(q.AsQueryable());
        }
    }
}
