﻿using AssetManagement.Repositories;
using System.Linq;
using System.Web.Http;
using System.Web.OData;

namespace AssetManagement.ResourceServer.API.Controllers.ODataControllers
{
    public class OfficesController : ODataController
    {
        UnitOfWork unitOfWork;
        
        public OfficesController()
        {
            this.unitOfWork = new UnitOfWork();
        }

        [EnableQuery]
        public IHttpActionResult Get()
        {
            return Ok(this.unitOfWork.OfficeRepository.Data.AsQueryable());
        }
    }
}
