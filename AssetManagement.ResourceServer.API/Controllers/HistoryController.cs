﻿using AssetManagement.BusinessLogic;
using AssetManagement.DomainModels;
using AssetManagement.ResourceServer.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AssetManagement.ResourceServer.API.Controllers
{
    [Attributes.Authorize]
    [RoutePrefix("api/History")]
    public class HistoryController : ApiController
    {
        private AssetLogBusinessLogic logBusinessLogic;

        public HistoryController()
        {
            this.logBusinessLogic = new AssetLogBusinessLogic();
        }

        [HttpGet]
        [Route("GetHistory")]
        public IHttpActionResult GetHistory(int Id, string Name)
        {
            var type = this.logBusinessLogic.GetHistoryType(Id, Name);
            if (type == null)
                return BadRequest();

            if (type.Equals(typeof(Staff)))
            {
                return GetStaffHistory(Id);
            }
            else if (type.Equals(typeof(Asset)))
            {
                return GetAssetHistory(Id);
            }

            return BadRequest();
        }

        [HttpGet]
        [Route("GetStaffHistory")]
        public IHttpActionResult GetStaffHistory(int Id)
        {
            var staff = this.logBusinessLogic.GetStaffById(Id);
            var currentAssignments = this.logBusinessLogic.GetCurrentAssignments(staff);
            var previousAssignments = this.logBusinessLogic.GetPreviousAssignments(staff);

            return Ok(new StaffHistoryModel
                    {
                        StaffId = staff.Id,
                        StaffName = staff.Name,
                        CurrentAssignments = currentAssignments,
                        PreviousAssignments = previousAssignments
                    });
        }

        [HttpGet]
        [Route("GetAssetHistory")]
        public IHttpActionResult GetAssetHistory(int Id)
        {
            var asset = this.logBusinessLogic.GetAssetById(Id);
            var assignments = this.logBusinessLogic.GetAssignmentsOfAsset(Id);
            var items = assignments.Select(item => new AssetHistoryItem
            {
                AssignmentId = item.Id,
                IdentityCode = item.IdentityCode,
                CurrentOwner = item.Staff != null ? item.Staff.Name : string.Empty,
                PreviousOwners = this.logBusinessLogic.GetPreviousOwnersOfAssignment(item)
            });

            return Ok(new AssetHistoryModel
                    {
                        AssetId = asset.Id,
                        AssetName = asset.Name,
                        Items = items
                    });
        }
    }
}
