﻿using AssetManagement.BusinessLogic;
using AssetManagement.DomainModels;
using AssetManagement.ResourceServer.API.Attributes;
using AssetManagement.ResourceServer.API.Models;
using System;
using System.Web.Http;

namespace AssetManagement.ResourceServer.API.Controllers
{
    [Attributes.Authorize]
    [RoutePrefix("api/Staff")]
    public class StaffController : ApiController
    {
        private StaffBusinessLogic staffBusinessLogic;

        public StaffController()
        {
            this.staffBusinessLogic = new StaffBusinessLogic();
        }

        [HttpPost]
        [Route("Create")]
        public IHttpActionResult Create([FromBody] StaffRegisterModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var existExt = this.staffBusinessLogic.Exist(model.Ext);
            if (existExt) return BadRequest();
            Staff staff = new Staff
            {
                Name = model.Name,
                Ext = model.Ext,
                Note = model.Note,
                OfficeId = model.OfficeId,
                Resigned = false,
                CreatedDate = DateTime.Now,
                CreatedBy = User.Identity.Name
            };

            var id = this.staffBusinessLogic.AddNewStaff(staff);
            return Ok(id);
        }

        [HttpPost]
        [TwoFactorAuthorize]
        [Route("Resign")]
        public IHttpActionResult Resign([FromBody] int id)
        {
            Staff staff = this.staffBusinessLogic.GetById(id);
            if (staff == null)
                return BadRequest();

            staff.ModifiedBy = User.Identity.Name;
            this.staffBusinessLogic.ResignedStaff(staff);

            return Ok(id);
        }

        [HttpPost]
        [Route("ChangeOffice")]
        public IHttpActionResult ChangeOffice([FromBody] StaffOfficeModel staffOfficeModel)
        {
            this.staffBusinessLogic.ChangingOffice(staffOfficeModel.Id, staffOfficeModel.OfficeId, User.Identity.Name);
            return Ok();
        }

        [HttpPost]
        [Route("Assign")]
        public IHttpActionResult Assign([FromBody] StaffAssignmentModel staffAssignmentModel)
        {
            this.staffBusinessLogic.AssignAsset(staffAssignmentModel.StaffId, staffAssignmentModel.AssetsId, User.Identity.Name);
            return Ok();
        }

        [HttpPost]
        [Route("EditAssign")]
        public IHttpActionResult EditAssign([FromBody] StaffAssignmentModel staffAssignmentModel)
        {
            this.staffBusinessLogic.EditAssignmentAsset(staffAssignmentModel.StaffId, staffAssignmentModel.AssetsId, User.Identity.Name);
            return Ok();
        }

        [HttpGet]
        [Route("UsedExtList")]
        public IHttpActionResult GetUsedExtList()
        {
            var result = this.staffBusinessLogic.GetUsedExtList();
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAssignments")]
        public IHttpActionResult GetAssignments([FromUri] int id)
        {
            var result = this.staffBusinessLogic.GetAssignments(id);
            var staff = this.staffBusinessLogic.GetById(id);
            return Ok(new {
                            Assignments = result,
                            StaffName = staff.Name
                        }
                     );
        }

        [HttpGet]
        [Route("GetStaffName")]
        public IHttpActionResult GetStaffName([FromUri] int id)
        {
            var staff = staffBusinessLogic.GetById(id);
            return Ok(new { name = staff.Name });
        }
    }
}
