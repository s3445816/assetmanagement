﻿using AssetManagement.Repositories;
using System.Collections;
using System.Linq;
using System.Web.Http;

namespace AssetManagement.ResourceServer.API.Controllers
{
    [Attributes.Authorize]
    [RoutePrefix("api/Office")]
    public class OfficeController : ApiController
    {
        private UnitOfWork unitOfWork;

        public OfficeController()
        {
            this.unitOfWork = new UnitOfWork();
        }

        [Route("All")]
        public IList Get()
        {
            return this.unitOfWork.OfficeRepository.Data.Select(p => new { p.Id, p.Name }).ToList();
        }
    }
}
