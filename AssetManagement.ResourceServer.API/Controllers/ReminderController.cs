﻿using AssetManagement.BusinessLogic;
using AssetManagement.DomainModels;
using AssetManagement.ResourceServer.API.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AssetManagement.ResourceServer.API.Controllers
{
    [Attributes.Authorize]
    [RoutePrefix("api/Reminder")]
    public class ReminderController : ApiController
    {
        private ReminderBusinessLogic reminderBusinessLogic;

        public ReminderController()
        {
            this.reminderBusinessLogic = new ReminderBusinessLogic();
        }

        [Route("Complete")]
        [HttpPost]
        public IHttpActionResult CompleteReminder(IEnumerable<int> reminderIds)
        {
            var modifiedBy = User.Identity.Name;

            foreach (var id in reminderIds)
            {
                var reminder = this.reminderBusinessLogic.GetById(id);
                reminder.Completed = true;
                this.reminderBusinessLogic.UpdateReminder(reminder, modifiedBy);
            }
            return Ok();
        }

        [HttpPost]
        [Route("AddNewReminder")]
        public IHttpActionResult Create([FromBody] ReminderViewModel reminder)
        {
            Reminder entity = Mapper.Map<Reminder>(reminder);
            entity.CreatedBy = User.Identity.Name;

            var result = this.reminderBusinessLogic.AddNewReminder(entity);
            return Ok(result);
        }

        [HttpPost]
        [Route("UpdateReminder")]
        public IHttpActionResult Edit([FromBody] ReminderViewModel reminder)
        {
            Reminder entity = this.reminderBusinessLogic.GetById(reminder.Id);
            Mapper.Map(reminder, entity);
            entity.ModifiedBy = User.Identity.Name;

            this.reminderBusinessLogic.UpdateReminder(entity);
            return Ok();
        }

        [HttpPost]
        [Route("DeleteReminder")]
        public IHttpActionResult Delete([FromBody] int id)
        {
            this.reminderBusinessLogic.DeleteReminder(id);
            return Ok();
        }
    }
}
