﻿using AssetManagement.API.Models;
using AssetManagement.DomainModels;
using AssetManagement.ResourceServer.API.App_Start;
using AssetManagement.ResourceServer.API.Models;
using AutoMapper;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System.Web.Http;

[assembly: OwinStartup(typeof(AssetManagement.ResourceServer.API.Startup))]
namespace AssetManagement.ResourceServer.API
{
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            ConfigureAuth(app);

            ConfigureOAuthTokenConsumption(app);

            WebApiConfig.Register(config);
            app.UseCors(CorsOptions.AllowAll);
            app.UseWebApi(config);

            AutoMapperConfig.RegisterMappings();
        }
    }
}