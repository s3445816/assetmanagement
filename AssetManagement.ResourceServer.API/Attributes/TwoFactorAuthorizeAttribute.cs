﻿using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net.Http;
using System.Net;
using AssetManagement.ResourceServer.API.Helpers;

namespace AssetManagement.ResourceServer.API.Attributes
{
    public class TwoFactorAuthorizeAttribute : AuthorizationFilterAttribute
    {
        public override Task OnAuthorizationAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            var principal = actionContext.RequestContext.Principal as ClaimsPrincipal;
            var preSharedKey = principal.FindFirst("PSK").Value;
            bool hasValidTotp = OtpHelper.HasValidToptp(actionContext.Request, preSharedKey);

            if (!hasValidTotp)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized,
                                                new CustomError()
                                                {
                                                    Code = 100,
                                                    Message = "One Time Password is invalid"
                                                });
            }

            return Task.FromResult<object>(null);
        }
    }

    public class CustomError
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }
}