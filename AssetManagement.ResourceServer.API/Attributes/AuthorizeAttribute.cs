﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;

namespace AssetManagement.ResourceServer.API.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class AuthorizeAttribute : System.Web.Http.AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            if(actionContext.RequestContext.Principal.Identity.IsAuthenticated)
            {
                // A server that receives valid credentials that are not adequate 
                // to gain access ought to respond with the 403 (Forbidden) status code.
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Forbidden);
            }
            else
            {
                // The 401 (Unauthorized) status code indicates that the request has not 
                // been applied because it lacks valid authentication credentials for 
                // the target resource
                base.HandleUnauthorizedRequest(actionContext);
            }
        }
    }
}