﻿using AssetManagement.ResourceServer.API.AutoMapperProfile;
using AutoMapper;

namespace AssetManagement.ResourceServer.API.App_Start
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(config =>
            {
                config.AddProfile(new StaffMappingProfile());
                config.AddProfile(new AssetMappingProfile());
                config.AddProfile(new ReminderMappingProfile());
                config.AddProfile(new RequestMappingProfile());
            });

            Mapper.AssertConfigurationIsValid();
        }
    }
}