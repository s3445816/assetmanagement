﻿using AssetManagement.DomainModels;
using Microsoft.OData.Edm;
using Newtonsoft.Json.Serialization;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.OData.Builder;
using System.Web.OData.Extensions;

namespace AssetManagement.ResourceServer.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Config OData
            config.MapODataServiceRoute("odata", "odata", GenerateEdmModel());

            // Config Json Formatter
            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }

        private static IEdmModel GenerateEdmModel()
        {
            ODataModelBuilder builder = new ODataConventionModelBuilder();

            // To ignore a property in class:
            var staffEntitySet = builder.EntitySet<Staff>("staffs");
            staffEntitySet.EntityType.Ignore(p => p.AssetLogs);
            staffEntitySet.EntityType.Ignore(p => p.Users);

            var assignmentCplx = builder.ComplexType<Assignment>();
            assignmentCplx.Ignore(p => p.StaffId);
            assignmentCplx.Ignore(p => p.Staff);
            assignmentCplx.Ignore(p => p.CreatedBy);
            assignmentCplx.Ignore(p => p.CreatedDate);
            assignmentCplx.Ignore(p => p.ModifiedBy);
            assignmentCplx.Ignore(p => p.ModifiedDate);
            assignmentCplx.Ignore(p => p.AssetLogs);
            assignmentCplx.Ignore(p => p.Asset);

            var officeCplx = builder.ComplexType<Office>();
            officeCplx.Ignore(p => p.Id);
            officeCplx.Ignore(p => p.CreatedBy);
            officeCplx.Ignore(p => p.CreatedDate);
            officeCplx.Ignore(p => p.ModifiedBy);
            officeCplx.Ignore(p => p.ModifiedDate);
            officeCplx.Ignore(p => p.Staffs);

            var reminderEntitySet = builder.EntitySet<Reminder>("reminders");
            reminderEntitySet.EntityType.Ignore(r => r.CreatedBy);
            reminderEntitySet.EntityType.Ignore(r => r.CreatedDate);
            reminderEntitySet.EntityType.Ignore(r => r.ModifiedBy);
            reminderEntitySet.EntityType.Ignore(r => r.ModifiedDate);

            var assetEntitySet = builder.EntitySet<Asset>("assets");

            var activityLogEntitySet = builder.EntitySet<ActivityLog>("activitylogs");

            return builder.GetEdmModel();
        }
    }
}
