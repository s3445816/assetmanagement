﻿using AssetManagement.DomainModels;
using AssetManagement.ResourceServer.API.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetManagement.ResourceServer.API.AutoMapperProfile
{
    public class ReminderMappingProfile : Profile
    {
        public ReminderMappingProfile()
            : base ("ReminderMappingProfile")
        {
        }

        protected override void Configure()
        {
            Mapper.CreateMap<Reminder, ReminderViewModel>();
            Mapper.CreateMap<ReminderViewModel, Reminder>()
                .ForMember(x => x.CreatedDate, opt => opt.Ignore())
                .ForMember(x => x.CreatedBy, opt => opt.Ignore())
                .ForMember(x => x.ModifiedDate, opt => opt.Ignore())
                .ForMember(x => x.ModifiedBy, opt => opt.Ignore());
        }
    }
}