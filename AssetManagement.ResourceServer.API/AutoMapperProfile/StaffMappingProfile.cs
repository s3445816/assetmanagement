﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetManagement.ResourceServer.API.AutoMapperProfile
{
    public class StaffMappingProfile : Profile
    {
        public const string ViewModel = "StaffMappingProfile";

        public string ProfileName
        {
            get { return ViewModel; }
        }

        protected override void Configure()
        {

        }
    }
}