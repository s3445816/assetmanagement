﻿using AssetManagement.API.Models;
using AssetManagement.DomainModels;
using AssetManagement.ResourceServer.API.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetManagement.ResourceServer.API.AutoMapperProfile
{
    public class RequestMappingProfile : Profile
    {
        public RequestMappingProfile()
            : base ("RequestMappingProfile")
        {
        }

        protected override void Configure()
        {
            CreateMap<RequestAddNewModel, Request>()
                .ForAllMembers(p => p.Ignore());

            CreateMap<RequestAddNewModel, Request>()
                .ForMember(s => s.Reason, t => t.MapFrom(p => p.Reason))
                .ForMember(s => s.StartDate, t => t.MapFrom(p => p.Start))
                .ForMember(s => s.EndDate, t => t.MapFrom(p => p.End))
                .ForMember(s => s.LeaveTypeId, t => t.MapFrom(p => p.LeaveType))
                ;

            CreateMap<Request, RequestCalendarModel>()
                .ForAllMembers(p => p.Ignore());

            CreateMap<Request, RequestCalendarModel>()
                .ForMember(s => s.Title, t => t.MapFrom(p => p.Reason))
                .ForMember(s => s.Start, t => t.MapFrom(p => p.StartDate))
                .ForMember(s => s.End, t => t.MapFrom(p => p.EndDate))
                .ForMember(s => s.LeaveType, t => t.MapFrom(p => p.LeaveType.Code))
                ;
        }
    }
}