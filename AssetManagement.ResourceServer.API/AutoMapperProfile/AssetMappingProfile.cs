﻿using AssetManagement.API.Models;
using AssetManagement.DomainModels;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetManagement.ResourceServer.API.AutoMapperProfile
{
    public class AssetMappingProfile : Profile
    {
        public const string ViewModel = "AssetMappingProfile";

        public string ProfileName
        {
            get { return ViewModel; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<Asset, AssetViewModel>();
            Mapper.CreateMap<AssetViewModel, Asset>()
                .ForMember(x => x.CreatedDate, opt => opt.Ignore())
                .ForMember(x => x.CreatedBy, opt => opt.Ignore())
                .ForMember(x => x.ModifiedDate, opt => opt.Ignore())
                .ForMember(x => x.ModifiedBy, opt => opt.Ignore())
                .ForMember(x => x.IsDeleted, opt => opt.Ignore())
                .ForMember(x => x.Assignments, opt => opt.Ignore());
        }
    }
}